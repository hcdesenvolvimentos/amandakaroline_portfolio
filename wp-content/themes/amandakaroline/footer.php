<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amanda_Karoline
 */
global $configuracao;
?>

	<!-- BOTÃO VOLTAR AO TOPO -->
		<div class="backToTop" style="display: none;">
			<span id="subir">
				<i class="fas fa-chevron-up"></i>
			</span>
		</div>

		<!-- RODAPÉ -->
		<footer class="rodape">

			<div class="redesSociais">
				<a href="<?php echo $configuracao['opt_whatsapp'] ?>" target="_blank">
					<i class="fab fa-whatsapp"></i>
				</a>
				<a href="<?php echo $configuracao['opt_facebook'] ?>" target="_blank">
					<i class="fab fa-facebook-square"></i>
				</a>
				<a href="<?php echo $configuracao['opt_instagram'] ?>" target="_blank">
					<i class="fab fa-instagram"></i>
				</a>
				<a href="<?php echo $configuracao['opt_youtube'] ?>" target="_blank">
					<i class="fab fa-youtube"></i>
				</a>
				<a href="<?php echo $configuracao['opt_pinterest'] ?>" target="_blank">
					<i class="fab fa-pinterest"></i>
				</a>
			</div>

			<div class="copyright">
				<p><?php echo $configuracao['opt_copyright'] ?></p>
			</div>
		</footer>	
<?php wp_footer(); ?>

</body>
</html>
