<?php

/**
 * Plugin Name: Base Amanda Karoline
 * Description: Controle base do tema Amanda Karoline.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseAmandakaroline () {

		// TIPOS DE CONTEÚDO
		conteudosAmandakaroline();

		taxonomiaAmandakaroline();

		metaboxesAmandakaroline();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
	function conteudosAmandakaroline ()
	{

		// TIPOS DE CONTEÚDO SERVIÇO
	 	tipoServico();

		// TIPOS DE DEPOIMENTOS
		tipoDepoimento();
		
		//TIPOS DE PARCERIAS
		tipoParceria();

		//SESSÃO BANNER INICIAL
		tipoSessaoInicial();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	

	// CUSTOM POST TYPE SERVIÇO
	function tipoServico() {

		$rotulosServico = array(
								'name'               => 'Portfolio',
								'singular_name'      => 'portfolio',
								'menu_name'          => 'Portfolio',
								'name_admin_bar'     => 'Portfolio',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo portfolio',
								'new_item'           => 'Novo portfolio',
								'edit_item'          => 'Editar portfolio',
								'view_item'          => 'Ver portfolio',
								'all_items'          => 'Todos os portfolios',
								'search_items'       => 'Buscar portfolio',
								'parent_item_colon'  => 'Dos portfolios',
								'not_found'          => 'Nenhum portfolio cadastrado.',
								'not_found_in_trash' => 'Nenhum portfolio na lixeira.'
							);

		$argsServico 	= array(
								'labels'             => $rotulosServico,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-hammer',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'portfolio' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail' , 'editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('portfolio', $argsServico);

	}

	// CUSTOM POST TYPE DEPOIMENTOS
	function tipoDepoimento() {

		$rotulosDepoimento = array(
								'name'               => 'Depoimentos',
								'singular_name'      => 'depoimento',
								'menu_name'          => 'Depoimentos',
								'name_admin_bar'     => 'Depoimentos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo depoimento',
								'new_item'           => 'Novo depoimento',
								'edit_item'          => 'Editar depoimento',
								'view_item'          => 'Ver depoimento',
								'all_items'          => 'Todos os depoimentos',
								'search_items'       => 'Buscar depoimento',
								'parent_item_colon'  => 'Dos depoimentos',
								'not_found'          => 'Nenhum depoimento cadastrado.',
								'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
							);

		$argsDepoimento 	= array(
								'labels'             => $rotulosDepoimento,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-book',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'depoimentos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail'),
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('depoimento', $argsDepoimento);

	}

	// CUSTOM POST TYPE PARCERIAS
	function tipoParceria() {

		$rotulosParceria = array(
								'name'               => 'Parcerias',
								'singular_name'      => 'parceria',
								'menu_name'          => 'Parcerias',
								'name_admin_bar'     => 'Parcerias',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova parceria',
								'new_item'           => 'Nova parceria',
								'edit_item'          => 'Editar parceria',
								'view_item'          => 'Ver parceria',
								'all_items'          => 'Todos as parcerias',
								'search_items'       => 'Buscar parcerias',
								'parent_item_colon'  => 'Das parcerias',
								'not_found'          => 'Nenhuma parceria cadastrada.',
								'not_found_in_trash' => 'Nenhuma parceria na lixeira.'
							);

		$argsParceria 	= array(
								'labels'             => $rotulosParceria,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-groups',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'parceiros' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail'),
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('parceria', $argsParceria);

	}


	//CUSTOM POST TYPE SESSÃO INICIAL
	function tipoSessaoInicial() {

		$rotulosSessaoInicial = array(
								'name'               => 'Banner Destaque',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Banner Destaque',
								'name_admin_bar'     => 'Banner Destaque',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo banner destaque',
								'new_item'           => 'Novo Banner Destaque',
								'edit_item'          => 'Editar banner destaque',
								'view_item'          => 'Ver banner destaque',
								'all_items'          => 'Todos os banners destaques',
								'search_items'       => 'Buscar banners destaques',
								'parent_item_colon'  => 'Dos banners destaques',
								'not_found'          => 'Nenhum banner destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum banner destaque na lixeira.'
							);

		$argsSessaoInicial 	= array(
								'labels'             => $rotulosSessaoInicial,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 3,
								'menu_icon'          => 'dashicons-media-video',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail'),
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsSessaoInicial);

	}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaAmandakaroline () {		
		taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesAmandakaroline(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Amandakaroline_';

			// METABOX DE SERVIÇOS
			$metaboxes[] = array(

				'id'			=> 'metaServicos',
				'title'			=> 'Detalhes do serviço',
				'pages' 		=> array( 'portfolio' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(	
					array(
						'name'  => 'Imagem de fundo da galeria: ',
						'id'    => "{$prefix}imagem_fundo_galeria",
						'desc'  => 'Imagem que ficará no carrosel de serviços.',
						'type'  => 'image_advanced',
						'max_file_uploads' => 1,
					),
					array(
						'name'  => 'Galeria de imagens do serviço:',
						'id'    => "{$prefix}galeria_imagens_servicos",
						'desc'  => 'Galeria de imagens do serviço',
						'type'  => 'image_advanced',
					), //  A IMAGEM DESTAQUE VAI FICAR COMO FUNDO DA GALERIA DO SERVIÇO
					// O EDITOR DO WORDPRESS SERVE NESTE CASO PARA DEFINIR A DESCRIÇÃO DO SERVICO 
				),		
			);

			// METABOX DE DEPOIMENTOS
			$metaboxes[] = array(

				'id'			=> 'metaDepoimentos',
				'title'			=> 'Detalhes do depoimento',
				'pages' 		=> array( 'depoimento' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Texto do Depoimento: ',
						'id'    => "{$prefix}text_depoimento",
						'desc'  => 'Texto do Depoimento',
						'type'  => 'textarea',
					),	
				),		
			);

			// METABOX DE PARCERIA
			$metaboxes[] = array(

				'id'			=> 'metaParcerias',
				'title'			=> 'Detalhes da parceria',
				'pages' 		=> array( 'parceria' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Link do parceiro: ',
						'id'    => "{$prefix}link_do_parceiro",
						'desc'  => 'Link para redirecionamento do parceiro',
						'type'  => 'text',
					),	
					
				),		
			);

			// METABOX SESSÃO INICIAL
			$metaboxes[] = array(

				'id'			=> 'metaSessao_Inicial',
				'title'			=> 'Detalhes da sessão inicial',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'URL do vídeo: ',
						'id'    => "{$prefix}url_video_destaque",
						'desc'  => 'URL do vídeo',
						'type'  => 'text',
					),	
					array(
					    'name' => 'Link do destaque',
					    'id'   => "{$prefix}link_redirecionamento_destaque",
					    'type' => 'text',
					    'placeholder' => 'Cole aqui o link de redirecionamento.',
					),
					array(
					    'name' => 'Verificação',
					    'id'   => "{$prefix}checkbox_destaque",
					    'type' => 'checkbox',
					    'std'  => 0, // 0 or 1
					    'desc' => 'Preencha este checkbox se você está inserindo um vídeo',
					),
					
				),		
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesAmandakaroline(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerAmandakaroline(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseAmandakaroline');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseAmandakaroline();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );