

<?php
/**
 * Template Name: Inicial
 * Description: 
 *
 * @package Amanda_Karoline
 */



get_header(); ?>
<!-- PG INICIAL -->
<div class="pg pg-inicial">

	<!-- CARROSSEL DESTAQUE -->
	<section class="carrosselDestaqueTopo" id="carrosselDestaqueTopo" class="owl-Carousel">
		<?php 
			//LOOP DE POST DESTAQUES
			$destaque = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
			while ( $destaque->have_posts() ) : $destaque->the_post();
				
				// FOTO DESTACADA
				$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoDestaque = $fotoDestaque[0];

				// METABOX
				$urlVideoDestaque = rwmb_meta('Amandakaroline_url_video_destaque');
				$linkDestaque = rwmb_meta('Amandakaroline_link_redirecionamento_destaque');
				$verificacaoVideo = rwmb_meta('Amandakaroline_checkbox_destaque');
				
				// VERIFICAÇÃO - VÍDEO
				if ($verificacaoVideo != 0) :
		 ?>
		<div class="item" style="background:url(<?php echo $fotoDestaque ?>)" >
			<!-- BANNER PRINCIPAL OU VIDEO-->
			<video loop="" autoplay="true" muted="" class="video0" style="background:url( )" id="" controls="">
				<source src="<?php echo $urlVideoDestaque ?>" type="video/mp4">
			</video>
		</div> 
		
		<?php else: ?>

		<div class="item" style="background:url(<?php echo $fotoDestaque ?>)" >
		</div>

		<?php endif;endwhile; wp_reset_query(); ?>
	</section>
	
	<!-- SESSÃO INICIAL SOBRE MIM  TOPO -->
	<section class="sessaoSobreMimTopo" style="background: url(<?php echo $configuracao['opt_fundo_sessao_apresentacao']['url'] ?>);">
		<h6 class="hidden">Amanda Karoline</h6>

		<div class="row">		

			<!-- IMAGEM APRESENTAÇÃO -->
			<div class="col-sm-6">
				<div class="imgTopo">
					<figure>
						<img class="img-responsive" src="<?php echo $configuracao['opt_imagemLadoEsquerdoSessaoApresentacao']['url'] ?> " alt="Amanda Karoline">
					</figure>
				</div>						
			</div>

			<!--CONTEUDO SOBRE MIM TOPO-->
			<div class="col-sm-6">
				<article>								
					<span class="bordaSuperiorTextoSobreMimTopo"></span>
					<div class="textoSobreMimTopo">
						<div class="tituloPrincipal">
							<h1><?php echo $configuracao['opt_TituloApresentacaoDireita'] ?></h1>
		     		    </div>	
					
						<?php echo $configuracao['opt_textoApresentacaoPrincipal'] ?>

						<div class="imagemLogoSobreMim">
							<figure class="logoAbaixoDoTexto">
								<img src="<?php echo $configuracao['opt_logoFinalDeTexto']['url'] ?>" alt="Amanda Karoline">
							</figure>
		     		    </div>							
					</div>
					<span class="bordaInferiorTextoSobreMimTopo"></span>
				</article>
			</div>

	    </div>					
	</section>

	<!-- SESSÃO DO PARALLAX -->
	<section class="paralaxCentral verificacaoVisibilidade3 filtroPreto" style="background: url(<?php echo $configuracao['opt_parallax']['url'] ?>); ">
	</section>

	<!-- CARROSSEL DE DESTAQUE -->
	<section class="carrosselDestaque sessao" >
		<h6 id="portfolio">Portfólio</h6>
		<div class="areaCarrossel">
			
			<section class="detalhesCarrosselServicos">
				<div class="areaTexto">
					<h2></h2>
					<small class="hidden" data-descricao=""></small>
					<a href="#" class="abrirModalGaleria">Clique aqui para conferir</a>
				</div>
				<span class="fecharModalDetalhesPortfolio">
					<i class="far fa-times-circle"></i>
				</span>
				<div class="imagemCentralizada"></div>
			</section>

			<div id="carrosselDestaque" class="owl-Carousel">
				<?php 
					$carrosselPortfolio = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $carrosselPortfolio->have_posts() ) : $carrosselPortfolio->the_post();
						
						//FOTO PORTFOLIO 
						$fotoDestaquePortfolio = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoDestaquePortfolio = $fotoDestaquePortfolio[0];
						$tituloDestaquePortfolio = $post->post_title;
						$descricaoDestaquePortfolio = $post->post_content;
						$imagemFundoGaleria = rwmb_meta( 'Amandakaroline_imagem_fundo_galeria' );
						foreach ($imagemFundoGaleria as $imagemFundoGaleria) {
							$imagemFundoGaleria = $imagemFundoGaleria['full_url'];

						}

			

						// METABOX
						$imagem = rwmb_meta( 'Amandakaroline_galeria_imagens_servicos');
				?>
				<!-- ITEM -->
				<figure class="item post<?php echo $post->ID ?>" style="background: url(<?php echo $fotoDestaquePortfolio ?>);">
					<a href="#" 
					data-titulo="<?php echo $tituloDestaquePortfolio ?>" 
					data-fundoGaleria="<?php echo $imagemFundoGaleria; ?>" 
					data-descricao="<?php echo $descricaoDestaquePortfolio ?>" 
					data-urlImg="<?php echo $fotoDestaquePortfolio ?>"
					data-galeria="<?php foreach ($imagem as $imagem ){ $imagem = $imagem['full_url']; echo $imagem." | "; } ?>">
						<img class="img-responsive" src="<?php echo $fotoDestaquePortfolio ?>" alt="<?php echo $tituloDestaquePortfolio ?>">
					</a>
				</figure>
				<?php endwhile;  wp_reset_query(); ?>
			</div>
		</div>
	</section>
	
	<!-- SESSÃO SOBRE MIM  CENTRO-->
	<section class="sessaoSobreMimCentro" id="sobre-mim-centro">
		<div class="row">

			<div class="col-sm-6">
				<article>
					<span class="bordaSuperiorTextoSobreMimCentro"></span>
					<div class="textoSobreMimCentro">
						<h6><?php echo $configuracao['opt_tituloSobreMim'] ?></h6>
						<?php echo $configuracao['opt_textoSobreMimCentro'] ?>
						
					    <div class="imagemLogoSobreMim">
							<figure>
								<img src="<?php echo $configuracao['opt_logoFinalDeTexto']['url'] ?>" alt="Amanda Karoline">
						   </figure>
			     	   </div>	
					</div>
		     	   	<span class="bordaInferiorTextoSobreMimCentro"></span>
					
				</article>
			</div>

			<div class="col-sm-6">
				<div class="imagemCentroParte1">
					<figure>
						<img class="verificacaoVisibilidade1 filtroPreto" src="<?php echo $configuracao['opt_imagemSobreMimCentro']['url'] ?>">
					</figure>
				</div>
			</div>
			
		</div>

		<div class="row">					
			<div class="col-sm-6">
				<div class="imagemCentroParte2">
				<figure>
					<img class="verificacaoVisibilidade2 filtroPreto" src="<?php echo $configuracao['opt_imagemSobreMimParte2']['url'] ?>">
				</figure>
				</div>
			</div>

			<div class="col-sm-6">
				<article>
					<span class="bordaSuperiorTextoSobreMimCentroParte2"></span>
					<div class="textoSobreMimCentroParte2">
						
						<?php echo $configuracao['opt_textoSobreMimCentroParte2'] ?>
					</div>
					<span class="bordaInferiorTextoSobreMimCentroParte2"></span>
				</article>
			</div>	
		</div>
		<div class="sobreMimDesktop">
			<div class="row">					
				
				<div class="col-sm-6">
					<article>
						<span class="bordaSuperiorTextoSobreMimCentroParte3"></span>
						<div class="textoSobreMimCentroParte3">
							
							<?php echo $configuracao['opt_textoSobreMimCentroParte3'] ?>
							<a href="<?php echo $configuracao['opt_linkSobreMim'] ?>" class="link confiraAqui">confira aqui</a>
							<div class="imagemLogoSobreMim">
								<figure>
									<img src="<?php echo $configuracao['opt_logoFinalDeTexto']['url'] ?>" alt="Amanda Karoline">
							</figure>
						</div>
						</div>
						
						<span class="bordaInferiorTextoSobreMimCentroParte3"></span>
					</article>
				</div>
				<div class="col-sm-6">
					<div class="imagemCentroParte3">
						<figure>
							<img class="verificacaoVisibilidade2 filtroPreto" src="<?php echo $configuracao['opt_imagemSobreMimParte3']['url'] ?>">
						</figure>
					</div>				
				</div>	
			</div>
		</div>
		<div class="sobreMimMobile">
			<div class="row">					
				<div class="col-sm-6">
						<div class="imagemCentroParte3">
							<figure>
								<img class="verificacaoVisibilidade2 filtroPreto" src="<?php echo $configuracao['opt_imagemSobreMimParte3']['url'] ?>">
							</figure>
						</div>				
				</div>
				<div class="col-sm-6">
					<article>
						<span class="bordaSuperiorTextoSobreMimCentroParte3"></span>
						<div class="textoSobreMimCentroParte3">
							
							<?php echo $configuracao['opt_textoSobreMimCentroParte3'] ?>
							<a href="<?php echo $configuracao['opt_linkSobreMim'] ?>" class="link">confira aqui</a>
							<div class="imagemLogoSobreMim">
								<figure>
									<img src="<?php echo $configuracao['opt_logoFinalDeTexto']['url'] ?>" alt="Amanda Karoline">
							</figure>
						</div>
						</div>
						
						<span class="bordaInferiorTextoSobreMimCentroParte3"></span>
					</article>
				</div>
					
			</div>
		</div>
	</section>

	<!-- SESSÃO ESTATISTICA-->
	<section class="minhaEstatistica " id="estatistica">
		<h6 class="hidden">Área Estatistica</h6>
		<div class="estatisticas">
			<figure>
				<img src="<?php echo $configuracao['opt_imagemEstatisticas']['url'] ?>" alt="Estatisticas">
			</figure>
		</div>				
	</section>

	<!-- SESSÃO QUALIDADES-->
	<section class="sessaoQualidades">
		<div class="row ">
			<div class="areaQualidades" id="qualidades"  >
				<div class="col-xs-3">
					<figure>
						<img src="<?php echo $configuracao['opt_imagemQualidadeEsquerda']['url'] ?>" alt="Imagem qualidade">
					</figure>
				</div>
				<div class="col-xs-9">
					<div class="textoQualidades">
						<p id="texto" data-texto="<?php echo $configuracao['opt_textoQualidadeDireita'] ?> "></p>
					</div>
				</div>
			</div>	
		</div>	

		<div class="row ">					
			<div class="col-xs-9">
				<div class="textoQualidades2">
					<p id="text2" data-texto="<?php echo $configuracao['opt_textoQualidadeEsquerda'] ?>"></p>
				</div>
			</div>

			<div class="col-xs-3">
				<figure class="segundaImagemTextoQualidades">
					<img src="<?php echo $configuracao['opt_imagemQualidadeDireita']['url'] ?>" alt="Imagem qualidade">
				</figure>
			
			</div>					
		</div>
	</section>

	<!-- SESSÃO  CARROSSEL DEPOIMENTOS-->
	<section class="carrosselDepoimentos ">	
	    <h6>Depoimentos</h6>	
		<div id="carrosselDepoimentos" class="owl-Carousel">
			<?php 
				$carrosselDepoimentos = new WP_Query( array( 'post_type' => 'depoimento', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $carrosselDepoimentos->have_posts() ) : $carrosselDepoimentos->the_post();
					
					//FOTO DESTACADA DEPOIMENTOS
					$fotoDestaqueDepoimentos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoDestaqueDepoimentos = $fotoDestaqueDepoimentos[0];
					
					//METABOX
					$textoDepoimento = rwmb_meta('Amandakaroline_text_depoimento');
					$autorDepoimento = $post->post_title;
			?>
			<!-- ITEM-->
			<div class="item">
				<article>
					<p><?php echo $textoDepoimento; ?></p>
					
				</article>
				<figure>
					<img src="<?php echo $fotoDestaqueDepoimentos ?>">
					 
				</figure>
				<h4><?php echo $autorDepoimento ?></h4>
			</div>
			<?php endwhile;  wp_reset_query(); ?>
		</div>								
	</section>

	<!-- SESSÃO  PARCERIAS-->
	<section class="carrosselParceria parceria">
		<h6>Parceiros</h6>

		<div id="carrosselParceria" class="owl-Carousel">
			<?php 
				$carrosselParceria = new WP_Query( array( 'post_type' => 'parceria', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $carrosselParceria->have_posts() ) : $carrosselParceria->the_post();
					
					//FOTO DESTACADA PARCERIA
					$fotoDestaqueParceria = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoDestaqueParceria = $fotoDestaqueParceria[0];
					//METABOX
					$linkDoParceiro = rwmb_meta('Amandakaroline_link_do_parceiro');
					$nomeParceiro = $post->post_title;
			?>	

			<!-- ITEM -->
			<figure class="item" ">
				<a href="<?php echo $linkDoParceiro; ?>" target="_blank">
					<img class="img-responsive" src="<?php echo $fotoDestaqueParceria; ?>" alt="<?php echo $nomeParceiro ?> ">
				</a>
			</figure>
	
			<?php endwhile;  wp_reset_query(); ?>
			
		</div>

		<div class="botoesCarrosselParceria">
			<button id="flechaEsquerda"><img src="img/esquerda.png" alt=""></button>
			<button id="flechaDireita"><img src="img/direita.png" alt=""></button>
		</div>

		<div class="tornarParceiro">
			<div class="abrirModalPaceiro">
				<a class="abrirFormularioParceria">TORNAR-SE PARCEIRO</a>
			</div>
		</div>	
	</section>

	<div class="instagramFooter">
		<h6> Me <strong>siga</strong> no Instagram </h6>
		<a href="">
			<figure>
				<img src="img/Capturar.JPG" alt="">
			</figure>
		</a>
	</div>

</div>	

<!-- GALERIA-->
<div class="pg pg-galeria" style="display:none">
	
	<!--TÍTULO GALERIA --> 			
	<section >
		<h6 class="hidden">Título Galeria </h6>
		<div class="descricaoGaleria" style="background: url(img/backgroundGaleria.jpg);">
			<a href="#" class="voltarPaginaAnterior">
				<i class="fas fa-arrow-left"></i>
			</a>
			<div class="textosGaleria">
				<h2></h2>
				<p>
				</p>
			</div>
		</div>

        <!-- GALERIA DE FOTOS -->
		<div class="galeria">
			
			<div class="listaDeImagensGaleria">
				<ul id="imagensDaGaleria"> 
					
				</ul>
			</div>					

		</div>

	</section>
</div>

<?php get_footer(); ?>

