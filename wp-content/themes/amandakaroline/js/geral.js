$(function(){

	/**********************************************************************
		SCRIPTS SCRIPT PARA INICIAR O VIDEO AO CARREGAR A TELA
		SCRIPT CARROSSEL DESTAQUE DO TOPO
	***********************************************************************/
		
		$(document).ready(function() {
			setTimeout(function(){
				var video = $(".video0");
				video[0].load();
				video[0].play();	
			}, 1000)

			
			$("#carrosselDestaqueTopo").owlCarousel({
				items : 1,
		        dots: true,
		        loop: false,
		        lazyLoad: true,
		        mouseDrag:true,
		        touchDrag  : true,	       
			    autoplayTimeout:5000,
			    autoplayHoverPause:true,
			    scroll:true,
			    navigation: true,
			    smartSpeed: 450,
		   		    
			    
			});
		});

	/**********************************************************************
		SCRIPTS CARROSSEL DE SERVIÇOS DO PORTFOLIO
	***********************************************************************/

		$("#carrosselDestaque").owlCarousel({
			items : 6,
	        dots: true,
	        loop: true,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,
	        autoplay:true,	       
		    autoplayTimeout:2000,
		    autoplayHoverPause:true,
		    scroll:true,
		    navigation: true,
		    smartSpeed: 450,

		    // CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:2
	            },
	            600:{
	                items:4
	            },
	           
	            991:{
	                items:4
	            },
	            1024:{
	                items:6
	            },
	            1440:{
	                items:6
	            },
	            			            
	        }		    		   		    
		    
		});

	/**********************************************************************
		SCRIPTS DO CARROSSEL DE DEPOIMENTOS
	**********************************************************************/

			if ($("#carrosselDepoimentos").find('.item').length > 1) {

			  $("#carrosselDepoimentos").owlCarousel({
		  	 	items : 1,
		        dots: true,
		        loop: true,
		        autoplay:true,
		        lazyLoad: true,
		        mouseDrag:true,
		        touchDrag  : true,	       
			    autoplayTimeout:3000,
			    autoplayHoverPause:true,
			    smartSpeed: 450,
			  });

			}else{

			  $("#carrosselDepoimentos").owlCarousel({
			    items : 1,
		        dots: true,
		        loop: false,
		        autoplay:true,
		        lazyLoad: true,
		        mouseDrag:true,
		        touchDrag  : true,	       
			    autoplayTimeout:3000,
			    autoplayHoverPause:true,
			    smartSpeed: 450,
			  });

			}

	/**********************************************************************
		SCRIPTS DO CARROSSEL DE PARCERIA
	**********************************************************************/
		
		if ($("#carrosselParceria").find('.item').length > 1) {
			$("#carrosselParceria").owlCarousel({
				items : 3,
		        dots: false,
		        loop: true,
		        autoplay:true,
		        lazyLoad: true,
		        mouseDrag:true,
		        touchDrag  : true,	       
			    autoplayTimeout:3000,
			    autoplayHoverPause:true,
			    smartSpeed: 450,
		   	
			    //CARROSSEL RESPONSIVO
		    responsiveClass:true,			    
	        responsive:{
	            320:{
	                items:1,
	                loop:true,
	            },
	            600:{
	                items:2,
	                loop:true,
	            },
	           
	            991:{
	                items:2
	            },
	            1024:{
	                items:3	
	            },
	            1440:{
	                items:3
	            },
	            			            
	        }	

		   	});


		}else{
			$("#carrosselParceria").owlCarousel({
				items : 3,
		        dots: false,
		        loop: true,
		        autoplay:true,
		        lazyLoad: true,
		        mouseDrag:true,
		        touchDrag  : true,	       
			    autoplayTimeout:3000,
			    autoplayHoverPause:true,
			    smartSpeed: 450,

			    //CARROSSEL RESPONSIVO
			    responsiveClass:true,			    
		        responsive:{
		            320:{
		                items:1,
		                loop:true,
		            },
		            600:{
		                items:2,
		                loop:true,
		            },
		           
		            991:{
		                items:2
		            },
		            1024:{
		                items:3	
		            },
		            1440:{
		                items:3
		            },
		            			            
		        }	

			    
			});
		}
		//BOTÕES DO CARROSSEL DE PARCERIA
		var carrossel_parceria = $("#carrosselParceria").data('owlCarousel');
		$('#flechaEsquerda').click(function(){ carrossel_parceria.prev(); });
		$('#flechaDireita').click(function(){ carrossel_parceria.next(); });

	/**********************************************************************
		SCRIPTS PARA PEGAR O TEXTO DO HTML 
	**********************************************************************/
		function typeWritter(texto,idElemento,tempo){
	    	var char = texto.split('').reverse();
	    	var typer = setInterval(function () {
		        if (!char.length) return clearInterval(typer);
		        var next = char.pop();
		        document.getElementById(idElemento).innerHTML += next;
		    }, tempo);
		}

	/**********************************************************************
		SCRIPTS PARA VERIFICAR SE A TELA ESTÁ APARECENDO 
	**********************************************************************/
		$.fn.isOnScreen = function(){
				var win = $(window);
				var viewport = {
					top : win.scrollTop(),
					left : win.scrollLeft()
				};

				viewport.right = viewport.left + win.width();
				viewport.bottom = viewport.top + win.height();
				
				var bounds = this.offset();
			    bounds.right = bounds.left + this.outerWidth();
			    bounds.bottom = bounds.top + this.outerHeight();
				
			    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
			};
			//DECLARAÇÃO DE VARIAVEIS
	        var text1 = false;
	        var text2 = false;
	        var text3 = false;
	        var text4 = false;
	        var text5 = false;

	        // ESTA VARIAVEL ESTA PEGANDO O QUE ESTA VINDO DENTRO DO ATRIBUTO data-texto NO HTML.
	        let textoQualidades = $('#texto').attr("data-texto");
	        let textoQualidades2 = $('#text2').attr("data-texto");

		    $(window).scroll(function(){
				var larguraWidth = $(window).width();
				if ($('#texto').isOnScreen() == true && text1 == false) {
			  		
					setTimeout(function(){ 

						// ESSA FUNÇÃO FAZ COM QUE AS LETRAS PERCORRAM A TELA, E ESTÁ EXIBINDO A VARIAVEL textoQualidades,
						// ONDE SEU CONTEUDO VEM PELO PAINEL E É PASSADO PELOS 'datas'.
				   		typeWritter(textoQualidades,'texto',45.00);
				   		
				    }, 1000);
				  
					text1 = true;
				};

				//VERIFICACAO SE O HTML DA id ESTÁ APARECENDO NA TELA,
				if ($('#text2').isOnScreen() == true && text2 == false) {
			  		
						setTimeout(function(){ 
							// ESSA FUNÇÃO FAZ COM QUE AS LETRAS PERCORRAM A TELA, E ESTÁ EXIBINDO A VARIAVEL textoQualidades2,
							// ONDE SEU CONTEUDO VEM PELO PAINEL E É PASSADO PELOS 'datas'.
					   		typeWritter(textoQualidades2,'text2',45.00);
					    }, 2000);
					  
						text2 = true;
				};
				
				//ESTE SCRIPT VERIFICA SE A CLASSE 'verificacaoVisibilidade1' ESTÁ VISIVEL, PARA APLICAR OU REMOVER O FILTRO NA IMAGEM
				if ($('.verificacaoVisibilidade1').isOnScreen() == true && text3 == false) {
					
						$(".verificacaoVisibilidade1").addClass("filtroPretoEBranco");
						$(".verificacaoVisibilidade1").removeClass("filtroPreto");

						text3 = true;
				};
				//ESTE SCRIPT VERIFICA SE A CLASSE 'verificacaoVisibilidade2' ESTÁ VISIVEL, PARA APLICAR OU REMOVER O FILTRO NA IMAGEM
				if ($('.verificacaoVisibilidade2').isOnScreen() == true && text4 == false) {
				
						$(".verificacaoVisibilidade2").addClass("filtroPretoEBranco");
						$(".verificacaoVisibilidade2").removeClass("filtroPreto");

						text4 = true;
				};
					
				//ESTE SCRIPT VERIFICA SE A CLASSE 'verificacaoVisibilidade3' ESTÁ VISIVEL, PARA APLICAR OU REMOVER O FILTRO NA IMAGEM
				if ($('.verificacaoVisibilidade3').isOnScreen() == true && text4 == false) {
					
						$(".verificacaoVisibilidade3").addClass("filtroPretoEBranco");
						$(".verificacaoVisibilidade3").removeClass("filtroPreto");
						
						text5 = true;
				};
		});		

    /**********************************************************************
		SCRIPTS MODAIS DA GALERIA
	**********************************************************************/

		//PREVENT DEFAULT CLICK DO `a`
		$( ".pg-inicial .carrosselDestaque .item" ).click(function(e) {
	  		

	  		// PEGANDO OS CONTEUDOS DOS ATRIBUTOS 'datas' E PASSANDO PARA OUTRAS VARIAVEIS
		 	let titulo = $(this).children('a').attr("data-titulo");
		 	let descricao = $(this).children('a').attr("data-descricao");
		 	let dataUrlImg = $(this).children('a').attr("data-urlImg");
		 	let fundoGaleria= $(this).children('a').attr("data-fundoGaleria");
		 	let galeriaImagens = $(this).children('a').attr("data-galeria");

			
			var galeriaImagem = galeriaImagens.split(" | ");	

			
			for (var i = 0 ; i < galeriaImagem.length - 1; i++) {
				$('#imagensDaGaleria').append("<li class='fotoGaleria'><a href='"+galeriaImagem[i]+"' id='fancy' rel='gallery1'><figure style='background:url("+galeriaImagem[i]+" )'><img src='"+galeriaImagem[i]+"'></figure></a></li>");	
			}


		 	$(".pg-inicial .carrosselDestaque .detalhesCarrosselServicos").fadeIn();
		 	$(".pg-inicial .carrosselDestaque .detalhesCarrosselServicos h2").text(titulo);
		 	$(".pg-inicial .carrosselDestaque .imagemCentralizada").css({"background":"url("+dataUrlImg+")"});


		 	//PASSANDO ATRIBUTOS PARA A GALERIA
		 	$(".pg-galeria .descricaoGaleria .textosGaleria h2").text(titulo);
		 	$(".pg-galeria .descricaoGaleria .textosGaleria p").text(descricao);
		 	$(".pg-galeria .descricaoGaleria").css({"background":"url("+fundoGaleria+")"});
		 	

	 
		 	// SCRIPT PARA DETECTAR O 'esc' DO TECLADO PARA FECHAR O MODAL DOS DETALHES DO PORTFOLIO
		 	$(document).keyup(function(e) {
				if (e.keyCode == 27) {
					$(".pg-inicial .carrosselDestaque .detalhesCarrosselServicos").fadeOut();
				}
			});

		 	// SCRIPT PARA DETECTAR O CLICK DO MOUSE NO BOTAO DE FECHAR O MODAL DOS DETALHES DO PORTFOLIO
			$( ".fecharModalDetalhesPortfolio" ).click(function(e) {
				$(".pg-inicial .carrosselDestaque .detalhesCarrosselServicos").fadeOut();
			});
		 	 event.preventDefault();
	 	});
   	
   	/**********************************************************************
		SCRIPTS FANCYBOX PARA A GALERIA DE IMAGENS 
	**********************************************************************/
		
	   	$("a#fancy").fancybox({
			'titleShow' : false,
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			closeBtn    : true,
			arrows      : true,
			nextClick   : true
		});
	
	/**********************************************************************
		SCRIPTS PARA DETECTAR O CLICK DO MOUSE E 
		ABRIR O MODAL DA GALERIA
	**********************************************************************/
		$( ".pg-inicial .carrosselDestaque .detalhesCarrosselServicos .areaTexto .abrirModalGaleria" ).click(function(e){
			event.preventDefault();
			$('.pg-galeria').fadeIn();
			$("body").addClass("travarScroll");
		});

	/**********************************************************************
		SCRIPTS PARA DETECTAR O CLICK DO MOUSE E 
		FECHAR O MODAL DA GALERIA DE IMAGENS
	**********************************************************************/
		$( ".voltarPaginaAnterior" ).click(function(e) {
	  		event.preventDefault();
	  	});

	/**********************************************************************
		SCRIPTS PARA QUANDO FECHAR O MODAL DA GALERIA, 
		REMOVER A CLASSE QUE TRAVA O SCROLL DO SITE
	**********************************************************************/

		$( ".voltarPaginaAnterior" ).click(function(e) {
		 	$('.pg-galeria').fadeOut();
		 	$("body").removeClass("travarScroll");

		 	$('#imagensDaGaleria .fotoGaleria').remove();

		});

	/**********************************************************************
		SCRIPTS PARA PEGAR O TAMANHO DA TELA
	**********************************************************************/

		$('#carrosselDestaqueTopo .item').css({'height':($(window).height())+'px'});
		$(window).resize(function(){
			$('#carrosselDestaqueTopo .item').css({'height':($(window).height())+'px'});
		});

	/**********************************************************************
		SCRIPTS BOTAO VOLTAR AO TOPO
	**********************************************************************/
		//SCRIPT APARECER BOTAO VOLTAR AO TOPO
	  	$(window).bind('scroll', function () {
	       
	       var alturaScroll = $(window).scrollTop()
	       if (alturaScroll > 400) {
	            $(".backToTop").fadeIn();
	       }else{
	            $(".backToTop").fadeOut();

	       }
	    });

	  	//SCRIPT VOLTAR AO TOPO
	  	$(document).ready(function() {
		$('#subir').click(function(){
			$('html, body').animate({scrollTop:0}, 'fast');
			
			});
		});

  	/**********************************************************************
		SCRIPTS ABRIR FORMULÁRIO DE PARCERIA
	**********************************************************************/
		
		$( ".abrirFormularioParceria" ).click(function() {
	  		$(".pg-parceria").fadeIn();
	  		$("body").addClass('travarScroll');
	  	});

	/**********************************************************************
		SCRIPTS FECHAR FORMULÁRIO DE PARCERIA
	**********************************************************************/
		$( ".fecharModalFormularioParceria" ).click(function() {
	  		$(".pg-parceria").fadeOut();
	  		$("body").removeClass('travarScroll');
	  	});

	/**********************************************************************
		SCRIPTS PARA MOSTRAR O MENU FIXO APÓS O SCROLL
	**********************************************************************/
	 
	    $(window).bind('scroll', function () {
			var alturaDaTela;
			alturaDaTela = $(window).height();
	       	var alturaScroll = $(window).scrollTop();
		     
	       	if (alturaScroll > alturaDaTela) {
	       	 	$(".topo").fadeIn('fast');
	       	}else{
	     		$(".topo").fadeOut('fast');
	       	}
	    });     


	/**********************************************************************
		SCRIPTS ANIMAÇÃO DAS ANCORAS
	**********************************************************************/
		$('.scrollTop a').click(function() {
			$(".navbar-collapse").removeClass('in');
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

	/**********************************************************************
		SCRIPTS PARA REMOVER O CLICK DO 'a' 
	**********************************************************************/

		$( ".abrirModalPaceiro .abrirFormularioParceria" ).click(function(e) {
				event.preventDefault();
	 	});

	/**********************************************************************
		SCRIPTS PARA REMOVER O CLICK DO 'a' 
	**********************************************************************/
	
	/**********************************************************************
		SCRIPTS AO  CLICAR EM CONFERIR
	**********************************************************************/

	$(document).ready(function(){
    $(".confiraAqui").click(function(){
    	setTimeout(function(){
      $(".post94").trigger('click');
 		},1000);
       });
    });

	
});
