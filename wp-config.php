<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */



// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define('DB_NAME', 'projetos_amandakaroline_portfolio');



/** MySQL database username */

define('DB_USER', 'root');



/** MySQL database password */

define('DB_PASSWORD', '');



/** MySQL hostname */

define('DB_HOST', 'localhost');



/** Database Charset to use in creating database tables. */

define('DB_CHARSET', 'utf8mb4');



/** The Database Collate type. Don't change this if in doubt. */

define('DB_COLLATE', '');



/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define('AUTH_KEY',         '0ha9emH4hc~`g(8b#J86&m{3;HIRc,,VLV^2Dw*A}deS[~exvG#9kSm-!q%:Em}}');

define('SECURE_AUTH_KEY',  'Vg+RbLeHTix}J25v4B@ucP#3$Y-Oy^h ,y;o}K|TS_~5&md,xZ?,p:OqK89qd`@1');

define('LOGGED_IN_KEY',    '/rhc,.<j)d@6;_gy#`w#+a77x1Q}^Um;~ <byiS iB!PUHjkm9kNehZj<5#d1YM5');

define('NONCE_KEY',        '#Z#MOqyzYn:]JbLuN%NvP2?I8fr+LJCO~8KgZu; 7{*%&BaO.3O4x0El31!$svAs');

define('AUTH_SALT',        'bivY?4?lz3{y0e~615$9w04P&teJMu1!&k^O!kIYIx{at,//oQX%xoKNJMjxCw9K');

define('SECURE_AUTH_SALT', '`B#{.fZkm8b.9]C>=ZRb-V]6wuUYYrX?55K(`ovr#Y`pRf1||heJ7NgvW_M+>vy{');

define('LOGGED_IN_SALT',   '0p5#KjT{AYyt?+n6;.hHC,u[EfG*7RIS4){TloKZABY<YA@r.?D0QMHJt[)CF7[t');

define('NONCE_SALT',       'f7a$F+fa_ J=YXsX9+|F{kh5oj+zI hiS~HKT`k-tN()V$dYjeeBRPY4$V#W{QYC');



/**#@-*/



/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix  = 'ak_';



/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define('WP_DEBUG', false);



/* That's all, stop editing! Happy blogging. */



/** Absolute path to the WordPress directory. */

if ( !defined('ABSPATH') )

	define('ABSPATH', dirname(__FILE__) . '/');



/** Sets up WordPress vars and included files. */

require_once(ABSPATH . 'wp-settings.php');

