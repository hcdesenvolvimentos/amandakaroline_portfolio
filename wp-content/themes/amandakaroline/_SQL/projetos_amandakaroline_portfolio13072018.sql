-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 13-Jul-2018 às 18:55
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_amandakaroline_portfolio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_aiowps_events`
--

CREATE TABLE `ak_aiowps_events` (
  `id` bigint(20) NOT NULL,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_aiowps_failed_logins`
--

CREATE TABLE `ak_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_aiowps_global_meta`
--

CREATE TABLE `ak_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_aiowps_login_activity`
--

CREATE TABLE `ak_aiowps_login_activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_aiowps_login_activity`
--

INSERT INTO `ak_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'amandakaroline', '2018-07-05 15:34:47', '2018-07-10 14:00:45', '::1', '', ''),
(2, 1, 'amandakaroline', '2018-07-05 17:01:14', '2018-07-10 14:00:45', '::1', '', ''),
(3, 1, 'amandakaroline', '2018-07-06 09:20:50', '2018-07-10 14:00:45', '::1', '', ''),
(4, 1, 'amandakaroline', '2018-07-06 09:35:14', '2018-07-10 14:00:45', '::1', '', ''),
(5, 1, 'amandakaroline', '2018-07-10 09:32:53', '2018-07-10 14:00:45', '::1', '', ''),
(6, 1, 'amandakaroline', '2018-07-10 15:41:39', '2018-07-11 13:52:22', '::1', '', ''),
(7, 1, 'amandakaroline', '2018-07-11 13:29:36', '2018-07-11 13:52:22', '::1', '', ''),
(8, 1, 'amandakaroline', '2018-07-11 13:53:08', '2018-07-11 14:47:46', '::1', '', ''),
(9, 1, 'amandakaroline', '2018-07-11 14:39:00', '2018-07-11 14:47:46', '::1', '', ''),
(10, 1, 'amandakaroline', '2018-07-11 14:52:11', '2018-07-13 11:15:52', '::1', '', ''),
(11, 1, 'amandakaroline', '2018-07-12 18:29:23', '2018-07-13 11:15:52', '::1', '', ''),
(12, 1, 'amandakaroline', '2018-07-13 11:29:42', '0000-00-00 00:00:00', '::1', '', ''),
(13, 1, 'amandakaroline', '2018-07-13 12:28:23', '0000-00-00 00:00:00', '::1', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_aiowps_login_lockdown`
--

CREATE TABLE `ak_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_aiowps_permanent_block`
--

CREATE TABLE `ak_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_commentmeta`
--

CREATE TABLE `ak_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_comments`
--

CREATE TABLE `ak_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_comments`
--

INSERT INTO `ak_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-06-19 15:44:03', '2018-06-19 18:44:03', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_links`
--

CREATE TABLE `ak_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_options`
--

CREATE TABLE `ak_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_options`
--

INSERT INTO `ak_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/amandakaroline_portfolio', 'yes'),
(2, 'home', 'http://localhost/projetos/amandakaroline_portfolio', 'yes'),
(3, 'blogname', 'Amanda Karoline', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'carolinoamandacs@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:9:{i:0;s:35:"redux-framework/redux-framework.php";i:1;s:51:"all-in-one-wp-security-and-firewall/wp-security.php";i:2;s:43:"base-amandakaroline/base-amandakaroline.php";i:3;s:36:"contact-form-7/wp-contact-form-7.php";i:4;s:32:"disqus-comment-system/disqus.php";i:5;s:21:"meta-box/meta-box.php";i:6;s:37:"post-types-order/post-types-order.php";i:7;s:24:"wordpress-seo/wp-seo.php";i:8;s:29:"wp-mail-smtp/wp_mail_smtp.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'amandakaroline', 'yes'),
(41, 'stylesheet', 'amandakaroline', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '72', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'initial_db_version', '38590', 'yes'),
(93, 'ak_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:20:"wpseo_manage_options";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:35:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:13:"wpseo_manager";a:2:{s:4:"name";s:11:"SEO Manager";s:12:"capabilities";a:37:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;s:28:"wpseo_edit_advanced_metadata";b:1;s:20:"wpseo_manage_options";b:1;}}s:12:"wpseo_editor";a:2:{s:4:"name";s:10:"SEO Editor";s:12:"capabilities";a:36:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;s:28:"wpseo_edit_advanced_metadata";b:1;}}}', 'yes'),
(94, 'fresh_site', '0', 'yes'),
(95, 'WPLANG', 'pt_BR', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'cron', 'a:8:{i:1531509012;a:1:{s:24:"aiowps_hourly_cron_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1531511044;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1531513653;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1531550644;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1531581012;a:1:{s:23:"aiowps_daily_cron_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1531581404;a:1:{s:19:"wpseo-reindex-links";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1531593906;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(559, '_site_transient_timeout_theme_roots', '1531509318', 'no'),
(560, '_site_transient_theme_roots', 'a:1:{s:14:"amandakaroline";s:7:"/themes";}', 'no'),
(463, 'wpseo_sitemap_111_cache_validator', '5jI9G', 'no'),
(213, 'wpcf7', 'a:2:{s:7:"version";s:5:"5.0.2";s:13:"bulk_validate";a:4:{s:9:"timestamp";d:1530792903;s:7:"version";s:5:"5.0.2";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(214, 'disqus_sync_token', '8e284f1b4c44787d09441ba79e774d82', 'yes'),
(238, 'rewrite_rules', 'a:207:{s:19:"sitemap_index\\.xml$";s:19:"index.php?sitemap=1";s:31:"([^/]+?)-sitemap([0-9]+)?\\.xml$";s:51:"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]";s:24:"([a-z]+)?-?sitemap\\.xsl$";s:25:"index.php?xsl=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:12:"portfolio/?$";s:29:"index.php?post_type=portfolio";s:42:"portfolio/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=portfolio&feed=$matches[1]";s:37:"portfolio/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=portfolio&feed=$matches[1]";s:29:"portfolio/page/([0-9]{1,})/?$";s:47:"index.php?post_type=portfolio&paged=$matches[1]";s:14:"depoimentos/?$";s:30:"index.php?post_type=depoimento";s:44:"depoimentos/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?post_type=depoimento&feed=$matches[1]";s:39:"depoimentos/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?post_type=depoimento&feed=$matches[1]";s:31:"depoimentos/page/([0-9]{1,})/?$";s:48:"index.php?post_type=depoimento&paged=$matches[1]";s:12:"parceiros/?$";s:28:"index.php?post_type=parceria";s:42:"parceiros/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=parceria&feed=$matches[1]";s:37:"parceiros/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=parceria&feed=$matches[1]";s:29:"parceiros/page/([0-9]{1,})/?$";s:46:"index.php?post_type=parceria&paged=$matches[1]";s:11:"destaque/?$";s:28:"index.php?post_type=destaque";s:41:"destaque/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=destaque&feed=$matches[1]";s:36:"destaque/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=destaque&feed=$matches[1]";s:28:"destaque/page/([0-9]{1,})/?$";s:46:"index.php?post_type=destaque&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:37:"portfolio/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"portfolio/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"portfolio/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"portfolio/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"portfolio/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:43:"portfolio/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:26:"portfolio/([^/]+)/embed/?$";s:42:"index.php?portfolio=$matches[1]&embed=true";s:30:"portfolio/([^/]+)/trackback/?$";s:36:"index.php?portfolio=$matches[1]&tb=1";s:50:"portfolio/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?portfolio=$matches[1]&feed=$matches[2]";s:45:"portfolio/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?portfolio=$matches[1]&feed=$matches[2]";s:38:"portfolio/([^/]+)/page/?([0-9]{1,})/?$";s:49:"index.php?portfolio=$matches[1]&paged=$matches[2]";s:45:"portfolio/([^/]+)/comment-page-([0-9]{1,})/?$";s:49:"index.php?portfolio=$matches[1]&cpage=$matches[2]";s:34:"portfolio/([^/]+)(?:/([0-9]+))?/?$";s:48:"index.php?portfolio=$matches[1]&page=$matches[2]";s:26:"portfolio/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:36:"portfolio/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:56:"portfolio/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"portfolio/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"portfolio/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:32:"portfolio/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:39:"depoimentos/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:49:"depoimentos/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:69:"depoimentos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"depoimentos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:64:"depoimentos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:45:"depoimentos/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:28:"depoimentos/([^/]+)/embed/?$";s:43:"index.php?depoimento=$matches[1]&embed=true";s:32:"depoimentos/([^/]+)/trackback/?$";s:37:"index.php?depoimento=$matches[1]&tb=1";s:52:"depoimentos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?depoimento=$matches[1]&feed=$matches[2]";s:47:"depoimentos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?depoimento=$matches[1]&feed=$matches[2]";s:40:"depoimentos/([^/]+)/page/?([0-9]{1,})/?$";s:50:"index.php?depoimento=$matches[1]&paged=$matches[2]";s:47:"depoimentos/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?depoimento=$matches[1]&cpage=$matches[2]";s:36:"depoimentos/([^/]+)(?:/([0-9]+))?/?$";s:49:"index.php?depoimento=$matches[1]&page=$matches[2]";s:28:"depoimentos/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:38:"depoimentos/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:58:"depoimentos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"depoimentos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:53:"depoimentos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:34:"depoimentos/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:37:"parceiros/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"parceiros/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"parceiros/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"parceiros/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"parceiros/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:43:"parceiros/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:26:"parceiros/([^/]+)/embed/?$";s:41:"index.php?parceria=$matches[1]&embed=true";s:30:"parceiros/([^/]+)/trackback/?$";s:35:"index.php?parceria=$matches[1]&tb=1";s:50:"parceiros/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?parceria=$matches[1]&feed=$matches[2]";s:45:"parceiros/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?parceria=$matches[1]&feed=$matches[2]";s:38:"parceiros/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?parceria=$matches[1]&paged=$matches[2]";s:45:"parceiros/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?parceria=$matches[1]&cpage=$matches[2]";s:34:"parceiros/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?parceria=$matches[1]&page=$matches[2]";s:26:"parceiros/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:36:"parceiros/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:56:"parceiros/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"parceiros/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"parceiros/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:32:"parceiros/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:36:"destaque/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"destaque/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:42:"destaque/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:25:"destaque/([^/]+)/embed/?$";s:41:"index.php?destaque=$matches[1]&embed=true";s:29:"destaque/([^/]+)/trackback/?$";s:35:"index.php?destaque=$matches[1]&tb=1";s:49:"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?destaque=$matches[1]&feed=$matches[2]";s:44:"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?destaque=$matches[1]&feed=$matches[2]";s:37:"destaque/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?destaque=$matches[1]&paged=$matches[2]";s:44:"destaque/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?destaque=$matches[1]&cpage=$matches[2]";s:33:"destaque/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?destaque=$matches[1]&page=$matches[2]";s:25:"destaque/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"destaque/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:31:"destaque/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:59:"categoria-destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:56:"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]";s:54:"categoria-destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:56:"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]";s:35:"categoria-destaque/([^/]+)/embed/?$";s:50:"index.php?categoriaDestaque=$matches[1]&embed=true";s:47:"categoria-destaque/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?categoriaDestaque=$matches[1]&paged=$matches[2]";s:29:"categoria-destaque/([^/]+)/?$";s:39:"index.php?categoriaDestaque=$matches[1]";s:36:"amn_smtp/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"amn_smtp/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"amn_smtp/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"amn_smtp/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"amn_smtp/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:42:"amn_smtp/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:25:"amn_smtp/([^/]+)/embed/?$";s:41:"index.php?amn_smtp=$matches[1]&embed=true";s:29:"amn_smtp/([^/]+)/trackback/?$";s:35:"index.php?amn_smtp=$matches[1]&tb=1";s:37:"amn_smtp/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?amn_smtp=$matches[1]&paged=$matches[2]";s:44:"amn_smtp/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?amn_smtp=$matches[1]&cpage=$matches[2]";s:33:"amn_smtp/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?amn_smtp=$matches[1]&page=$matches[2]";s:25:"amn_smtp/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"amn_smtp/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"amn_smtp/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"amn_smtp/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"amn_smtp/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:31:"amn_smtp/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=72&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(217, 'ReduxFrameworkPlugin', 'a:1:{s:4:"demo";b:0;}', 'yes'),
(218, 'r_notice_data', '\n{"type":"redux-message","title":"<strong>The Ultimate Redux Extensions Bundle!<\\/strong><br\\/>\\r\\n\\r\\n","message":"Attention Redux Developers!  Due to popular demand, we are extending the availability of the Ultimate Redux Extension Bundle, which includes <strong>all<\\/strong> of our extensions at over 40% off!  Grab you\'re copy today at <a href=\\"https:\\/\\/reduxframework.com\\/extension\\/ultimate-bundle\\">https:\\/\\/reduxframework.com\\/extension\\/ultimate-bundle<\\/a>","color":"rgba(0,162,227,1)"}\n', 'yes'),
(200, 'aiowpsec_db_version', '1.9', 'yes'),
(201, 'aio_wp_security_configs', 'a:86:{s:19:"aiowps_enable_debug";s:0:"";s:36:"aiowps_remove_wp_generator_meta_info";s:0:"";s:25:"aiowps_prevent_hotlinking";s:0:"";s:28:"aiowps_enable_login_lockdown";s:0:"";s:28:"aiowps_allow_unlock_requests";s:0:"";s:25:"aiowps_max_login_attempts";s:1:"3";s:24:"aiowps_retry_time_period";s:1:"5";s:26:"aiowps_lockout_time_length";s:2:"60";s:28:"aiowps_set_generic_login_msg";s:0:"";s:26:"aiowps_enable_email_notify";s:0:"";s:20:"aiowps_email_address";s:26:"carolinoamandacs@gmail.com";s:27:"aiowps_enable_forced_logout";s:0:"";s:25:"aiowps_logout_time_period";s:2:"60";s:39:"aiowps_enable_invalid_username_lockdown";s:0:"";s:43:"aiowps_instantly_lockout_specific_usernames";a:0:{}s:32:"aiowps_unlock_request_secret_key";s:20:"odqfxh0wcolcknuaonkn";s:35:"aiowps_lockdown_enable_whitelisting";s:0:"";s:36:"aiowps_lockdown_allowed_ip_addresses";s:0:"";s:26:"aiowps_enable_whitelisting";s:0:"";s:27:"aiowps_allowed_ip_addresses";s:0:"";s:27:"aiowps_enable_login_captcha";s:0:"";s:34:"aiowps_enable_custom_login_captcha";s:0:"";s:31:"aiowps_enable_woo_login_captcha";s:0:"";s:34:"aiowps_enable_woo_register_captcha";s:0:"";s:25:"aiowps_captcha_secret_key";s:20:"8d5xd2d2oenj8nrcni4x";s:42:"aiowps_enable_manual_registration_approval";s:0:"";s:39:"aiowps_enable_registration_page_captcha";s:0:"";s:35:"aiowps_enable_registration_honeypot";s:0:"";s:27:"aiowps_enable_random_prefix";s:0:"";s:31:"aiowps_enable_automated_backups";s:0:"";s:26:"aiowps_db_backup_frequency";s:1:"4";s:25:"aiowps_db_backup_interval";s:1:"2";s:26:"aiowps_backup_files_stored";s:1:"2";s:32:"aiowps_send_backup_email_address";s:0:"";s:27:"aiowps_backup_email_address";s:26:"carolinoamandacs@gmail.com";s:27:"aiowps_disable_file_editing";s:0:"";s:37:"aiowps_prevent_default_wp_file_access";s:0:"";s:22:"aiowps_system_log_file";s:9:"error_log";s:26:"aiowps_enable_blacklisting";s:0:"";s:26:"aiowps_banned_ip_addresses";s:0:"";s:28:"aiowps_enable_basic_firewall";s:0:"";s:31:"aiowps_enable_pingback_firewall";s:0:"";s:38:"aiowps_disable_xmlrpc_pingback_methods";s:0:"";s:34:"aiowps_block_debug_log_file_access";s:0:"";s:26:"aiowps_disable_index_views";s:0:"";s:30:"aiowps_disable_trace_and_track";s:0:"";s:28:"aiowps_forbid_proxy_comments";s:0:"";s:29:"aiowps_deny_bad_query_strings";s:0:"";s:34:"aiowps_advanced_char_string_filter";s:0:"";s:25:"aiowps_enable_5g_firewall";s:0:"";s:25:"aiowps_enable_6g_firewall";s:0:"";s:26:"aiowps_enable_custom_rules";s:0:"";s:32:"aiowps_place_custom_rules_at_top";s:0:"";s:19:"aiowps_custom_rules";s:0:"";s:25:"aiowps_enable_404_logging";s:0:"";s:28:"aiowps_enable_404_IP_lockout";s:0:"";s:30:"aiowps_404_lockout_time_length";s:2:"60";s:28:"aiowps_404_lock_redirect_url";s:16:"http://127.0.0.1";s:31:"aiowps_enable_rename_login_page";s:0:"";s:28:"aiowps_enable_login_honeypot";s:0:"";s:43:"aiowps_enable_brute_force_attack_prevention";s:0:"";s:30:"aiowps_brute_force_secret_word";s:0:"";s:24:"aiowps_cookie_brute_test";s:0:"";s:44:"aiowps_cookie_based_brute_force_redirect_url";s:16:"http://127.0.0.1";s:59:"aiowps_brute_force_attack_prevention_pw_protected_exception";s:0:"";s:51:"aiowps_brute_force_attack_prevention_ajax_exception";s:0:"";s:19:"aiowps_site_lockout";s:0:"";s:23:"aiowps_site_lockout_msg";s:0:"";s:30:"aiowps_enable_spambot_blocking";s:0:"";s:29:"aiowps_enable_comment_captcha";s:0:"";s:31:"aiowps_enable_autoblock_spam_ip";s:0:"";s:33:"aiowps_spam_ip_min_comments_block";s:0:"";s:33:"aiowps_enable_bp_register_captcha";s:0:"";s:35:"aiowps_enable_bbp_new_topic_captcha";s:0:"";s:32:"aiowps_enable_automated_fcd_scan";s:0:"";s:25:"aiowps_fcd_scan_frequency";s:1:"4";s:24:"aiowps_fcd_scan_interval";s:1:"2";s:28:"aiowps_fcd_exclude_filetypes";s:0:"";s:24:"aiowps_fcd_exclude_files";s:0:"";s:26:"aiowps_send_fcd_scan_email";s:0:"";s:29:"aiowps_fcd_scan_email_address";s:26:"carolinoamandacs@gmail.com";s:27:"aiowps_fcds_change_detected";b:0;s:22:"aiowps_copy_protection";s:0:"";s:40:"aiowps_prevent_site_display_inside_frame";s:0:"";s:32:"aiowps_prevent_users_enumeration";s:0:"";s:25:"aiowps_ip_retrieve_method";s:1:"0";}', 'yes'),
(279, 'configuracao-transients', 'a:2:{s:14:"changed_values";a:1:{s:22:"opt_imagemEstatisticas";a:9:{s:3:"url";s:87:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/dados.png";s:2:"id";s:2:"69";s:6:"height";s:4:"1582";s:5:"width";s:4:"1920";s:9:"thumbnail";s:95:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/dados-150x150.png";s:5:"title";s:5:"dados";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}}s:9:"last_save";i:1531330355;}', 'yes'),
(287, 'wpseo_sitemap_1_cache_validator', '6ARGO', 'no'),
(288, 'wpseo_sitemap_destaque_cache_validator', '6AMJR', 'no'),
(290, 'wpseo_sitemap_post_cache_validator', '6ARGT', 'no'),
(291, 'wpseo_sitemap_servico_cache_validator', '6nSDm', 'no'),
(295, 'wpseo_sitemap_depoimento_cache_validator', '6AMJN', 'no'),
(296, 'wpseo_sitemap_attachment_cache_validator', '62zvq', 'no'),
(458, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:1:{i:0;i:2;}}', 'yes'),
(477, 'wpseo_sitemap_135_cache_validator', '5CjNZ', 'no'),
(478, 'wpseo_sitemap_137_cache_validator', '5CjO8', 'no'),
(471, 'wpseo_sitemap_129_cache_validator', '5CjNc', 'no'),
(472, 'wpseo_sitemap_130_cache_validator', '5CjNk', 'no'),
(473, 'wpseo_sitemap_131_cache_validator', '5CjNs', 'no'),
(474, 'wpseo_sitemap_132_cache_validator', '5CjNB', 'no'),
(475, 'wpseo_sitemap_133_cache_validator', '5CjNK', 'no'),
(476, 'wpseo_sitemap_134_cache_validator', '5CjNS', 'no'),
(352, 'wpseo_sitemap_sessao-inicial_cache_validator', '4rctS', 'no'),
(362, 'wpseo_sitemap_portfolio_cache_validator', '6lZAp', 'no'),
(363, 'wpseo_sitemap_banner-destaque_cache_validator', '6fzqg', 'no'),
(385, 'wpseo_sitemap_page_cache_validator', '5B3UM', 'no'),
(534, '_transient_timeout_plugin_slugs', '1531517380', 'no'),
(535, '_transient_plugin_slugs', 'a:9:{i:0;s:51:"all-in-one-wp-security-and-firewall/wp-security.php";i:1;s:43:"base-amandakaroline/base-amandakaroline.php";i:2;s:36:"contact-form-7/wp-contact-form-7.php";i:3;s:32:"disqus-comment-system/disqus.php";i:4;s:21:"meta-box/meta-box.php";i:5;s:37:"post-types-order/post-types-order.php";i:6;s:35:"redux-framework/redux-framework.php";i:7;s:29:"wp-mail-smtp/wp_mail_smtp.php";i:8;s:24:"wordpress-seo/wp-seo.php";}', 'no'),
(457, 'wpseo_sitemap_nav_menu_item_cache_validator', '5D74W', 'no'),
(132, 'can_compress_scripts', '1', 'no'),
(141, 'recently_activated', 'a:0:{}', 'yes'),
(160, 'theme_mods_twentyseventeen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1529496996;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(161, 'current_theme', 'Amanda Karoline', 'yes'),
(162, 'theme_mods_amandakaroline', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:6:"menu-1";i:2;}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(163, 'theme_switched', '', 'yes'),
(231, '_transient_timeout_wpseo_link_table_inaccessible', '1562339804', 'no'),
(232, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(233, '_transient_timeout_wpseo_meta_table_inaccessible', '1562339804', 'no'),
(234, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(531, '_site_transient_browser_a54eb83090ed984332f4eca22d3ec5e4', 'a:10:{s:4:"name";s:6:"Chrome";s:7:"version";s:12:"67.0.3396.99";s:8:"platform";s:7:"Windows";s:10:"update_url";s:29:"https://www.google.com/chrome";s:7:"img_src";s:43:"http://s.w.org/images/browsers/chrome.png?1";s:11:"img_src_ssl";s:44:"https://s.w.org/images/browsers/chrome.png?1";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;s:6:"mobile";b:0;}', 'no'),
(532, '_transient_timeout_wpseo-statistics-totals', '1531517372', 'no'),
(533, '_transient_wpseo-statistics-totals', 'a:1:{i:1;a:2:{s:6:"scores";a:1:{i:0;a:4:{s:8:"seo_rank";s:2:"na";s:5:"label";s:46:"Posts <strong>without</strong> a focus keyword";s:5:"count";s:1:"1";s:4:"link";s:127:"http://localhost/projetos/amandakaroline_portfolio/wp-admin/edit.php?post_status=publish&#038;post_type=post&#038;seo_filter=na";}}s:8:"division";a:5:{s:3:"bad";i:0;s:2:"ok";i:0;s:4:"good";i:0;s:2:"na";i:1;s:7:"noindex";i:0;}}}', 'no'),
(327, 'wpseo_sitemap_parceria_cache_validator', '5n1Cl', 'no'),
(329, 'wpseo_sitemap_servicos_cache_validator', '4sdFU', 'no'),
(219, 'redux_blast', '1530803758', 'yes'),
(220, 'redux_version_upgraded_from', '3.6.9', 'yes'),
(221, 'redux_demo', 'a:70:{s:12:"opt-checkbox";s:1:"1";s:15:"opt-multi-check";a:3:{i:1;s:1:"1";i:2;s:1:"0";i:3;s:1:"0";}s:9:"opt-radio";s:1:"2";s:12:"opt-sortable";a:3:{s:8:"Text One";s:6:"Item 1";s:8:"Text Two";s:6:"Item 2";s:10:"Text Three";s:6:"Item 3";}s:18:"opt-check-sortable";a:3:{s:3:"cb1";b:0;s:3:"cb2";b:1;s:3:"cb3";b:0;}s:12:"text-example";s:12:"Default Text";s:17:"text-example-hint";s:12:"Default Text";s:12:"opt-textarea";s:12:"Default Text";s:10:"opt-editor";s:27:"Powered by Redux Framework.";s:15:"opt-editor-tiny";s:27:"Powered by Redux Framework.";s:18:"opt-ace-editor-css";s:29:"#header{\n   margin: 0 auto;\n}";s:17:"opt-ace-editor-js";s:39:"jQuery(document).ready(function(){\n\n});";s:18:"opt-ace-editor-php";s:28:"<?php\n    echo "PHP String";";s:15:"opt-color-title";s:7:"#000000";s:16:"opt-color-footer";s:7:"#dd9933";s:16:"opt-color-header";a:2:{s:4:"from";s:7:"#1e73be";s:2:"to";s:7:"#00897e";}s:14:"opt-color-rgba";a:2:{s:5:"color";s:7:"#7e33dd";s:5:"alpha";s:2:".8";}s:14:"opt-link-color";a:3:{s:7:"regular";s:4:"#aaa";s:5:"hover";s:4:"#bbb";s:6:"active";s:4:"#ccc";}s:17:"opt-palette-color";s:3:"red";s:17:"opt-header-border";a:6:{s:12:"border-color";s:7:"#1e73be";s:12:"border-style";s:5:"solid";s:10:"border-top";s:3:"3px";s:12:"border-right";s:3:"3px";s:13:"border-bottom";s:3:"3px";s:11:"border-left";s:3:"3px";}s:26:"opt-header-border-expanded";a:6:{s:12:"border-color";s:7:"#1e73be";s:12:"border-style";s:5:"solid";s:10:"border-top";s:3:"3px";s:12:"border-right";s:3:"3px";s:13:"border-bottom";s:3:"3px";s:11:"border-left";s:3:"3px";}s:14:"opt-dimensions";a:2:{s:5:"width";i:200;s:6:"height";i:100;}s:20:"opt-dimensions-width";a:2:{s:5:"width";i:200;s:6:"height";i:100;}s:11:"opt-spacing";a:4:{s:10:"margin-top";s:3:"1px";s:12:"margin-right";s:3:"2px";s:13:"margin-bottom";s:3:"3px";s:11:"margin-left";s:3:"4px";}s:20:"opt-spacing-expanded";a:4:{s:10:"margin-top";s:3:"1px";s:12:"margin-right";s:3:"2px";s:13:"margin-bottom";s:3:"3px";s:11:"margin-left";s:3:"4px";}s:9:"opt-media";a:1:{s:3:"url";s:52:"http://s.wordpress.org/style/images/codeispoetry.png";}s:14:"opt-button-set";s:1:"2";s:20:"opt-button-set-multi";a:2:{i:0;s:1:"2";i:1;s:1:"3";}s:9:"switch-on";b:1;s:10:"switch-off";b:0;s:13:"switch-parent";i:0;s:13:"switch-child1";b:0;s:13:"switch-child2";b:0;s:10:"opt-select";s:1:"2";s:21:"opt-select-stylesheet";s:11:"default.css";s:19:"opt-select-optgroup";s:1:"2";s:16:"opt-multi-select";a:2:{i:0;s:1:"2";i:1;s:1:"3";}s:23:"opt-image-select-layout";s:1:"2";s:12:"opt-patterns";i:0;s:16:"opt-image-select";s:1:"2";s:11:"opt-presets";i:0;s:22:"opt-select_image-field";s:125:"http://localhost/projetos/amandakaroline_portfolio/wp-content/plugins/redux-framework/ReduxCore/../sample/presets/preset2.png";s:16:"opt-select-image";s:129:"http://localhost/projetos/amandakaroline_portfolio/wp-content/plugins/redux-framework/ReduxCore/../sample/patterns/triangular.png";s:16:"opt-slider-label";i:250;s:15:"opt-slider-text";i:75;s:17:"opt-slider-select";a:2:{i:1;i:100;i:2;i:300;}s:16:"opt-slider-float";d:0.5;s:11:"opt-spinner";s:2:"40";s:19:"opt-typography-body";a:4:{s:5:"color";s:7:"#dd9933";s:9:"font-size";s:4:"30px";s:11:"font-family";s:26:"Arial,Helvetica,sans-serif";s:11:"font-weight";s:6:"Normal";}s:14:"opt-typography";a:6:{s:5:"color";s:4:"#333";s:10:"font-style";s:3:"700";s:11:"font-family";s:4:"Abel";s:6:"google";b:1;s:9:"font-size";s:4:"33px";s:11:"line-height";s:4:"40px";}s:19:"opt-homepage-layout";a:3:{s:7:"enabled";a:4:{s:10:"highlights";s:10:"Highlights";s:6:"slider";s:6:"Slider";s:10:"staticpage";s:11:"Static Page";s:8:"services";s:8:"Services";}s:8:"disabled";a:0:{}s:6:"backup";a:0:{}}s:21:"opt-homepage-layout-2";a:2:{s:8:"disabled";a:2:{s:10:"highlights";s:10:"Highlights";s:6:"slider";s:6:"Slider";}s:7:"enabled";a:2:{s:10:"staticpage";s:11:"Static Page";s:8:"services";s:8:"Services";}}s:14:"opt-text-email";s:13:"test@test.com";s:12:"opt-text-url";s:25:"http://reduxframework.com";s:16:"opt-text-numeric";s:1:"0";s:22:"opt-text-comma-numeric";s:1:"0";s:25:"opt-text-no-special-chars";s:1:"0";s:20:"opt-text-str_replace";s:20:"This is the default.";s:21:"opt-text-preg_replace";s:1:"0";s:24:"opt-text-custom_validate";s:1:"0";s:20:"opt-textarea-no-html";s:27:"No HTML is allowed in here.";s:17:"opt-textarea-html";s:24:"HTML is allowed in here.";s:22:"opt-textarea-some-html";s:36:"<p>Some HTML is allowed in here.</p>";s:18:"opt-required-basic";b:0;s:19:"opt-required-nested";b:0;s:29:"opt-required-nested-buttonset";s:11:"button-text";s:19:"opt-required-select";s:10:"no-sidebar";s:32:"opt-required-select-left-sidebar";s:0:"";s:33:"opt-required-select-right-sidebar";s:0:"";s:19:"opt-customizer-only";s:1:"2";}', 'yes'),
(222, 'redux_demo-transients', 'a:2:{s:14:"changed_values";a:0:{}s:9:"last_save";i:1530803760;}', 'yes'),
(223, 'wp_mail_smtp_initial_version', '1.3.3', 'no'),
(224, 'wp_mail_smtp_version', '1.3.3', 'no'),
(225, 'wp_mail_smtp', 'a:2:{s:4:"mail";a:6:{s:10:"from_email";s:26:"carolinoamandacs@gmail.com";s:9:"from_name";s:15:"Amanda Karoline";s:6:"mailer";s:4:"mail";s:11:"return_path";b:0;s:16:"from_email_force";b:0;s:15:"from_name_force";b:0;}s:4:"smtp";a:1:{s:7:"autotls";b:1;}}', 'no'),
(226, '_amn_smtp_last_checked', '1531440000', 'yes'),
(227, 'wpseo', 'a:19:{s:15:"ms_defaults_set";b:0;s:7:"version";s:5:"7.7.3";s:20:"disableadvanced_meta";b:1;s:19:"onpage_indexability";b:1;s:11:"baiduverify";s:0:"";s:12:"googleverify";s:0:"";s:8:"msverify";s:0:"";s:12:"yandexverify";s:0:"";s:9:"site_type";s:0:"";s:20:"has_multiple_authors";s:0:"";s:16:"environment_type";s:0:"";s:23:"content_analysis_active";b:1;s:23:"keyword_analysis_active";b:1;s:21:"enable_admin_bar_menu";b:1;s:26:"enable_cornerstone_content";b:1;s:18:"enable_xml_sitemap";b:1;s:24:"enable_text_link_counter";b:1;s:22:"show_onboarding_notice";b:1;s:18:"first_activated_on";i:1530803803;}', 'yes');
INSERT INTO `ak_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(228, 'wpseo_titles', 'a:65:{s:10:"title_test";i:0;s:17:"forcerewritetitle";b:0;s:9:"separator";s:7:"sc-dash";s:16:"title-home-wpseo";s:42:"%%sitename%% %%page%% %%sep%% %%sitedesc%%";s:18:"title-author-wpseo";s:41:"%%name%%, Author at %%sitename%% %%page%%";s:19:"title-archive-wpseo";s:38:"%%date%% %%page%% %%sep%% %%sitename%%";s:18:"title-search-wpseo";s:63:"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%";s:15:"title-404-wpseo";s:35:"Page not found %%sep%% %%sitename%%";s:19:"metadesc-home-wpseo";s:0:"";s:21:"metadesc-author-wpseo";s:0:"";s:22:"metadesc-archive-wpseo";s:0:"";s:9:"rssbefore";s:0:"";s:8:"rssafter";s:53:"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.";s:20:"noindex-author-wpseo";b:0;s:28:"noindex-author-noposts-wpseo";b:1;s:21:"noindex-archive-wpseo";b:1;s:14:"disable-author";b:0;s:12:"disable-date";b:0;s:19:"disable-post_format";b:0;s:18:"disable-attachment";b:1;s:23:"is-media-purge-relevant";b:0;s:20:"breadcrumbs-404crumb";s:25:"Error 404: Page not found";s:29:"breadcrumbs-display-blog-page";b:1;s:20:"breadcrumbs-boldlast";b:0;s:25:"breadcrumbs-archiveprefix";s:12:"Archives for";s:18:"breadcrumbs-enable";b:0;s:16:"breadcrumbs-home";s:4:"Home";s:18:"breadcrumbs-prefix";s:0:"";s:24:"breadcrumbs-searchprefix";s:16:"You searched for";s:15:"breadcrumbs-sep";s:7:"&raquo;";s:12:"website_name";s:0:"";s:11:"person_name";s:0:"";s:22:"alternate_website_name";s:0:"";s:12:"company_logo";s:0:"";s:12:"company_name";s:0:"";s:17:"company_or_person";s:0:"";s:17:"stripcategorybase";b:0;s:10:"title-post";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:13:"metadesc-post";s:0:"";s:12:"noindex-post";b:0;s:13:"showdate-post";b:0;s:23:"display-metabox-pt-post";b:1;s:10:"title-page";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:13:"metadesc-page";s:0:"";s:12:"noindex-page";b:0;s:13:"showdate-page";b:0;s:23:"display-metabox-pt-page";b:1;s:16:"title-attachment";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:19:"metadesc-attachment";s:0:"";s:18:"noindex-attachment";b:0;s:19:"showdate-attachment";b:0;s:29:"display-metabox-pt-attachment";b:1;s:18:"title-tax-category";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:21:"metadesc-tax-category";s:0:"";s:28:"display-metabox-tax-category";b:1;s:20:"noindex-tax-category";b:0;s:18:"title-tax-post_tag";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:21:"metadesc-tax-post_tag";s:0:"";s:28:"display-metabox-tax-post_tag";b:1;s:20:"noindex-tax-post_tag";b:0;s:21:"title-tax-post_format";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:24:"metadesc-tax-post_format";s:0:"";s:31:"display-metabox-tax-post_format";b:1;s:23:"noindex-tax-post_format";b:1;s:23:"post_types-post-maintax";i:0;}', 'yes'),
(229, 'wpseo_social', 'a:18:{s:13:"facebook_site";s:0:"";s:13:"instagram_url";s:0:"";s:12:"linkedin_url";s:0:"";s:11:"myspace_url";s:0:"";s:16:"og_default_image";s:0:"";s:18:"og_frontpage_title";s:0:"";s:17:"og_frontpage_desc";s:0:"";s:18:"og_frontpage_image";s:0:"";s:9:"opengraph";b:1;s:13:"pinterest_url";s:0:"";s:15:"pinterestverify";s:0:"";s:14:"plus-publisher";s:0:"";s:7:"twitter";b:1;s:12:"twitter_site";s:0:"";s:17:"twitter_card_type";s:19:"summary_large_image";s:11:"youtube_url";s:0:"";s:15:"google_plus_url";s:0:"";s:10:"fbadminapp";s:0:"";}', 'yes'),
(230, 'wpseo_flush_rewrite', '1', 'yes'),
(248, 'category_children', 'a:0:{}', 'yes'),
(278, 'configuracao', 'a:29:{s:8:"last_tab";s:1:"1";s:8:"opt_logo";a:9:{s:3:"url";s:91:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logo-fixa.png";s:2:"id";s:2:"60";s:6:"height";s:3:"629";s:5:"width";s:4:"1200";s:9:"thumbnail";s:99:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logo-fixa-150x150.png";s:5:"title";s:9:"logo-fixa";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:20:"opt_background-color";s:7:"#000000";s:27:"opt_textarea_scripts_header";s:0:"";s:13:"opt_copyright";s:93:"COPYRIGHT © 2018 AMANDA KAROLINE. TODOS OS DIREITOS RESERVADOS. SITE POR HC DESENVOLVIMENTOS";s:27:"opt_textarea_scripts_footer";s:0:"";s:12:"opt_whatsapp";s:1:"#";s:12:"opt_facebook";s:1:"#";s:13:"opt_instagram";s:1:"#";s:11:"opt_youtube";s:1:"#";s:13:"opt_pinterest";s:1:"#";s:29:"opt_fundo_sessao_apresentacao";a:9:{s:3:"url";s:93:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/opacidade-4.png";s:2:"id";s:2:"61";s:6:"height";s:4:"1080";s:5:"width";s:4:"1920";s:9:"thumbnail";s:101:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/opacidade-4-150x150.png";s:5:"title";s:11:"opacidade-4";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:40:"opt_imagemLadoEsquerdoSessaoApresentacao";a:9:{s:3:"url";s:94:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/rosa-na-boca.png";s:2:"id";s:2:"62";s:6:"height";s:3:"787";s:5:"width";s:3:"879";s:9:"thumbnail";s:102:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/rosa-na-boca-150x150.png";s:5:"title";s:12:"rosa-na-boca";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:29:"opt_TituloApresentacaoDireita";s:15:"AMANDA KAROLINE";s:30:"opt_textoApresentacaoPrincipal";s:383:"<p>Olá, meu nome é Amanda Karoline, sou modelo profissional e seja bem vindo ao meu portfólio! Aqui você encontrará grande parte dos meus trabalhos, chegou a hora de ter a oportunidade de fazer o que amo e mostrar que sou boa nisso!</p>\r\n\r\n<p>Modelar, é um pouco mais do que o aspecto da moda.</p>\r\n\r\n<p>É cativar sem falar.Desde já, agradeço a todos, espero que gostem.</p>";s:20:"opt_logoFinalDeTexto";a:9:{s:3:"url";s:94:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logoSobreMim.png";s:2:"id";s:2:"93";s:6:"height";s:2:"65";s:5:"width";s:3:"150";s:9:"thumbnail";s:101:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logoSobreMim-150x65.png";s:5:"title";s:12:"logoSobreMim";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:12:"opt_parallax";a:9:{s:3:"url";s:89:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC0142.jpg";s:2:"id";s:2:"64";s:6:"height";s:4:"3072";s:5:"width";s:4:"4608";s:9:"thumbnail";s:97:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC0142-150x150.jpg";s:5:"title";s:8:"_DSC0142";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:18:"opt_tituloSobreMim";s:9:"Sobre mim";s:23:"opt_textoSobreMimCentro";s:282:"Tenho 19 anos, sou nascida em Pitanga, interior do Paraná, mas atualmente moro na cidade de Guarapuava. Estou cursando Sistemas para Internet na Universidade Tecnológica Federal do Paraná e Pedagogia na Uninter.\r\n\r\nAtualmente sou desenvolvedora web na empresa HC Desenvolvimentos";s:24:"opt_imagemSobreMimCentro";a:9:{s:3:"url";s:103:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/imagem-sobremimCentro.jpg";s:2:"id";s:2:"65";s:6:"height";s:4:"3072";s:5:"width";s:4:"4608";s:9:"thumbnail";s:111:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/imagem-sobremimCentro-150x150.jpg";s:5:"title";s:21:"imagem-sobremimCentro";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:24:"opt_imagemSobreMimParte2";a:9:{s:3:"url";s:90:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC01016.jpg";s:2:"id";s:2:"66";s:6:"height";s:4:"3632";s:5:"width";s:4:"5456";s:9:"thumbnail";s:98:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC01016-150x150.jpg";s:5:"title";s:8:"DSC01016";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:29:"opt_textoSobreMimCentroParte2";s:666:"<p>Iniciei a vida nas passarelas aos 10 anos, onde despertei um grande interesse na carreira, tirar fotos e desfilar sempre foi minha paixão.</p>\r\n\r\n<p>Comecei realizando alguns trabalhos como modelo para algumas lojas, fotografias, maquiagens, entre outros.</p>\r\n\r\n<p>Com o incentivo e apoio da minha família, realizei alguns cursos de modelo em Curitiba e após a conclusão dos mesmos, emiti minha primeira DRT (Delegacia Regional do Trabalho), podendo atuar como modelo em todo território nacional.</p>\r\n\r\n<p>Atualmente trabalho como modelo para a maquiadora conhecida internacionalmente Hayana Sanquetta, onde recentemente realizei um Editorial de Noivas</p>";s:16:"opt_linkSobreMim";s:1:"#";s:22:"opt_imagemEstatisticas";a:9:{s:3:"url";s:89:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/dadosv4.png";s:2:"id";s:3:"141";s:6:"height";s:4:"1582";s:5:"width";s:4:"1920";s:9:"thumbnail";s:97:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/dadosv4-150x150.png";s:5:"title";s:7:"dadosv4";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:27:"opt_imagemQualidadeEsquerda";a:9:{s:3:"url";s:85:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/100.jpg";s:2:"id";s:2:"97";s:6:"height";s:3:"462";s:5:"width";s:3:"211";s:9:"thumbnail";s:93:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/100-150x150.jpg";s:5:"title";s:3:"100";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:25:"opt_textoQualidadeDireita";s:51:"Linda, Simpática, Confiante, versátil, flexível ";s:26:"opt_textoQualidadeEsquerda";s:30:"Pontual, Animada, Cuidadosa...";s:26:"opt_imagemQualidadeDireita";a:9:{s:3:"url";s:85:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/100.jpg";s:2:"id";s:2:"97";s:6:"height";s:3:"462";s:5:"width";s:3:"211";s:9:"thumbnail";s:93:"http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/100-150x150.jpg";s:5:"title";s:3:"100";s:7:"caption";s:0:"";s:3:"alt";s:0:"";s:11:"description";s:0:"";}s:19:"opt-customizer-only";s:1:"2";}', 'yes'),
(562, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:3:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:65:"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.7.zip";s:6:"locale";s:5:"pt_BR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.7.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.7";s:7:"version";s:5:"4.9.7";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.7.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.7.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.7-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.7-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.9.7-partial-6.zip";s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.7";s:7:"version";s:5:"4.9.7";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.9.6";}i:2;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.7.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.7.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.7-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.7-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.9.7-partial-6.zip";s:8:"rollback";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.7-rollback-6.zip";}s:7:"current";s:5:"4.9.7";s:7:"version";s:5:"4.9.7";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.9.6";s:9:"new_files";s:0:"";}}s:12:"last_checked";i:1531507520;s:15:"version_checked";s:5:"4.9.6";s:12:"translations";a:0:{}}', 'no'),
(563, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1531507521;s:7:"checked";a:1:{s:14:"amandakaroline";s:3:"1.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(564, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1531507522;s:7:"checked";a:9:{s:51:"all-in-one-wp-security-and-firewall/wp-security.php";s:5:"4.3.5";s:43:"base-amandakaroline/base-amandakaroline.php";s:3:"0.1";s:36:"contact-form-7/wp-contact-form-7.php";s:5:"5.0.2";s:32:"disqus-comment-system/disqus.php";s:6:"3.0.16";s:21:"meta-box/meta-box.php";s:7:"4.14.11";s:37:"post-types-order/post-types-order.php";s:7:"1.9.3.6";s:35:"redux-framework/redux-framework.php";s:5:"3.6.9";s:29:"wp-mail-smtp/wp_mail_smtp.php";s:5:"1.3.3";s:24:"wordpress-seo/wp-seo.php";s:5:"7.7.3";}s:8:"response";a:4:{s:51:"all-in-one-wp-security-and-firewall/wp-security.php";O:8:"stdClass":12:{s:2:"id";s:49:"w.org/plugins/all-in-one-wp-security-and-firewall";s:4:"slug";s:35:"all-in-one-wp-security-and-firewall";s:6:"plugin";s:51:"all-in-one-wp-security-and-firewall/wp-security.php";s:11:"new_version";s:5:"4.3.6";s:3:"url";s:66:"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/";s:7:"package";s:78:"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip";s:5:"icons";a:1:{s:2:"1x";s:88:"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826";}s:7:"banners";a:1:{s:2:"1x";s:90:"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1232826";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.7";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":12:{s:2:"id";s:28:"w.org/plugins/contact-form-7";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:5:"5.0.3";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/contact-form-7.5.0.3.zip";s:5:"icons";a:2:{s:2:"2x";s:66:"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007";s:2:"1x";s:66:"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007";}s:7:"banners";a:2:{s:2:"2x";s:69:"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901";s:2:"1x";s:68:"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.7";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:37:"post-types-order/post-types-order.php";O:8:"stdClass":12:{s:2:"id";s:30:"w.org/plugins/post-types-order";s:4:"slug";s:16:"post-types-order";s:6:"plugin";s:37:"post-types-order/post-types-order.php";s:11:"new_version";s:7:"1.9.3.9";s:3:"url";s:47:"https://wordpress.org/plugins/post-types-order/";s:7:"package";s:67:"https://downloads.wordpress.org/plugin/post-types-order.1.9.3.9.zip";s:5:"icons";a:1:{s:2:"1x";s:69:"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428";}s:7:"banners";a:2:{s:2:"2x";s:72:"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574";s:2:"1x";s:71:"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.7";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:24:"wordpress-seo/wp-seo.php";O:8:"stdClass":12:{s:2:"id";s:27:"w.org/plugins/wordpress-seo";s:4:"slug";s:13:"wordpress-seo";s:6:"plugin";s:24:"wordpress-seo/wp-seo.php";s:11:"new_version";s:3:"7.8";s:3:"url";s:44:"https://wordpress.org/plugins/wordpress-seo/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/wordpress-seo.7.8.zip";s:5:"icons";a:3:{s:2:"2x";s:66:"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347";s:2:"1x";s:58:"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1859687";s:3:"svg";s:58:"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1859687";}s:7:"banners";a:2:{s:2:"2x";s:69:"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435";s:2:"1x";s:68:"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435";}s:11:"banners_rtl";a:2:{s:2:"2x";s:73:"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435";s:2:"1x";s:72:"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435";}s:6:"tested";s:5:"4.9.7";s:12:"requires_php";s:5:"5.2.4";s:13:"compatibility";O:8:"stdClass":0:{}}}s:12:"translations";a:3:{i:0;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"contact-form-7";s:8:"language";s:5:"pt_BR";s:7:"version";s:3:"5.0";s:7:"updated";s:19:"2018-02-03 16:12:23";s:7:"package";s:79:"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.0/pt_BR.zip";s:10:"autoupdate";b:1;}i:1;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:12:"wp-mail-smtp";s:8:"language";s:5:"pt_BR";s:7:"version";s:6:"0.10.1";s:7:"updated";s:19:"2017-04-14 19:57:38";s:7:"package";s:80:"https://downloads.wordpress.org/translation/plugin/wp-mail-smtp/0.10.1/pt_BR.zip";s:10:"autoupdate";b:1;}i:2;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:13:"wordpress-seo";s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"6.3.1";s:7:"updated";s:19:"2018-02-03 16:01:18";s:7:"package";s:80:"https://downloads.wordpress.org/translation/plugin/wordpress-seo/6.3.1/pt_BR.zip";s:10:"autoupdate";b:1;}}s:9:"no_update";a:4:{s:32:"disqus-comment-system/disqus.php";O:8:"stdClass":9:{s:2:"id";s:35:"w.org/plugins/disqus-comment-system";s:4:"slug";s:21:"disqus-comment-system";s:6:"plugin";s:32:"disqus-comment-system/disqus.php";s:11:"new_version";s:6:"3.0.16";s:3:"url";s:52:"https://wordpress.org/plugins/disqus-comment-system/";s:7:"package";s:71:"https://downloads.wordpress.org/plugin/disqus-comment-system.3.0.16.zip";s:5:"icons";a:3:{s:2:"2x";s:74:"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448";s:2:"1x";s:66:"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350";s:3:"svg";s:66:"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350";}s:7:"banners";a:1:{s:2:"1x";s:76:"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350";}s:11:"banners_rtl";a:0:{}}s:21:"meta-box/meta-box.php";O:8:"stdClass":9:{s:2:"id";s:22:"w.org/plugins/meta-box";s:4:"slug";s:8:"meta-box";s:6:"plugin";s:21:"meta-box/meta-box.php";s:11:"new_version";s:7:"4.14.11";s:3:"url";s:39:"https://wordpress.org/plugins/meta-box/";s:7:"package";s:59:"https://downloads.wordpress.org/plugin/meta-box.4.14.11.zip";s:5:"icons";a:1:{s:2:"1x";s:61:"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915";}s:7:"banners";a:1:{s:2:"1x";s:63:"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1626382";}s:11:"banners_rtl";a:0:{}}s:35:"redux-framework/redux-framework.php";O:8:"stdClass":9:{s:2:"id";s:29:"w.org/plugins/redux-framework";s:4:"slug";s:15:"redux-framework";s:6:"plugin";s:35:"redux-framework/redux-framework.php";s:11:"new_version";s:5:"3.6.9";s:3:"url";s:46:"https://wordpress.org/plugins/redux-framework/";s:7:"package";s:64:"https://downloads.wordpress.org/plugin/redux-framework.3.6.9.zip";s:5:"icons";a:3:{s:2:"2x";s:67:"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554";s:2:"1x";s:59:"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554";s:3:"svg";s:59:"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554";}s:7:"banners";a:1:{s:2:"1x";s:69:"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165";}s:11:"banners_rtl";a:0:{}}s:29:"wp-mail-smtp/wp_mail_smtp.php";O:8:"stdClass":9:{s:2:"id";s:26:"w.org/plugins/wp-mail-smtp";s:4:"slug";s:12:"wp-mail-smtp";s:6:"plugin";s:29:"wp-mail-smtp/wp_mail_smtp.php";s:11:"new_version";s:5:"1.3.3";s:3:"url";s:43:"https://wordpress.org/plugins/wp-mail-smtp/";s:7:"package";s:55:"https://downloads.wordpress.org/plugin/wp-mail-smtp.zip";s:5:"icons";a:2:{s:2:"2x";s:65:"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440";s:2:"1x";s:65:"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440";}s:7:"banners";a:2:{s:2:"2x";s:68:"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=1900656";s:2:"1x";s:67:"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=1900656";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(530, '_site_transient_timeout_browser_a54eb83090ed984332f4eca22d3ec5e4', '1532035769', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_postmeta`
--

CREATE TABLE `ak_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_postmeta`
--

INSERT INTO `ak_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]'),
(4, 5, '_mail', 'a:8:{s:7:"subject";s:32:"Amanda Karoline "[your-subject]"";s:6:"sender";s:40:"[your-name] <carolinoamandacs@gmail.com>";s:4:"body";s:204:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Amanda Karoline (http://localhost/projetos/amandakaroline_portfolio)";s:9:"recipient";s:26:"carolinoamandacs@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(5, 5, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:32:"Amanda Karoline "[your-subject]"";s:6:"sender";s:44:"Amanda Karoline <carolinoamandacs@gmail.com>";s:4:"body";s:146:"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Amanda Karoline (http://localhost/projetos/amandakaroline_portfolio)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:36:"Reply-To: carolinoamandacs@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(6, 5, '_messages', 'a:8:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";}'),
(7, 5, '_additional_settings', NULL),
(8, 5, '_locale', 'pt_BR'),
(9, 1, '_edit_lock', '1530816849:1'),
(10, 18, '_edit_last', '1'),
(11, 18, '_edit_lock', '1530823949:1'),
(12, 19, '_wp_attached_file', '2018/07/hay.jpg'),
(13, 19, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:960;s:4:"file";s:15:"2018/07/hay.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"hay-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"hay-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:15:"hay-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(14, 18, '_yoast_wpseo_content_score', '60'),
(15, 18, '_wp_trash_meta_status', 'publish'),
(16, 18, '_wp_trash_meta_time', '1530824546'),
(17, 18, '_wp_desired_post_slug', 'hayana-sanquetta'),
(18, 25, '_edit_last', '1'),
(19, 25, 'Amandakaroline_nome_autorDepoimento', 'Hayana Sanquetta'),
(20, 25, 'Amandakaroline_text_depoimento', '"Mulher incrível, diversos projetos desenvolvidos em parceria. Sempre disposta a participar e sempre conquistando os objetivos"'),
(21, 25, 'Amandakaroline_foto_autorDepoimento', '19'),
(22, 25, '_yoast_wpseo_content_score', '30'),
(23, 25, '_edit_lock', '1531315579:1'),
(24, 25, '_thumbnail_id', '19'),
(25, 25, '_wp_old_slug', 'rascunho-automatico'),
(26, 29, '_edit_last', '1'),
(27, 29, '_edit_lock', '1531315730:1'),
(28, 30, '_wp_attached_file', '2018/07/logoHC.png'),
(29, 30, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:242;s:6:"height";i:155;s:4:"file";s:18:"2018/07/logoHC.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"logoHC-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(30, 29, '_thumbnail_id', '30'),
(31, 29, 'Amandakaroline_link_do_parceiro', 'http://hcdesenvolvimentos.com.br'),
(32, 29, '_yoast_wpseo_content_score', '30'),
(33, 60, '_wp_attached_file', '2018/07/logo-fixa.png'),
(34, 60, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:629;s:4:"file";s:21:"2018/07/logo-fixa.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"logo-fixa-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:21:"logo-fixa-300x157.png";s:5:"width";i:300;s:6:"height";i:157;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:21:"logo-fixa-768x403.png";s:5:"width";i:768;s:6:"height";i:403;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:22:"logo-fixa-1024x537.png";s:5:"width";i:1024;s:6:"height";i:537;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(35, 61, '_wp_attached_file', '2018/07/opacidade-4.png'),
(36, 61, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:1080;s:4:"file";s:23:"2018/07/opacidade-4.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"opacidade-4-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"opacidade-4-300x169.png";s:5:"width";i:300;s:6:"height";i:169;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:23:"opacidade-4-768x432.png";s:5:"width";i:768;s:6:"height";i:432;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"opacidade-4-1024x576.png";s:5:"width";i:1024;s:6:"height";i:576;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(37, 62, '_wp_attached_file', '2018/07/rosa-na-boca.png'),
(38, 62, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:879;s:6:"height";i:787;s:4:"file";s:24:"2018/07/rosa-na-boca.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"rosa-na-boca-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:24:"rosa-na-boca-300x269.png";s:5:"width";i:300;s:6:"height";i:269;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:24:"rosa-na-boca-768x688.png";s:5:"width";i:768;s:6:"height";i:688;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(41, 64, '_wp_attached_file', '2018/07/DSC0142.jpg'),
(42, 64, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC0142.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC0142-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC0142-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC0142-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC0142-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525529537";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:9:"0.0015625";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(43, 65, '_wp_attached_file', '2018/07/imagem-sobremimCentro.jpg'),
(44, 65, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:33:"2018/07/imagem-sobremimCentro.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:33:"imagem-sobremimCentro-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:33:"imagem-sobremimCentro-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"imagem-sobremimCentro-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:34:"imagem-sobremimCentro-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.5";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525525116";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:5:"0.005";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(45, 66, '_wp_attached_file', '2018/07/DSC01016.jpg'),
(46, 66, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5456;s:6:"height";i:3632;s:4:"file";s:20:"2018/07/DSC01016.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC01016-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"DSC01016-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC01016-768x511.jpg";s:5:"width";i:768;s:6:"height";i:511;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC01016-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(556, 164, '_wp_attached_file', '2018/07/DSC1235.jpg'),
(557, 164, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC1235.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1235-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1235-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC1235-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1235-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522590";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:6:"0.0125";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(550, 161, '_wp_attached_file', '2018/07/DSC1227-1.jpg'),
(551, 161, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:21:"2018/07/DSC1227-1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"DSC1227-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"DSC1227-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:21:"DSC1227-1-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"DSC1227-1-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522502";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:17:"0.016666666666667";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(341, 141, '_wp_attached_file', '2018/07/dadosv4.png'),
(342, 141, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:1582;s:4:"file";s:19:"2018/07/dadosv4.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"dadosv4-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"dadosv4-300x247.png";s:5:"width";i:300;s:6:"height";i:247;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:19:"dadosv4-768x633.png";s:5:"width";i:768;s:6:"height";i:633;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:20:"dadosv4-1024x844.png";s:5:"width";i:1024;s:6:"height";i:844;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(53, 3, '_wp_trash_meta_status', 'draft'),
(54, 3, '_wp_trash_meta_time', '1531249053'),
(55, 3, '_wp_desired_post_slug', 'politica-de-privacidade'),
(56, 2, '_wp_trash_meta_status', 'publish'),
(57, 2, '_wp_trash_meta_time', '1531249053'),
(58, 2, '_wp_desired_post_slug', 'pagina-exemplo'),
(59, 72, '_edit_last', '1'),
(63, 72, '_wp_page_template', 'paginas/inicial.php'),
(61, 72, '_yoast_wpseo_content_score', '30'),
(62, 72, '_edit_lock', '1531249455:1'),
(64, 74, '_edit_last', '1'),
(65, 74, '_edit_lock', '1531253334:1'),
(66, 75, '_wp_attached_file', '2018/07/videoteste.mp4'),
(67, 75, '_wp_attachment_metadata', 'a:10:{s:8:"filesize";i:1726925;s:9:"mime_type";s:9:"video/mp4";s:6:"length";i:12;s:16:"length_formatted";s:4:"0:12";s:5:"width";i:480;s:6:"height";i:848;s:10:"fileformat";s:3:"mp4";s:10:"dataformat";s:9:"quicktime";s:5:"audio";a:7:{s:10:"dataformat";s:3:"mp4";s:5:"codec";s:19:"ISO/IEC 14496-3 AAC";s:11:"sample_rate";d:44100;s:8:"channels";i:2;s:15:"bits_per_sample";i:16;s:8:"lossless";b:0;s:11:"channelmode";s:6:"stereo";}s:17:"created_timestamp";i:1529690714;}'),
(548, 160, '_wp_attached_file', '2018/07/DSC1227.jpg'),
(549, 160, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC1227.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1227-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1227-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC1227-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1227-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522502";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:17:"0.016666666666667";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(70, 77, '_wp_attached_file', '2018/07/backgroundGaleria.jpg'),
(71, 77, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2560;s:6:"height";i:1707;s:4:"file";s:29:"2018/07/backgroundGaleria.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"backgroundGaleria-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"backgroundGaleria-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:29:"backgroundGaleria-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"backgroundGaleria-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(72, 74, '_thumbnail_id', '77'),
(73, 74, 'Amandakaroline_url_video_banner-destaque', 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/videoteste.mp4'),
(74, 74, 'Amandakaroline_link_redirecionamento_banner-destaque', '#'),
(75, 74, 'Amandakaroline_checkbox_banner-destaque', '1'),
(76, 74, '_yoast_wpseo_content_score', '30'),
(77, 79, '_edit_last', '1'),
(78, 79, '_edit_lock', '1531249710:1'),
(79, 79, '_thumbnail_id', '77'),
(80, 79, 'Amandakaroline_checkbox_banner-destaque', '0'),
(81, 79, '_yoast_wpseo_content_score', '30'),
(82, 80, '_edit_last', '1'),
(83, 80, '_edit_lock', '1531341096:1'),
(555, 163, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC1232.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1232-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1232-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC1232-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1232-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522571";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:6:"0.0125";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(554, 163, '_wp_attached_file', '2018/07/DSC1232.jpg'),
(552, 162, '_wp_attached_file', '2018/07/DSC1228.jpg'),
(553, 162, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC1228.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1228-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1228-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC1228-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1228-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"3.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522546";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:17:"0.016666666666667";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(583, 172, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn082.jpg'),
(558, 165, '_wp_attached_file', '2018/07/DSC1237.jpg'),
(559, 165, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC1237.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1237-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1237-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC1237-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1237-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522618";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:6:"0.0125";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(560, 166, '_wp_attached_file', '2018/07/DSC1239.jpg'),
(561, 166, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC1239.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1239-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1239-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC1239-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1239-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522636";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:6:"0.0125";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(562, 167, '_wp_attached_file', '2018/07/DSC1241.jpg'),
(563, 167, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC1241.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1241-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1241-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC1241-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1241-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522651";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:6:"0.0125";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(840, 80, 'Amandakaroline_imagem_fundo_galeria', '160'),
(850, 80, 'Amandakaroline_galeria_imagens_servicos', '170'),
(849, 80, 'Amandakaroline_galeria_imagens_servicos', '169'),
(848, 80, 'Amandakaroline_galeria_imagens_servicos', '168'),
(847, 80, 'Amandakaroline_galeria_imagens_servicos', '167'),
(846, 80, 'Amandakaroline_galeria_imagens_servicos', '166'),
(105, 80, '_yoast_wpseo_content_score', '30'),
(564, 168, '_wp_attached_file', '2018/07/DSC1245.jpg'),
(565, 168, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC1245.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1245-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1245-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC1245-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1245-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522681";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:6:"0.0125";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(108, 80, '_thumbnail_id', '159'),
(116, 89, '_edit_last', '1'),
(117, 89, '_edit_lock', '1531250178:1'),
(118, 89, '_thumbnail_id', '77'),
(119, 89, 'Amandakaroline_checkbox_banner-destaque', '0'),
(120, 89, '_yoast_wpseo_content_score', '30'),
(121, 90, '_edit_last', '1'),
(122, 90, '_thumbnail_id', '77'),
(123, 90, 'Amandakaroline_checkbox_destaque', '0'),
(124, 90, '_yoast_wpseo_primary_categoriaDestaque', ''),
(125, 90, '_yoast_wpseo_content_score', '30'),
(126, 90, '_edit_lock', '1531251229:1'),
(127, 91, '_edit_last', '1'),
(129, 91, 'Amandakaroline_checkbox_destaque', '0'),
(130, 91, '_yoast_wpseo_primary_categoriaDestaque', ''),
(131, 91, '_yoast_wpseo_content_score', '30'),
(132, 91, '_edit_lock', '1531252972:1'),
(133, 92, '_edit_last', '1'),
(134, 92, '_edit_lock', '1531272036:1'),
(135, 92, 'Amandakaroline_url_video_destaque', 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/videoteste.mp4'),
(136, 92, 'Amandakaroline_checkbox_destaque', '1'),
(137, 92, '_yoast_wpseo_primary_categoriaDestaque', ''),
(138, 92, '_yoast_wpseo_content_score', '30'),
(139, 92, 'Amandakaroline_link_redirecionamento_destaque', '#'),
(582, 171, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-2.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-2-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-2-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-2-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(141, 93, '_wp_attached_file', '2018/07/logoSobreMim.png'),
(142, 93, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:150;s:6:"height";i:65;s:4:"file";s:24:"2018/07/logoSobreMim.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"logoSobreMim-150x65.png";s:5:"width";i:150;s:6:"height";i:65;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(845, 80, 'Amandakaroline_galeria_imagens_servicos', '165'),
(150, 94, '_edit_last', '1'),
(151, 94, '_edit_lock', '1531341101:1'),
(566, 169, '_wp_attached_file', '2018/07/DSC1250.jpg'),
(567, 169, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC1250.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1250-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1250-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC1250-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1250-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522727";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:6:"0.0125";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(862, 94, 'Amandakaroline_imagem_fundo_galeria', '172'),
(872, 94, 'Amandakaroline_galeria_imagens_servicos', '182'),
(871, 94, 'Amandakaroline_galeria_imagens_servicos', '181'),
(870, 94, 'Amandakaroline_galeria_imagens_servicos', '180'),
(159, 94, '_yoast_wpseo_content_score', '30'),
(166, 97, '_wp_attached_file', '2018/07/100.jpg'),
(167, 97, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:211;s:6:"height";i:462;s:4:"file";s:15:"2018/07/100.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"100-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"100-137x300.jpg";s:5:"width";i:137;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(168, 99, '_edit_last', '1'),
(169, 99, '_edit_lock', '1531315726:1'),
(170, 100, '_wp_attached_file', '2018/07/logoedu.png'),
(171, 100, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:242;s:6:"height";i:155;s:4:"file";s:19:"2018/07/logoedu.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"logoedu-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(172, 99, '_thumbnail_id', '100'),
(173, 99, 'Amandakaroline_link_do_parceiro', 'http://eduardoem.com.br/'),
(174, 99, '_yoast_wpseo_content_score', '30'),
(175, 101, '_edit_last', '1'),
(176, 101, '_edit_lock', '1531319509:1'),
(202, 109, '_wp_attached_file', '2018/07/logodarlan.png'),
(203, 109, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:242;s:6:"height";i:155;s:4:"file";s:22:"2018/07/logodarlan.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"logodarlan-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(180, 101, 'Amandakaroline_link_do_parceiro', 'https://www.facebook.com/dldesignerc/?ref=br_rs'),
(181, 101, '_yoast_wpseo_content_score', '30'),
(182, 103, '_edit_last', '1'),
(183, 103, '_edit_lock', '1531318305:1'),
(192, 106, '_wp_attached_file', '2018/07/logoHayana.png'),
(193, 106, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:242;s:6:"height";i:155;s:4:"file";s:22:"2018/07/logoHayana.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"logoHayana-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(187, 103, 'Amandakaroline_link_do_parceiro', 'http://hayanasanquetta.com/'),
(188, 103, '_yoast_wpseo_content_score', '30'),
(194, 103, '_thumbnail_id', '106'),
(195, 107, '_edit_last', '1'),
(196, 107, '_edit_lock', '1531327849:1'),
(338, 140, '_wp_attached_file', '2018/07/dutrancas.png'),
(339, 140, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:242;s:6:"height";i:155;s:4:"file";s:21:"2018/07/dutrancas.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"dutrancas-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(200, 107, 'Amandakaroline_link_do_parceiro', 'https://www.instagram.com/dutrancas/'),
(201, 107, '_yoast_wpseo_content_score', '30'),
(204, 101, '_thumbnail_id', '109'),
(205, 110, '_menu_item_type', 'post_type'),
(206, 110, '_menu_item_menu_item_parent', '0'),
(207, 110, '_menu_item_object_id', '72'),
(208, 110, '_menu_item_object', 'page'),
(209, 110, '_menu_item_target', ''),
(210, 110, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(211, 110, '_menu_item_xfn', ''),
(212, 110, '_menu_item_url', ''),
(213, 110, '_menu_item_orphaned', '1531326588'),
(440, 150, '_edit_lock', '1531331612:1'),
(223, 112, '_edit_last', '1'),
(226, 114, '_edit_last', '1'),
(225, 112, '_edit_lock', '1531326499:1'),
(230, 116, '_edit_last', '1'),
(228, 114, '_yoast_wpseo_content_score', '30'),
(229, 114, '_edit_lock', '1531326540:1'),
(233, 118, '_edit_last', '1'),
(232, 116, '_edit_lock', '1531326558:1'),
(237, 120, '_edit_last', '1'),
(235, 118, '_yoast_wpseo_content_score', '30'),
(236, 118, '_edit_lock', '1531326577:1'),
(241, 122, '_edit_last', '1'),
(239, 120, '_yoast_wpseo_content_score', '30'),
(240, 120, '_edit_lock', '1531326591:1'),
(245, 124, '_edit_last', '1'),
(243, 122, '_yoast_wpseo_content_score', '30'),
(244, 122, '_edit_lock', '1531326602:1'),
(246, 124, '_wp_page_template', 'default'),
(247, 124, '_yoast_wpseo_content_score', '30'),
(248, 124, '_edit_lock', '1531326752:1'),
(249, 127, '_edit_last', '1'),
(251, 127, '_yoast_wpseo_content_score', '30'),
(252, 127, '_edit_lock', '1531326629:1'),
(340, 107, '_thumbnail_id', '140'),
(585, 173, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn009.jpg'),
(343, 127, '_wp_trash_meta_status', 'publish'),
(844, 80, 'Amandakaroline_galeria_imagens_servicos', '164'),
(843, 80, 'Amandakaroline_galeria_imagens_servicos', '163'),
(842, 80, 'Amandakaroline_galeria_imagens_servicos', '162'),
(841, 80, 'Amandakaroline_galeria_imagens_servicos', '161'),
(581, 171, '_wp_attached_file', '2018/07/carrossel-2.png'),
(344, 127, '_wp_trash_meta_time', '1531330765'),
(568, 170, '_wp_attached_file', '2018/07/DSC1252.jpg'),
(569, 170, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC1252.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC1252-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC1252-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC1252-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC1252-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:11:"NIKON D3100";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1525522748";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:6:"0.0125";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(345, 127, '_wp_desired_post_slug', 'loja'),
(346, 124, '_wp_trash_meta_status', 'publish'),
(347, 124, '_wp_trash_meta_time', '1531330765'),
(316, 136, '_menu_item_type', 'post_type'),
(317, 136, '_menu_item_menu_item_parent', '0'),
(318, 136, '_menu_item_object_id', '72'),
(319, 136, '_menu_item_object', 'page'),
(320, 136, '_menu_item_target', ''),
(321, 136, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(322, 136, '_menu_item_xfn', ''),
(323, 136, '_menu_item_url', ''),
(324, 136, '_menu_item_orphaned', '1531326793'),
(439, 150, '_edit_last', '1'),
(348, 124, '_wp_desired_post_slug', 'blog'),
(349, 122, '_wp_trash_meta_status', 'publish'),
(350, 122, '_wp_trash_meta_time', '1531330765'),
(351, 122, '_wp_desired_post_slug', 'contato'),
(352, 120, '_wp_trash_meta_status', 'publish'),
(353, 120, '_wp_trash_meta_time', '1531330765'),
(354, 120, '_wp_desired_post_slug', 'qualidades'),
(355, 118, '_wp_trash_meta_status', 'publish'),
(356, 118, '_wp_trash_meta_time', '1531330765'),
(357, 118, '_wp_desired_post_slug', 'estatistica'),
(358, 116, '_wp_trash_meta_status', 'publish'),
(359, 116, '_wp_trash_meta_time', '1531330765'),
(360, 116, '_wp_desired_post_slug', 'sobre-mim'),
(361, 114, '_wp_trash_meta_status', 'publish'),
(362, 114, '_wp_trash_meta_time', '1531330765'),
(363, 114, '_wp_desired_post_slug', 'portfolio'),
(364, 112, '_wp_trash_meta_status', 'publish'),
(365, 112, '_wp_trash_meta_time', '1531330765'),
(366, 112, '_wp_desired_post_slug', 'inicio'),
(367, 142, '_menu_item_type', 'custom'),
(368, 142, '_menu_item_menu_item_parent', '0'),
(369, 142, '_menu_item_object_id', '142'),
(370, 142, '_menu_item_object', 'custom'),
(371, 142, '_menu_item_target', ''),
(372, 142, '_menu_item_classes', 'a:1:{i:0;s:9:"scrollTop";}'),
(373, 142, '_menu_item_xfn', ''),
(374, 142, '_menu_item_url', '#carrosselDestaqueTopo'),
(376, 143, '_menu_item_type', 'custom'),
(377, 143, '_menu_item_menu_item_parent', '0'),
(378, 143, '_menu_item_object_id', '143'),
(379, 143, '_menu_item_object', 'custom'),
(380, 143, '_menu_item_target', ''),
(381, 143, '_menu_item_classes', 'a:1:{i:0;s:9:"scrollTop";}'),
(382, 143, '_menu_item_xfn', ''),
(383, 143, '_menu_item_url', '#portfolio'),
(385, 144, '_menu_item_type', 'custom'),
(386, 144, '_menu_item_menu_item_parent', '0'),
(387, 144, '_menu_item_object_id', '144'),
(388, 144, '_menu_item_object', 'custom'),
(389, 144, '_menu_item_target', ''),
(390, 144, '_menu_item_classes', 'a:1:{i:0;s:9:"scrollTop";}'),
(391, 144, '_menu_item_xfn', ''),
(392, 144, '_menu_item_url', '#sobre-mim-centro'),
(394, 145, '_menu_item_type', 'custom'),
(395, 145, '_menu_item_menu_item_parent', '0'),
(396, 145, '_menu_item_object_id', '145'),
(397, 145, '_menu_item_object', 'custom'),
(398, 145, '_menu_item_target', ''),
(399, 145, '_menu_item_classes', 'a:1:{i:0;s:9:"scrollTop";}'),
(400, 145, '_menu_item_xfn', ''),
(401, 145, '_menu_item_url', '#estatistica'),
(403, 146, '_menu_item_type', 'custom'),
(404, 146, '_menu_item_menu_item_parent', '0'),
(405, 146, '_menu_item_object_id', '146'),
(406, 146, '_menu_item_object', 'custom'),
(407, 146, '_menu_item_target', ''),
(408, 146, '_menu_item_classes', 'a:1:{i:0;s:9:"scrollTop";}'),
(409, 146, '_menu_item_xfn', ''),
(410, 146, '_menu_item_url', '#qualidades'),
(412, 147, '_menu_item_type', 'custom'),
(413, 147, '_menu_item_menu_item_parent', '0'),
(414, 147, '_menu_item_object_id', '147'),
(415, 147, '_menu_item_object', 'custom'),
(416, 147, '_menu_item_target', ''),
(417, 147, '_menu_item_classes', 'a:1:{i:0;s:9:"scrollTop";}'),
(418, 147, '_menu_item_xfn', ''),
(419, 147, '_menu_item_url', '#'),
(421, 148, '_menu_item_type', 'custom'),
(422, 148, '_menu_item_menu_item_parent', '0'),
(423, 148, '_menu_item_object_id', '148'),
(424, 148, '_menu_item_object', 'custom'),
(425, 148, '_menu_item_target', ''),
(426, 148, '_menu_item_classes', 'a:1:{i:0;s:9:"scrollTop";}'),
(427, 148, '_menu_item_xfn', ''),
(428, 148, '_menu_item_url', 'http://amandakaroline.hcdesenvolvimentos.com.br'),
(430, 149, '_menu_item_type', 'custom'),
(431, 149, '_menu_item_menu_item_parent', '0'),
(432, 149, '_menu_item_object_id', '149'),
(433, 149, '_menu_item_object', 'custom'),
(434, 149, '_menu_item_target', ''),
(435, 149, '_menu_item_classes', 'a:1:{i:0;s:9:"scrollTop";}'),
(436, 149, '_menu_item_xfn', ''),
(437, 149, '_menu_item_url', '#'),
(586, 173, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3656;s:6:"height";i:5484;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn009.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn009-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn009-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn009-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn009-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381051";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:7:"0.00125";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(448, 150, 'Amandakaroline_imagem_fundo_galeria', '152'),
(449, 150, 'Amandakaroline_galeria_imagens_servicos', '86'),
(450, 150, 'Amandakaroline_galeria_imagens_servicos', '87'),
(451, 150, 'Amandakaroline_galeria_imagens_servicos', '88'),
(452, 150, 'Amandakaroline_galeria_imagens_servicos', '81'),
(453, 150, 'Amandakaroline_galeria_imagens_servicos', '77'),
(454, 150, 'Amandakaroline_galeria_imagens_servicos', '68'),
(455, 150, 'Amandakaroline_galeria_imagens_servicos', '82'),
(456, 150, 'Amandakaroline_galeria_imagens_servicos', '83'),
(457, 150, 'Amandakaroline_galeria_imagens_servicos', '95'),
(458, 150, 'Amandakaroline_galeria_imagens_servicos', '84'),
(459, 150, '_yoast_wpseo_content_score', '30'),
(460, 154, '_edit_last', '1'),
(478, 154, 'Amandakaroline_imagem_fundo_galeria', '64'),
(491, 154, 'Amandakaroline_galeria_imagens_servicos', '82'),
(490, 154, 'Amandakaroline_galeria_imagens_servicos', '83'),
(489, 154, 'Amandakaroline_galeria_imagens_servicos', '84'),
(488, 154, 'Amandakaroline_galeria_imagens_servicos', '100'),
(487, 154, 'Amandakaroline_galeria_imagens_servicos', '97'),
(486, 154, 'Amandakaroline_galeria_imagens_servicos', '95'),
(485, 154, 'Amandakaroline_galeria_imagens_servicos', '93'),
(484, 154, 'Amandakaroline_galeria_imagens_servicos', '88'),
(483, 154, 'Amandakaroline_galeria_imagens_servicos', '87'),
(482, 154, 'Amandakaroline_galeria_imagens_servicos', '86'),
(481, 154, 'Amandakaroline_galeria_imagens_servicos', '106'),
(480, 154, 'Amandakaroline_galeria_imagens_servicos', '109'),
(479, 154, 'Amandakaroline_galeria_imagens_servicos', '140'),
(476, 154, '_yoast_wpseo_content_score', '30'),
(477, 154, '_edit_lock', '1531333396:1'),
(492, 155, '_edit_last', '1'),
(493, 155, '_edit_lock', '1531331780:1'),
(495, 155, 'Amandakaroline_imagem_fundo_galeria', '81'),
(496, 155, 'Amandakaroline_galeria_imagens_servicos', '19'),
(497, 155, 'Amandakaroline_galeria_imagens_servicos', '66'),
(498, 155, 'Amandakaroline_galeria_imagens_servicos', '67'),
(499, 155, 'Amandakaroline_galeria_imagens_servicos', '65'),
(500, 155, 'Amandakaroline_galeria_imagens_servicos', '64'),
(501, 155, 'Amandakaroline_galeria_imagens_servicos', '62'),
(502, 155, 'Amandakaroline_galeria_imagens_servicos', '77'),
(503, 155, 'Amandakaroline_galeria_imagens_servicos', '68'),
(504, 155, 'Amandakaroline_galeria_imagens_servicos', '81'),
(505, 155, 'Amandakaroline_galeria_imagens_servicos', '82'),
(506, 155, 'Amandakaroline_galeria_imagens_servicos', '83'),
(507, 155, 'Amandakaroline_galeria_imagens_servicos', '84'),
(508, 155, '_yoast_wpseo_content_score', '30'),
(509, 156, '_edit_last', '1'),
(510, 156, '_edit_lock', '1531341128:1'),
(511, 157, '_wp_attached_file', '2018/07/imagemTopoa.jpg'),
(512, 157, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:852;s:6:"height";i:1280;s:4:"file";s:23:"2018/07/imagemTopoa.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"imagemTopoa-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"imagemTopoa-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:24:"imagemTopoa-768x1154.jpg";s:5:"width";i:768;s:6:"height";i:1154;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"imagemTopoa-682x1024.jpg";s:5:"width";i:682;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(513, 156, '_thumbnail_id', '211'),
(750, 156, 'Amandakaroline_imagem_fundo_galeria', '215'),
(737, 212, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3632;s:6:"height";i:5456;s:4:"file";s:20:"2018/07/DSC00996.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC00996-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"DSC00996-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:21:"DSC00996-768x1154.jpg";s:5:"width";i:768;s:6:"height";i:1154;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC00996-682x1024.jpg";s:5:"width";i:682;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(736, 212, '_wp_attached_file', '2018/07/DSC00996.jpg'),
(751, 156, 'Amandakaroline_galeria_imagens_servicos', '216'),
(523, 156, '_yoast_wpseo_content_score', '60'),
(524, 158, '_edit_last', '1'),
(525, 158, '_edit_lock', '1531341103:1'),
(584, 172, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5391;s:6:"height";i:3594;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn082.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn082-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn082-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn082-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn082-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381821";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:7:"0.00125";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(851, 158, 'Amandakaroline_imagem_fundo_galeria', '196'),
(861, 158, 'Amandakaroline_galeria_imagens_servicos', '174'),
(860, 158, 'Amandakaroline_galeria_imagens_servicos', '175'),
(859, 158, 'Amandakaroline_galeria_imagens_servicos', '176'),
(858, 158, 'Amandakaroline_galeria_imagens_servicos', '178'),
(857, 158, 'Amandakaroline_galeria_imagens_servicos', '177'),
(856, 158, 'Amandakaroline_galeria_imagens_servicos', '179'),
(855, 158, 'Amandakaroline_galeria_imagens_servicos', '180'),
(854, 158, 'Amandakaroline_galeria_imagens_servicos', '173'),
(538, 158, '_yoast_wpseo_content_score', '60'),
(539, 159, '_wp_attached_file', '2018/07/carrossel-1.png'),
(540, 159, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-1.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-1-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-1-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(587, 174, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn022.jpg'),
(588, 174, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3744;s:6:"height";i:5616;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn022.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn022-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn022-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn022-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn022-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381265";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:9:"0.0015625";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(589, 175, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn032.jpg');
INSERT INTO `ak_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(590, 175, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3744;s:6:"height";i:5616;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn032.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn032-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn032-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn032-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn032-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381294";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:9:"0.0015625";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(591, 176, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn038.jpg'),
(592, 176, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3603;s:6:"height";i:5404;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn038.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn038-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn038-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn038-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn038-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381332";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:5:"0.001";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(593, 177, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn052.jpg'),
(594, 177, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3679;s:6:"height";i:5518;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn052.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn052-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn052-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn052-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn052-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381358";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:5:"0.001";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(595, 178, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn078.jpg'),
(596, 178, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3624;s:6:"height";i:5436;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn078.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn078-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn078-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn078-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn078-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381803";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:8:"0.000625";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(597, 179, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn082-1.jpg'),
(598, 179, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5391;s:6:"height";i:3594;s:4:"file";s:34:"2018/07/HsBeaute-seqnseqn082-1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:34:"HsBeaute-seqnseqn082-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:34:"HsBeaute-seqnseqn082-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:34:"HsBeaute-seqnseqn082-1-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:35:"HsBeaute-seqnseqn082-1-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381821";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:7:"0.00125";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(599, 180, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn084.jpg'),
(600, 180, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3586;s:6:"height";i:5379;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn084.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn084-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn084-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn084-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn084-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.2";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381834";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:7:"0.00125";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(601, 181, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn086.jpg'),
(602, 181, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3651;s:6:"height";i:5476;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn086.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn086-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn086-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn086-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn086-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.4";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381896";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:7:"0.00125";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(603, 182, '_wp_attached_file', '2018/07/HsBeaute-seqnseqn090.jpg'),
(604, 182, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3629;s:6:"height";i:5444;s:4:"file";s:32:"2018/07/HsBeaute-seqnseqn090.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn090-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:32:"HsBeaute-seqnseqn090-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn090-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"HsBeaute-seqnseqn090-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"1.4";s:6:"credit";s:0:"";s:6:"camera";s:20:"Canon EOS 5D Mark II";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1523381918";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"85";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:9:"0.0015625";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(605, 94, '_thumbnail_id', '171'),
(869, 94, 'Amandakaroline_galeria_imagens_servicos', '179'),
(868, 94, 'Amandakaroline_galeria_imagens_servicos', '178'),
(867, 94, 'Amandakaroline_galeria_imagens_servicos', '177'),
(866, 94, 'Amandakaroline_galeria_imagens_servicos', '176'),
(865, 94, 'Amandakaroline_galeria_imagens_servicos', '175'),
(864, 94, 'Amandakaroline_galeria_imagens_servicos', '174'),
(863, 94, 'Amandakaroline_galeria_imagens_servicos', '173'),
(617, 183, '_edit_last', '1'),
(618, 183, '_edit_lock', '1531341106:1'),
(619, 184, '_wp_attached_file', '2018/07/carrossel-4.png'),
(620, 184, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-4.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-4-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-4-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-4-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(621, 185, '_wp_attached_file', '2018/07/35666936_1855858947785753_8986319726498021376_n.jpg'),
(622, 185, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:768;s:6:"height";i:960;s:4:"file";s:59:"2018/07/35666936_1855858947785753_8986319726498021376_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"35666936_1855858947785753_8986319726498021376_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"35666936_1855858947785753_8986319726498021376_n-240x300.jpg";s:5:"width";i:240;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"35666936_1855858947785753_8986319726498021376_n-768x960.jpg";s:5:"width";i:768;s:6:"height";i:960;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(623, 186, '_wp_attached_file', '2018/07/16684059_1287276444692438_5059187177918994318_n.jpg'),
(624, 186, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:837;s:6:"height";i:960;s:4:"file";s:59:"2018/07/16684059_1287276444692438_5059187177918994318_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"16684059_1287276444692438_5059187177918994318_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"16684059_1287276444692438_5059187177918994318_n-262x300.jpg";s:5:"width";i:262;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"16684059_1287276444692438_5059187177918994318_n-768x881.jpg";s:5:"width";i:768;s:6:"height";i:881;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(625, 187, '_wp_attached_file', '2018/07/17264797_1313389965414419_6412324833316148071_n.jpg'),
(626, 187, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:910;s:6:"height";i:960;s:4:"file";s:59:"2018/07/17264797_1313389965414419_6412324833316148071_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"17264797_1313389965414419_6412324833316148071_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"17264797_1313389965414419_6412324833316148071_n-284x300.jpg";s:5:"width";i:284;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"17264797_1313389965414419_6412324833316148071_n-768x810.jpg";s:5:"width";i:768;s:6:"height";i:810;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(627, 188, '_wp_attached_file', '2018/07/17342651_1311622338924515_6478359688885335703_n.jpg'),
(628, 188, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:960;s:4:"file";s:59:"2018/07/17342651_1311622338924515_6478359688885335703_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"17342651_1311622338924515_6478359688885335703_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"17342651_1311622338924515_6478359688885335703_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"17342651_1311622338924515_6478359688885335703_n-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(629, 189, '_wp_attached_file', '2018/07/17353582_1226473577459781_2752955427670846283_n.jpg'),
(630, 189, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:960;s:4:"file";s:59:"2018/07/17353582_1226473577459781_2752955427670846283_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"17353582_1226473577459781_2752955427670846283_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"17353582_1226473577459781_2752955427670846283_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"17353582_1226473577459781_2752955427670846283_n-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(631, 190, '_wp_attached_file', '2018/07/17362681_1225811627525976_54261209401329020_n.jpg'),
(632, 190, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:960;s:4:"file";s:57:"2018/07/17362681_1225811627525976_54261209401329020_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:57:"17362681_1225811627525976_54261209401329020_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:57:"17362681_1225811627525976_54261209401329020_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:57:"17362681_1225811627525976_54261209401329020_n-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(633, 191, '_wp_attached_file', '2018/07/17951825_1249266415180497_761389869593649494_n.jpg'),
(634, 191, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:872;s:6:"height";i:960;s:4:"file";s:58:"2018/07/17951825_1249266415180497_761389869593649494_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:58:"17951825_1249266415180497_761389869593649494_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:58:"17951825_1249266415180497_761389869593649494_n-273x300.jpg";s:5:"width";i:273;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:58:"17951825_1249266415180497_761389869593649494_n-768x846.jpg";s:5:"width";i:768;s:6:"height";i:846;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(635, 192, '_wp_attached_file', '2018/07/23031541_1536869026399844_2931608818054581038_n.jpg'),
(636, 192, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:960;s:4:"file";s:59:"2018/07/23031541_1536869026399844_2931608818054581038_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"23031541_1536869026399844_2931608818054581038_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"23031541_1536869026399844_2931608818054581038_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"23031541_1536869026399844_2931608818054581038_n-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(637, 193, '_wp_attached_file', '2018/07/23316789_1542445372508876_3280173550834253973_n.jpg'),
(638, 193, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:960;s:4:"file";s:59:"2018/07/23316789_1542445372508876_3280173550834253973_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"23316789_1542445372508876_3280173550834253973_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"23316789_1542445372508876_3280173550834253973_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:59:"23316789_1542445372508876_3280173550834253973_n-768x768.jpg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(639, 194, '_wp_attached_file', '2018/07/30582263_1690576454362433_4529715240438333440_n.jpg'),
(640, 194, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:680;s:6:"height";i:960;s:4:"file";s:59:"2018/07/30582263_1690576454362433_4529715240438333440_n.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"30582263_1690576454362433_4529715240438333440_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"30582263_1690576454362433_4529715240438333440_n-213x300.jpg";s:5:"width";i:213;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(641, 183, '_thumbnail_id', '184'),
(642, 183, 'Amandakaroline_imagem_fundo_galeria', '185'),
(643, 183, 'Amandakaroline_galeria_imagens_servicos', '186'),
(644, 183, 'Amandakaroline_galeria_imagens_servicos', '187'),
(645, 183, 'Amandakaroline_galeria_imagens_servicos', '188'),
(646, 183, 'Amandakaroline_galeria_imagens_servicos', '189'),
(647, 183, 'Amandakaroline_galeria_imagens_servicos', '190'),
(648, 183, 'Amandakaroline_galeria_imagens_servicos', '191'),
(649, 183, 'Amandakaroline_galeria_imagens_servicos', '192'),
(650, 183, 'Amandakaroline_galeria_imagens_servicos', '193'),
(651, 183, 'Amandakaroline_galeria_imagens_servicos', '194'),
(652, 183, '_yoast_wpseo_content_score', '30'),
(653, 196, '_wp_attached_file', '2018/07/carrossel-3.png'),
(654, 196, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-3.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-3-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-3-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-3-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(655, 158, '_thumbnail_id', '196'),
(853, 158, 'Amandakaroline_galeria_imagens_servicos', '172'),
(852, 158, 'Amandakaroline_galeria_imagens_servicos', '171'),
(678, 197, '_edit_last', '1'),
(679, 197, '_edit_lock', '1531341114:1'),
(680, 198, '_wp_attached_file', '2018/07/carrossel-5.png'),
(681, 198, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-5.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-5-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-5-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-5-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(682, 197, '_thumbnail_id', '198'),
(683, 197, 'Amandakaroline_imagem_fundo_galeria', '167'),
(684, 197, 'Amandakaroline_galeria_imagens_servicos', '165'),
(685, 197, 'Amandakaroline_galeria_imagens_servicos', '164'),
(686, 197, 'Amandakaroline_galeria_imagens_servicos', '163'),
(687, 197, 'Amandakaroline_galeria_imagens_servicos', '161'),
(688, 197, 'Amandakaroline_galeria_imagens_servicos', '162'),
(689, 197, 'Amandakaroline_galeria_imagens_servicos', '166'),
(690, 197, 'Amandakaroline_galeria_imagens_servicos', '167'),
(691, 197, 'Amandakaroline_galeria_imagens_servicos', '160'),
(692, 197, 'Amandakaroline_galeria_imagens_servicos', '168'),
(693, 197, 'Amandakaroline_galeria_imagens_servicos', '169'),
(694, 197, '_yoast_wpseo_content_score', '30'),
(695, 199, '_edit_last', '1'),
(696, 199, '_edit_lock', '1531341121:1'),
(697, 200, '_wp_attached_file', '2018/07/8ce9b5f6-838d-4305-a205-1c1c3b151d55.jpg'),
(698, 200, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:48:"2018/07/8ce9b5f6-838d-4305-a205-1c1c3b151d55.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:48:"8ce9b5f6-838d-4305-a205-1c1c3b151d55-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:48:"8ce9b5f6-838d-4305-a205-1c1c3b151d55-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:48:"8ce9b5f6-838d-4305-a205-1c1c3b151d55-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:49:"8ce9b5f6-838d-4305-a205-1c1c3b151d55-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(699, 201, '_wp_attached_file', '2018/07/c9988187-e605-43b8-9b8d-850b6180d621.jpg'),
(700, 201, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:48:"2018/07/c9988187-e605-43b8-9b8d-850b6180d621.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:48:"c9988187-e605-43b8-9b8d-850b6180d621-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:48:"c9988187-e605-43b8-9b8d-850b6180d621-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:48:"c9988187-e605-43b8-9b8d-850b6180d621-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:49:"c9988187-e605-43b8-9b8d-850b6180d621-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(701, 202, '_wp_attached_file', '2018/07/IMG_3052-2.jpg'),
(702, 202, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:22:"2018/07/IMG_3052-2.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"IMG_3052-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"IMG_3052-2-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"IMG_3052-2-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"IMG_3052-2-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(703, 203, '_wp_attached_file', '2018/07/IMG_3053.jpg'),
(704, 203, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:20:"2018/07/IMG_3053.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_3053-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_3053-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_3053-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_3053-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(705, 204, '_wp_attached_file', '2018/07/IMG_3054.jpg'),
(706, 204, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:20:"2018/07/IMG_3054.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_3054-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_3054-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_3054-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_3054-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(707, 205, '_wp_attached_file', '2018/07/IMG_3055.jpg'),
(708, 205, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:20:"2018/07/IMG_3055.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_3055-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_3055-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_3055-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_3055-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(709, 206, '_wp_attached_file', '2018/07/IMG_3056.jpg'),
(710, 206, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:20:"2018/07/IMG_3056.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_3056-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_3056-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_3056-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_3056-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(711, 207, '_wp_attached_file', '2018/07/IMG_3062.jpg'),
(712, 207, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:20:"2018/07/IMG_3062.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_3062-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_3062-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_3062-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_3062-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(713, 208, '_wp_attached_file', '2018/07/IMG_3072.jpg'),
(714, 208, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:853;s:4:"file";s:20:"2018/07/IMG_3072.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_3072-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_3072-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_3072-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_3072-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(715, 209, '_wp_attached_file', '2018/07/sereia.jpg'),
(716, 209, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:639;s:4:"file";s:18:"2018/07/sereia.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"sereia-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"sereia-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:18:"sereia-768x511.jpg";s:5:"width";i:768;s:6:"height";i:511;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(717, 210, '_wp_attached_file', '2018/07/carrossel-6.png'),
(718, 210, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-6.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-6-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-6-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-6-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(719, 199, '_thumbnail_id', '210'),
(720, 199, 'Amandakaroline_imagem_fundo_galeria', '200'),
(721, 199, 'Amandakaroline_galeria_imagens_servicos', '209'),
(722, 199, 'Amandakaroline_galeria_imagens_servicos', '203'),
(723, 199, 'Amandakaroline_galeria_imagens_servicos', '202'),
(724, 199, 'Amandakaroline_galeria_imagens_servicos', '201'),
(725, 199, 'Amandakaroline_galeria_imagens_servicos', '208'),
(726, 199, 'Amandakaroline_galeria_imagens_servicos', '207'),
(727, 199, 'Amandakaroline_galeria_imagens_servicos', '200'),
(728, 199, 'Amandakaroline_galeria_imagens_servicos', '206'),
(729, 199, 'Amandakaroline_galeria_imagens_servicos', '205'),
(730, 199, 'Amandakaroline_galeria_imagens_servicos', '204'),
(731, 199, '_yoast_wpseo_content_score', '30'),
(732, 211, '_wp_attached_file', '2018/07/carrossel-7.png'),
(733, 211, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-7.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-7-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-7-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-7-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(738, 213, '_wp_attached_file', '2018/07/DSC00999.jpg'),
(739, 213, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5456;s:6:"height";i:3632;s:4:"file";s:20:"2018/07/DSC00999.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC00999-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"DSC00999-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC00999-768x511.jpg";s:5:"width";i:768;s:6:"height";i:511;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC00999-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(740, 214, '_wp_attached_file', '2018/07/DSC01000.jpg'),
(741, 214, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5456;s:6:"height";i:3632;s:4:"file";s:20:"2018/07/DSC01000.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC01000-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"DSC01000-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC01000-768x511.jpg";s:5:"width";i:768;s:6:"height";i:511;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC01000-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(742, 215, '_wp_attached_file', '2018/07/DSC01001.jpg'),
(743, 215, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5456;s:6:"height";i:3632;s:4:"file";s:20:"2018/07/DSC01001.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC01001-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"DSC01001-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC01001-768x511.jpg";s:5:"width";i:768;s:6:"height";i:511;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC01001-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(744, 216, '_wp_attached_file', '2018/07/DSC01034.jpg'),
(745, 216, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3632;s:6:"height";i:5456;s:4:"file";s:20:"2018/07/DSC01034.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC01034-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"DSC01034-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:21:"DSC01034-768x1154.jpg";s:5:"width";i:768;s:6:"height";i:1154;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC01034-682x1024.jpg";s:5:"width";i:682;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(746, 217, '_wp_attached_file', '2018/07/DSC01036.jpg'),
(747, 217, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3632;s:6:"height";i:5456;s:4:"file";s:20:"2018/07/DSC01036.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC01036-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"DSC01036-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:21:"DSC01036-768x1154.jpg";s:5:"width";i:768;s:6:"height";i:1154;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC01036-682x1024.jpg";s:5:"width";i:682;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(748, 218, '_wp_attached_file', '2018/07/DSC01039.jpg'),
(749, 218, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3632;s:6:"height";i:5456;s:4:"file";s:20:"2018/07/DSC01039.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC01039-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"DSC01039-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:21:"DSC01039-768x1154.jpg";s:5:"width";i:768;s:6:"height";i:1154;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC01039-682x1024.jpg";s:5:"width";i:682;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(752, 156, 'Amandakaroline_galeria_imagens_servicos', '215'),
(753, 156, 'Amandakaroline_galeria_imagens_servicos', '214'),
(754, 156, 'Amandakaroline_galeria_imagens_servicos', '213'),
(755, 156, 'Amandakaroline_galeria_imagens_servicos', '212'),
(756, 156, 'Amandakaroline_galeria_imagens_servicos', '218'),
(757, 156, 'Amandakaroline_galeria_imagens_servicos', '211'),
(758, 156, 'Amandakaroline_galeria_imagens_servicos', '217'),
(759, 219, '_edit_last', '1'),
(760, 219, '_yoast_wpseo_content_score', '30'),
(761, 219, '_edit_lock', '1531341132:1'),
(762, 220, '_wp_attached_file', '2018/07/carrossel-8.png'),
(763, 220, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-8.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-8-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-8-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-8-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(764, 221, '_wp_attached_file', '2018/07/IMG_2251.jpg'),
(765, 221, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2251.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2251-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2251-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2251-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2251-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060390";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:4:"0.02";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(766, 222, '_wp_attached_file', '2018/07/IMG_2252.jpg'),
(767, 222, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2252.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2252-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2252-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2252-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2252-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060390";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:4:"0.02";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(768, 223, '_wp_attached_file', '2018/07/IMG_2253.jpg'),
(769, 223, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2253.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2253-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2253-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2253-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2253-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060414";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:4:"0.02";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(770, 224, '_wp_attached_file', '2018/07/IMG_2256.jpg'),
(771, 224, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2256.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2256-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2256-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2256-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2256-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060416";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:4:"0.02";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(772, 225, '_wp_attached_file', '2018/07/IMG_2258.jpg');
INSERT INTO `ak_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(773, 225, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2258.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2258-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2258-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2258-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2258-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060429";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:4:"0.02";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(774, 226, '_wp_attached_file', '2018/07/IMG_2259.jpg'),
(775, 226, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2259.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2259-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2259-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2259-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2259-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060429";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:4:"0.02";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(776, 227, '_wp_attached_file', '2018/07/IMG_2262.jpg'),
(777, 227, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2262.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2262-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2262-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2262-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2262-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060451";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(778, 228, '_wp_attached_file', '2018/07/IMG_2263.jpg'),
(779, 228, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2263.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2263-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2263-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2263-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2263-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060451";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(780, 229, '_wp_attached_file', '2018/07/IMG_2266.jpg'),
(781, 229, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2266.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2266-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2266-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2266-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2266-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060458";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:5:"0.008";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(782, 230, '_wp_attached_file', '2018/07/IMG_2268.jpg'),
(783, 230, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:5184;s:6:"height";i:3456;s:4:"file";s:20:"2018/07/IMG_2268.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_2268-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_2268-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"IMG_2268-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_2268-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"4";s:6:"credit";s:0:"";s:6:"camera";s:18:"Canon EOS REBEL T5";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1531060480";s:9:"copyright";s:0:"";s:12:"focal_length";s:2:"50";s:3:"iso";s:3:"100";s:13:"shutter_speed";s:7:"0.00625";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(784, 219, '_thumbnail_id', '220'),
(785, 219, 'Amandakaroline_imagem_fundo_galeria', '229'),
(786, 219, 'Amandakaroline_galeria_imagens_servicos', '230'),
(787, 219, 'Amandakaroline_galeria_imagens_servicos', '229'),
(788, 219, 'Amandakaroline_galeria_imagens_servicos', '228'),
(789, 219, 'Amandakaroline_galeria_imagens_servicos', '227'),
(790, 219, 'Amandakaroline_galeria_imagens_servicos', '226'),
(791, 219, 'Amandakaroline_galeria_imagens_servicos', '225'),
(792, 219, 'Amandakaroline_galeria_imagens_servicos', '224'),
(793, 219, 'Amandakaroline_galeria_imagens_servicos', '222'),
(794, 219, 'Amandakaroline_galeria_imagens_servicos', '223'),
(795, 231, '_edit_last', '1'),
(796, 231, '_edit_lock', '1531496027:1'),
(797, 232, '_wp_attached_file', '2018/07/carrossel-9.png'),
(798, 232, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:564;s:6:"height";i:1422;s:4:"file";s:23:"2018/07/carrossel-9.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"carrossel-9-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"carrossel-9-119x300.png";s:5:"width";i:119;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"carrossel-9-406x1024.png";s:5:"width";i:406;s:6:"height";i:1024;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(799, 233, '_wp_attached_file', '2018/07/DSC8146.jpg'),
(800, 233, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC8146.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC8146-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC8146-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC8146-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC8146-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(801, 234, '_wp_attached_file', '2018/07/DSC7938.jpg'),
(802, 234, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC7938.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC7938-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC7938-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC7938-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC7938-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(803, 235, '_wp_attached_file', '2018/07/DSC7939.jpg'),
(804, 235, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC7939.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC7939-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC7939-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC7939-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC7939-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(805, 236, '_wp_attached_file', '2018/07/DSC7951.jpg'),
(806, 236, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:19:"2018/07/DSC7951.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC7951-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC7951-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:19:"DSC7951-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC7951-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(807, 237, '_wp_attached_file', '2018/07/DSC7954.jpg'),
(808, 237, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC7954.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC7954-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC7954-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC7954-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC7954-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(809, 238, '_wp_attached_file', '2018/07/DSC7955.jpg'),
(810, 238, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC7955.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC7955-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC7955-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC7955-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC7955-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(811, 239, '_wp_attached_file', '2018/07/DSC8140.jpg'),
(812, 239, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC8140.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC8140-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC8140-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC8140-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC8140-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(813, 240, '_wp_attached_file', '2018/07/DSC8146-1.jpg'),
(814, 240, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4608;s:6:"height";i:3072;s:4:"file";s:21:"2018/07/DSC8146-1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"DSC8146-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"DSC8146-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:21:"DSC8146-1-768x512.jpg";s:5:"width";i:768;s:6:"height";i:512;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"DSC8146-1-1024x683.jpg";s:5:"width";i:1024;s:6:"height";i:683;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(815, 241, '_wp_attached_file', '2018/07/DSC8153.jpg'),
(816, 241, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC8153.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC8153-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC8153-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC8153-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC8153-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(817, 242, '_wp_attached_file', '2018/07/DSC8163.jpg'),
(818, 242, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC8163.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC8163-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC8163-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC8163-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC8163-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(819, 243, '_wp_attached_file', '2018/07/DSC8172.jpg'),
(820, 243, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC8172.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC8172-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC8172-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC8172-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC8172-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(821, 244, '_wp_attached_file', '2018/07/DSC8174.jpg'),
(822, 244, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC8174.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC8174-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC8174-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC8174-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC8174-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(823, 245, '_wp_attached_file', '2018/07/DSC8175.jpg'),
(824, 245, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3072;s:6:"height";i:4608;s:4:"file";s:19:"2018/07/DSC8175.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"DSC8175-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC8175-200x300.jpg";s:5:"width";i:200;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"DSC8175-768x1152.jpg";s:5:"width";i:768;s:6:"height";i:1152;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:20:"DSC8175-683x1024.jpg";s:5:"width";i:683;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(825, 231, '_thumbnail_id', '232'),
(826, 231, 'Amandakaroline_imagem_fundo_galeria', '233'),
(827, 231, 'Amandakaroline_galeria_imagens_servicos', '234'),
(828, 231, 'Amandakaroline_galeria_imagens_servicos', '235'),
(829, 231, 'Amandakaroline_galeria_imagens_servicos', '236'),
(830, 231, 'Amandakaroline_galeria_imagens_servicos', '237'),
(831, 231, 'Amandakaroline_galeria_imagens_servicos', '238'),
(832, 231, 'Amandakaroline_galeria_imagens_servicos', '239'),
(833, 231, 'Amandakaroline_galeria_imagens_servicos', '240'),
(834, 231, 'Amandakaroline_galeria_imagens_servicos', '241'),
(835, 231, 'Amandakaroline_galeria_imagens_servicos', '242'),
(836, 231, 'Amandakaroline_galeria_imagens_servicos', '243'),
(837, 231, 'Amandakaroline_galeria_imagens_servicos', '244'),
(838, 231, 'Amandakaroline_galeria_imagens_servicos', '245'),
(839, 231, '_yoast_wpseo_content_score', '30'),
(873, 155, '_wp_trash_meta_status', 'publish'),
(874, 155, '_wp_trash_meta_time', '1531341175'),
(875, 155, '_wp_desired_post_slug', 'ensaio-fotografico-darlan-lara'),
(876, 154, '_wp_trash_meta_status', 'publish'),
(877, 154, '_wp_trash_meta_time', '1531341187'),
(878, 154, '_wp_desired_post_slug', 'ensaio-fotografico-2o-sandra-mara'),
(879, 150, '_wp_trash_meta_status', 'publish'),
(880, 150, '_wp_trash_meta_time', '1531341194'),
(881, 150, '_wp_desired_post_slug', 'ensaio-fotografico-1o-sandra-mara');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_posts`
--

CREATE TABLE `ak_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_posts`
--

INSERT INTO `ak_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-06-19 15:44:03', '2018-06-19 18:44:03', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2018-06-19 15:44:03', '2018-06-19 18:44:03', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=1', 0, 'post', '', 1),
(2, 1, '2018-06-19 15:44:03', '2018-06-19 18:44:03', 'Este é o exemplo de uma página. É diferente de um post de blog porque é estática e pode aparecer em menus de navegação (na maioria dos temas). A maioria das pessoas começam com uma página \'Sobre\' que as apresenta aos potenciais visitantes do site. Você pode usar algo como:\n\n<blockquote>Oi! Sou um estudante de Biologia e gosto de esportes e natureza. Nos fins de semana pratico futebol com meus amigos no clube local. Eu moro em Valinhos e fiz este site para falar sobre minha cidade.</blockquote>\n\n...ou algo como:\n\n<blockquote>A empresa Logos foi fundada em 1980, e tem provido o comércio local com o que há de melhor em informatização. Localizada em Recife, nossa empresa tem se destacado como um das que também contribuem para o descarte correto de equipamentos eletrônicos substituídos.</blockquote>\n\nComo um novo usuário WordPress, vá ao seu <a href="http://localhost/projetos/amandakaroline_portfolio/wp-admin/">Painel</a> para excluir este conteúdo e criar o seu. Divirta-se!', 'Página de exemplo', '', 'trash', 'closed', 'open', '', 'pagina-exemplo__trashed', '', '', '2018-07-10 15:57:33', '2018-07-10 18:57:33', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-06-19 15:44:03', '2018-06-19 18:44:03', '<h2>Quem somos</h2><p>O endereço do nosso site é: http://localhost/projetos/amandakaroline_portfolio.</p><h2>Quais dados pessoais coletamos e porque</h2><h3>Comentários</h3><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><h3>Mídia</h3><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><h3>Formulários de contato</h3><h3>Cookies</h3><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><h3>Mídia incorporada de outros sites</h3><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicionar de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><h3>Análises</h3><h2>Com quem partilhamos seus dados</h2><h2>Por quanto tempo mantemos os seus dados</h2><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><h2>Quais os seus direitos sobre seus dados</h2><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><h2>Para onde enviamos seus dados</h2><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><h2>Suas informações de contato</h2><h2>Informações adicionais</h2><h3>Como protegemos seus dados</h3><h3>Quais são nossos procedimentos contra violação de dados</h3><h3>De quais terceiros nós recebemos dados</h3><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3>', 'Política de privacidade', '', 'trash', 'closed', 'open', '', 'politica-de-privacidade__trashed', '', '', '2018-07-10 15:57:33', '2018-07-10 18:57:33', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=3', 0, 'page', '', 0),
(5, 1, '2018-07-05 12:15:03', '2018-07-05 15:15:03', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]\nAmanda Karoline "[your-subject]"\n[your-name] <carolinoamandacs@gmail.com>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Amanda Karoline (http://localhost/projetos/amandakaroline_portfolio)\ncarolinoamandacs@gmail.com\nReply-To: [your-email]\n\n0\n0\n\nAmanda Karoline "[your-subject]"\nAmanda Karoline <carolinoamandacs@gmail.com>\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Amanda Karoline (http://localhost/projetos/amandakaroline_portfolio)\n[your-email]\nReply-To: carolinoamandacs@gmail.com\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2018-07-05 12:15:03', '2018-07-05 15:15:03', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=wpcf7_contact_form&p=5', 0, 'wpcf7_contact_form', '', 0),
(246, 1, '2018-07-12 18:29:29', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-07-12 18:29:29', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=246', 0, 'post', '', 0),
(18, 1, '2018-07-05 17:52:22', '2018-07-05 20:52:22', '"Mulher incrível, diversos projetos desenvolvidos em parceria. Sempre disposta a participar e sempre conquistando os objetivos"', 'Hayana Sanquetta', '', 'trash', 'closed', 'closed', '', 'hayana-sanquetta__trashed', '', '', '2018-07-05 18:02:26', '2018-07-05 21:02:26', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=depoimento&#038;p=18', 0, 'depoimento', '', 0),
(19, 1, '2018-07-05 17:52:01', '2018-07-05 20:52:01', '', 'hay', '', 'inherit', 'open', 'closed', '', 'hay', '', '', '2018-07-05 17:52:01', '2018-07-05 20:52:01', '', 18, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/hay.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2018-07-05 18:10:13', '2018-07-05 21:10:13', '', 'Hayana Sanquetta', '', 'publish', 'closed', 'closed', '', 'hayana-sanquetta', '', '', '2018-07-05 18:16:19', '2018-07-05 21:16:19', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=depoimento&#038;p=25', 0, 'depoimento', '', 0),
(26, 1, '2018-07-05 18:13:31', '2018-07-05 21:13:31', '', '', '', 'inherit', 'closed', 'closed', '', '25-autosave-v1', '', '', '2018-07-05 18:13:31', '2018-07-05 21:13:31', '', 25, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/05/25-autosave-v1/', 0, 'revision', '', 0),
(27, 1, '2018-07-06 09:38:08', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-06 09:38:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=parceria&p=27', 0, 'parceria', '', 0),
(28, 1, '2018-07-06 09:41:23', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-06 09:41:23', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=parceria&p=28', 0, 'parceria', '', 0),
(29, 1, '2018-07-06 09:57:31', '2018-07-06 12:57:31', '', 'HC Desenvolvimentos', '', 'publish', 'closed', 'closed', '', 'hc-desenvolvimentos', '', '', '2018-07-11 10:28:50', '2018-07-11 13:28:50', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=parceria&#038;p=29', 0, 'parceria', '', 0),
(30, 1, '2018-07-06 09:57:14', '2018-07-06 12:57:14', '', 'logoHC', '', 'inherit', 'open', 'closed', '', 'logohc', '', '', '2018-07-06 09:57:14', '2018-07-06 12:57:14', '', 29, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logoHC.png', 0, 'attachment', 'image/png', 0),
(31, 1, '2018-07-06 09:59:39', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-06 09:59:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=servicos&p=31', 0, 'servicos', '', 0),
(32, 1, '2018-07-06 10:01:46', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-06 10:01:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=servicos&p=32', 0, 'servicos', '', 0),
(33, 1, '2018-07-06 10:02:04', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-06 10:02:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=servicos&p=33', 0, 'servicos', '', 0),
(34, 1, '2018-07-06 10:04:20', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-06 10:04:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=servicos&p=34', 0, 'servicos', '', 0),
(35, 1, '2018-07-10 09:33:17', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:33:17', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=servicos&p=35', 0, 'servicos', '', 0),
(36, 1, '2018-07-10 09:48:39', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:48:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=36', 0, 'sessao-inicial', '', 0),
(37, 1, '2018-07-10 09:50:24', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:50:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=37', 0, 'sessao-inicial', '', 0),
(38, 1, '2018-07-10 09:52:40', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:52:40', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=38', 0, 'sessao-inicial', '', 0),
(39, 1, '2018-07-10 09:54:50', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:54:50', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=39', 0, 'sessao-inicial', '', 0),
(40, 1, '2018-07-10 09:55:32', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:55:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=40', 0, 'sessao-inicial', '', 0),
(41, 1, '2018-07-10 09:56:09', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:56:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=41', 0, 'sessao-inicial', '', 0),
(42, 1, '2018-07-10 09:56:53', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:56:53', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=42', 0, 'sessao-inicial', '', 0),
(43, 1, '2018-07-10 09:57:43', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 09:57:43', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=43', 0, 'sessao-inicial', '', 0),
(44, 1, '2018-07-10 10:00:04', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:00:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=44', 0, 'sessao-inicial', '', 0),
(45, 1, '2018-07-10 10:00:49', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:00:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=45', 0, 'sessao-inicial', '', 0),
(46, 1, '2018-07-10 10:01:22', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:01:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=46', 0, 'sessao-inicial', '', 0),
(47, 1, '2018-07-10 10:06:15', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:06:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=servicos&p=47', 0, 'servicos', '', 0),
(48, 1, '2018-07-10 10:11:21', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:11:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=sessao-inicial&p=48', 0, 'sessao-inicial', '', 0),
(49, 1, '2018-07-10 10:15:12', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:15:12', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=servicos&p=49', 0, 'servicos', '', 0),
(50, 1, '2018-07-10 10:17:27', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:17:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=depoimento&p=50', 0, 'depoimento', '', 0),
(51, 1, '2018-07-10 10:17:45', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:17:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=parceria&p=51', 0, 'parceria', '', 0),
(52, 1, '2018-07-10 10:24:10', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:24:10', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&p=52', 0, 'portfolio', '', 0),
(53, 1, '2018-07-10 10:24:41', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:24:41', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&p=53', 0, 'portfolio', '', 0),
(54, 1, '2018-07-10 10:26:05', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:26:05', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&p=54', 0, 'portfolio', '', 0),
(55, 1, '2018-07-10 10:27:04', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:27:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=banner-destaque&p=55', 0, 'banner-destaque', '', 0),
(56, 1, '2018-07-10 10:29:12', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:29:12', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=banner-destaque&p=56', 0, 'banner-destaque', '', 0),
(57, 1, '2018-07-10 10:29:45', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:29:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=banner-destaque&p=57', 0, 'banner-destaque', '', 0),
(58, 1, '2018-07-10 10:30:47', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:30:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=banner-destaque&p=58', 0, 'banner-destaque', '', 0),
(59, 1, '2018-07-10 10:32:34', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-10 10:32:34', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=banner-destaque&p=59', 0, 'banner-destaque', '', 0),
(60, 1, '2018-07-10 13:34:57', '2018-07-10 16:34:57', '', 'logo-fixa', '', 'inherit', 'open', 'closed', '', 'logo-fixa', '', '', '2018-07-10 13:34:57', '2018-07-10 16:34:57', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logo-fixa.png', 0, 'attachment', 'image/png', 0),
(61, 1, '2018-07-10 13:36:19', '2018-07-10 16:36:19', '', 'opacidade-4', '', 'inherit', 'open', 'closed', '', 'opacidade-4', '', '', '2018-07-10 13:36:19', '2018-07-10 16:36:19', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/opacidade-4.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2018-07-10 13:37:17', '2018-07-10 16:37:17', '', 'rosa-na-boca', '', 'inherit', 'open', 'closed', '', 'rosa-na-boca', '', '', '2018-07-10 13:37:17', '2018-07-10 16:37:17', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/rosa-na-boca.png', 0, 'attachment', 'image/png', 0),
(64, 1, '2018-07-10 13:44:21', '2018-07-10 16:44:21', '', '_DSC0142', '', 'inherit', 'open', 'closed', '', '_dsc0142', '', '', '2018-07-10 13:44:21', '2018-07-10 16:44:21', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC0142.jpg', 0, 'attachment', 'image/jpeg', 0),
(65, 1, '2018-07-10 13:48:34', '2018-07-10 16:48:34', '', 'imagem-sobremimCentro', '', 'inherit', 'open', 'closed', '', 'imagem-sobremimcentro', '', '', '2018-07-10 13:48:34', '2018-07-10 16:48:34', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/imagem-sobremimCentro.jpg', 0, 'attachment', 'image/jpeg', 0),
(66, 1, '2018-07-10 13:51:46', '2018-07-10 16:51:46', '', 'DSC01016', '', 'inherit', 'open', 'closed', '', 'dsc01016', '', '', '2018-07-10 13:51:46', '2018-07-10 16:51:46', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC01016.jpg', 0, 'attachment', 'image/jpeg', 0),
(169, 1, '2018-07-11 15:14:41', '2018-07-11 18:14:41', '', '_DSC1250', '', 'inherit', 'open', 'closed', '', '_dsc1250', '', '', '2018-07-11 15:14:41', '2018-07-11 18:14:41', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1250.jpg', 0, 'attachment', 'image/jpeg', 0),
(161, 1, '2018-07-11 15:13:45', '2018-07-11 18:13:45', '', '_DSC1227', '', 'inherit', 'open', 'closed', '', '_dsc1227-2', '', '', '2018-07-11 15:13:45', '2018-07-11 18:13:45', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1227-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(141, 1, '2018-07-11 14:32:30', '2018-07-11 17:32:30', '', 'dadosv4', '', 'inherit', 'open', 'closed', '', 'dadosv4', '', '', '2018-07-11 14:32:30', '2018-07-11 17:32:30', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/dadosv4.png', 0, 'attachment', 'image/png', 0),
(70, 1, '2018-07-10 15:57:33', '2018-07-10 18:57:33', '<h2>Quem somos</h2><p>O endereço do nosso site é: http://localhost/projetos/amandakaroline_portfolio.</p><h2>Quais dados pessoais coletamos e porque</h2><h3>Comentários</h3><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><h3>Mídia</h3><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><h3>Formulários de contato</h3><h3>Cookies</h3><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><h3>Mídia incorporada de outros sites</h3><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicionar de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><h3>Análises</h3><h2>Com quem partilhamos seus dados</h2><h2>Por quanto tempo mantemos os seus dados</h2><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><h2>Quais os seus direitos sobre seus dados</h2><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><h2>Para onde enviamos seus dados</h2><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><h2>Suas informações de contato</h2><h2>Informações adicionais</h2><h3>Como protegemos seus dados</h3><h3>Quais são nossos procedimentos contra violação de dados</h3><h3>De quais terceiros nós recebemos dados</h3><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3>', 'Política de privacidade', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2018-07-10 15:57:33', '2018-07-10 18:57:33', '', 3, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/10/3-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2018-07-10 15:57:33', '2018-07-10 18:57:33', 'Este é o exemplo de uma página. É diferente de um post de blog porque é estática e pode aparecer em menus de navegação (na maioria dos temas). A maioria das pessoas começam com uma página \'Sobre\' que as apresenta aos potenciais visitantes do site. Você pode usar algo como:\n\n<blockquote>Oi! Sou um estudante de Biologia e gosto de esportes e natureza. Nos fins de semana pratico futebol com meus amigos no clube local. Eu moro em Valinhos e fiz este site para falar sobre minha cidade.</blockquote>\n\n...ou algo como:\n\n<blockquote>A empresa Logos foi fundada em 1980, e tem provido o comércio local com o que há de melhor em informatização. Localizada em Recife, nossa empresa tem se destacado como um das que também contribuem para o descarte correto de equipamentos eletrônicos substituídos.</blockquote>\n\nComo um novo usuário WordPress, vá ao seu <a href="http://localhost/projetos/amandakaroline_portfolio/wp-admin/">Painel</a> para excluir este conteúdo e criar o seu. Divirta-se!', 'Página de exemplo', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-07-10 15:57:33', '2018-07-10 18:57:33', '', 2, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/10/2-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2018-07-10 15:57:52', '2018-07-10 18:57:52', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2018-07-10 15:58:30', '2018-07-10 18:58:30', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=72', 0, 'page', '', 0),
(73, 1, '2018-07-10 15:57:52', '2018-07-10 18:57:52', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '72-revision-v1', '', '', '2018-07-10 15:57:52', '2018-07-10 18:57:52', '', 72, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/10/72-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2018-07-10 16:08:38', '2018-07-10 19:08:38', '', 'Video Destaque', '', 'pending', 'closed', 'closed', '', 'video-destaque', '', '', '2018-07-10 17:08:54', '2018-07-10 20:08:54', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=banner-destaque&#038;p=74', 0, 'banner-destaque', '', 0),
(75, 1, '2018-07-10 16:07:50', '2018-07-10 19:07:50', '', 'videoteste', '', 'inherit', 'open', 'closed', '', 'videoteste', '', '', '2018-07-10 16:07:50', '2018-07-10 19:07:50', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/videoteste.mp4', 0, 'attachment', 'video/mp4', 0),
(160, 1, '2018-07-11 15:13:30', '2018-07-11 18:13:30', '', '_DSC1227', '', 'inherit', 'open', 'closed', '', '_dsc1227', '', '', '2018-07-11 15:13:30', '2018-07-11 18:13:30', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1227.jpg', 0, 'attachment', 'image/jpeg', 0),
(77, 1, '2018-07-10 16:08:31', '2018-07-10 19:08:31', '', 'backgroundGaleria', '', 'inherit', 'open', 'closed', '', 'backgroundgaleria', '', '', '2018-07-10 16:08:31', '2018-07-10 19:08:31', '', 74, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/backgroundGaleria.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 1, '2018-07-10 16:08:44', '2018-07-10 19:08:44', '', 'Video Destaque', '', 'inherit', 'closed', 'closed', '', '74-autosave-v1', '', '', '2018-07-10 16:08:44', '2018-07-10 19:08:44', '', 74, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/10/74-autosave-v1/', 0, 'revision', '', 0),
(79, 1, '2018-07-10 16:10:11', '2018-07-10 19:10:11', '', 'Imagem destaque', '', 'publish', 'closed', 'closed', '', 'imagem-destaque', '', '', '2018-07-10 16:10:11', '2018-07-10 19:10:11', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=banner-destaque&#038;p=79', 0, 'banner-destaque', '', 0),
(80, 1, '2018-07-10 16:15:06', '2018-07-10 19:15:06', 'Ver descrição do Projeto...', 'Ensaio numero 1', '', 'publish', 'closed', 'closed', '', 'ensaio-fotografico-1o-dani-lela', '', '', '2018-07-11 17:27:20', '2018-07-11 20:27:20', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=80', 8, 'portfolio', '', 0),
(168, 1, '2018-07-11 15:14:35', '2018-07-11 18:14:35', '', '_DSC1245', '', 'inherit', 'open', 'closed', '', '_dsc1245', '', '', '2018-07-11 15:14:35', '2018-07-11 18:14:35', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1245.jpg', 0, 'attachment', 'image/jpeg', 0),
(162, 1, '2018-07-11 15:13:51', '2018-07-11 18:13:51', '', '_DSC1228', '', 'inherit', 'open', 'closed', '', '_dsc1228', '', '', '2018-07-11 15:13:51', '2018-07-11 18:13:51', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1228.jpg', 0, 'attachment', 'image/jpeg', 0),
(163, 1, '2018-07-11 15:13:58', '2018-07-11 18:13:58', '', '_DSC1232', '', 'inherit', 'open', 'closed', '', '_dsc1232', '', '', '2018-07-11 15:13:58', '2018-07-11 18:13:58', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1232.jpg', 0, 'attachment', 'image/jpeg', 0),
(164, 1, '2018-07-11 15:14:05', '2018-07-11 18:14:05', '', '_DSC1235', '', 'inherit', 'open', 'closed', '', '_dsc1235', '', '', '2018-07-11 15:14:05', '2018-07-11 18:14:05', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1235.jpg', 0, 'attachment', 'image/jpeg', 0),
(165, 1, '2018-07-11 15:14:15', '2018-07-11 18:14:15', '', '_DSC1237', '', 'inherit', 'open', 'closed', '', '_dsc1237', '', '', '2018-07-11 15:14:15', '2018-07-11 18:14:15', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1237.jpg', 0, 'attachment', 'image/jpeg', 0),
(166, 1, '2018-07-11 15:14:24', '2018-07-11 18:14:24', '', '_DSC1239', '', 'inherit', 'open', 'closed', '', '_dsc1239', '', '', '2018-07-11 15:14:24', '2018-07-11 18:14:24', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1239.jpg', 0, 'attachment', 'image/jpeg', 0),
(167, 1, '2018-07-11 15:14:29', '2018-07-11 18:14:29', '', '_DSC1241', '', 'inherit', 'open', 'closed', '', '_dsc1241', '', '', '2018-07-11 15:14:29', '2018-07-11 18:14:29', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1241.jpg', 0, 'attachment', 'image/jpeg', 0),
(94, 1, '2018-07-11 09:22:56', '2018-07-11 12:22:56', 'descrição do serviço', 'Ensaio numero 2', '', 'publish', 'closed', 'closed', '', 'ensaio-fotografico-2o-dani-lela', '', '', '2018-07-11 17:32:36', '2018-07-11 20:32:36', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=94', 7, 'portfolio', '', 0),
(170, 1, '2018-07-11 15:14:47', '2018-07-11 18:14:47', '', '_DSC1252', '', 'inherit', 'open', 'closed', '', '_dsc1252', '', '', '2018-07-11 15:14:47', '2018-07-11 18:14:47', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC1252.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2018-07-10 16:18:34', '2018-07-10 19:18:34', '', 'Imagem destaque 2', '', 'publish', 'closed', 'closed', '', 'imagem-destaque-2', '', '', '2018-07-10 16:18:34', '2018-07-10 19:18:34', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=banner-destaque&#038;p=89', 0, 'banner-destaque', '', 0),
(90, 1, '2018-07-10 16:36:01', '2018-07-10 19:36:01', '', 'Imagem destaque', '', 'publish', 'closed', 'closed', '', 'imagem-destaque', '', '', '2018-07-10 16:36:01', '2018-07-10 19:36:01', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=destaque&#038;p=90', 0, 'destaque', '', 0),
(91, 1, '2018-07-10 16:36:31', '2018-07-10 19:36:31', '', 'Imagem destaque 2', '', 'draft', 'closed', 'closed', '', '91', '', '', '2018-07-10 17:02:52', '2018-07-10 20:02:52', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=destaque&#038;p=91', 0, 'destaque', '', 0),
(92, 1, '2018-07-10 17:09:44', '2018-07-10 20:09:44', '', 'Video Destaque', '', 'publish', 'closed', 'closed', '', 'video-destaque', '', '', '2018-07-10 17:10:58', '2018-07-10 20:10:58', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=destaque&#038;p=92', 0, 'destaque', '', 0),
(93, 1, '2018-07-10 20:44:05', '2018-07-10 23:44:05', '', 'logoSobreMim', '', 'inherit', 'open', 'closed', '', 'logosobremim', '', '', '2018-07-10 20:44:05', '2018-07-10 23:44:05', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logoSobreMim.png', 0, 'attachment', 'image/png', 0),
(171, 1, '2018-07-11 15:17:56', '2018-07-11 18:17:56', '', 'carrossel-2', '', 'inherit', 'open', 'closed', '', 'carrossel-2', '', '', '2018-07-11 15:17:56', '2018-07-11 18:17:56', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-2.png', 0, 'attachment', 'image/png', 0),
(96, 1, '2018-07-11 09:25:57', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-11 09:25:57', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&p=96', 0, 'portfolio', '', 0),
(97, 1, '2018-07-11 09:54:10', '2018-07-11 12:54:10', '', '100', '', 'inherit', 'open', 'closed', '', '100', '', '', '2018-07-11 09:54:10', '2018-07-11 12:54:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/100.jpg', 0, 'attachment', 'image/jpeg', 0),
(98, 1, '2018-07-11 09:57:55', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-11 09:57:55', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=depoimento&p=98', 0, 'depoimento', '', 0),
(99, 1, '2018-07-11 10:30:42', '2018-07-11 13:30:42', '', 'EM Design', '', 'publish', 'closed', 'closed', '', 'em-design', '', '', '2018-07-11 10:30:42', '2018-07-11 13:30:42', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=parceria&#038;p=99', 0, 'parceria', '', 0),
(100, 1, '2018-07-11 10:29:49', '2018-07-11 13:29:49', '', 'logoedu', '', 'inherit', 'open', 'closed', '', 'logoedu', '', '', '2018-07-11 10:29:49', '2018-07-11 13:29:49', '', 99, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logoedu.png', 0, 'attachment', 'image/png', 0),
(101, 1, '2018-07-11 10:32:15', '2018-07-11 13:32:15', '', 'DL Designer', '', 'publish', 'closed', 'closed', '', 'dl-designer', '', '', '2018-07-11 11:17:51', '2018-07-11 14:17:51', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=parceria&#038;p=101', 0, 'parceria', '', 0),
(109, 1, '2018-07-11 11:17:43', '2018-07-11 14:17:43', '', 'logodarlan', '', 'inherit', 'open', 'closed', '', 'logodarlan', '', '', '2018-07-11 11:17:43', '2018-07-11 14:17:43', '', 101, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logodarlan.png', 0, 'attachment', 'image/png', 0),
(103, 1, '2018-07-11 10:40:36', '2018-07-11 13:40:36', '', 'Hayana Sanquetta', '', 'publish', 'closed', 'closed', '', 'hayana-sanquetta', '', '', '2018-07-11 10:54:24', '2018-07-11 13:54:24', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=parceria&#038;p=103', 0, 'parceria', '', 0),
(106, 1, '2018-07-11 10:54:19', '2018-07-11 13:54:19', '', 'logoHayana', '', 'inherit', 'open', 'closed', '', 'logohayana', '', '', '2018-07-11 10:54:19', '2018-07-11 13:54:19', '', 103, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/logoHayana.png', 0, 'attachment', 'image/png', 0),
(107, 1, '2018-07-11 11:14:34', '2018-07-11 14:14:34', '', 'Du Tranças', '', 'publish', 'closed', 'closed', '', 'du-trancas', '', '', '2018-07-11 13:46:19', '2018-07-11 16:46:19', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=parceria&#038;p=107', 0, 'parceria', '', 0),
(140, 1, '2018-07-11 13:46:11', '2018-07-11 16:46:11', '', 'dutrancas', '', 'inherit', 'open', 'closed', '', 'dutrancas', '', '', '2018-07-11 13:46:11', '2018-07-11 16:46:11', '', 107, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/dutrancas.png', 0, 'attachment', 'image/png', 0),
(110, 1, '2018-07-11 13:29:48', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-07-11 13:29:48', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=110', 1, 'nav_menu_item', '', 0),
(150, 1, '2018-07-11 14:55:35', '2018-07-11 17:55:35', 'Ver descrição do Projeto...', 'Ensaio Fotográfico 1º Sandra Mara', '', 'trash', 'closed', 'closed', '', 'ensaio-fotografico-1o-sandra-mara__trashed', '', '', '2018-07-11 17:33:14', '2018-07-11 20:33:14', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=150', 0, 'portfolio', '', 0),
(112, 1, '2018-07-11 13:30:36', '2018-07-11 16:30:36', '', 'Início', '', 'trash', 'closed', 'closed', '', 'inicio__trashed', '', '', '2018-07-11 14:39:25', '2018-07-11 17:39:25', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=112', 0, 'page', '', 0),
(113, 1, '2018-07-11 13:30:36', '2018-07-11 16:30:36', '', 'Início', '', 'inherit', 'closed', 'closed', '', '112-revision-v1', '', '', '2018-07-11 13:30:36', '2018-07-11 16:30:36', '', 112, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/11/112-revision-v1/', 0, 'revision', '', 0),
(114, 1, '2018-07-11 13:30:49', '2018-07-11 16:30:49', '', 'Portfolio', '', 'trash', 'closed', 'closed', '', 'portfolio__trashed', '', '', '2018-07-11 14:39:25', '2018-07-11 17:39:25', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=114', 0, 'page', '', 0),
(115, 1, '2018-07-11 13:30:49', '2018-07-11 16:30:49', '', 'Portfolio', '', 'inherit', 'closed', 'closed', '', '114-revision-v1', '', '', '2018-07-11 13:30:49', '2018-07-11 16:30:49', '', 114, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/11/114-revision-v1/', 0, 'revision', '', 0),
(116, 1, '2018-07-11 13:31:27', '2018-07-11 16:31:27', '', 'Sobre Mim', '', 'trash', 'closed', 'closed', '', 'sobre-mim__trashed', '', '', '2018-07-11 14:39:25', '2018-07-11 17:39:25', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=116', 0, 'page', '', 0),
(117, 1, '2018-07-11 13:31:27', '2018-07-11 16:31:27', '', 'Sobre Mim', '', 'inherit', 'closed', 'closed', '', '116-revision-v1', '', '', '2018-07-11 13:31:27', '2018-07-11 16:31:27', '', 116, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/11/116-revision-v1/', 0, 'revision', '', 0),
(118, 1, '2018-07-11 13:31:47', '2018-07-11 16:31:47', '', 'Estatística', '', 'trash', 'closed', 'closed', '', 'estatistica__trashed', '', '', '2018-07-11 14:39:25', '2018-07-11 17:39:25', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=118', 0, 'page', '', 0),
(119, 1, '2018-07-11 13:31:47', '2018-07-11 16:31:47', '', 'Estatística', '', 'inherit', 'closed', 'closed', '', '118-revision-v1', '', '', '2018-07-11 13:31:47', '2018-07-11 16:31:47', '', 118, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/11/118-revision-v1/', 0, 'revision', '', 0),
(120, 1, '2018-07-11 13:32:05', '2018-07-11 16:32:05', '', 'Qualidades', '', 'trash', 'closed', 'closed', '', 'qualidades__trashed', '', '', '2018-07-11 14:39:25', '2018-07-11 17:39:25', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=120', 0, 'page', '', 0),
(121, 1, '2018-07-11 13:32:05', '2018-07-11 16:32:05', '', 'Qualidades', '', 'inherit', 'closed', 'closed', '', '120-revision-v1', '', '', '2018-07-11 13:32:05', '2018-07-11 16:32:05', '', 120, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/11/120-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2018-07-11 13:32:19', '2018-07-11 16:32:19', '', 'Contato', '', 'trash', 'closed', 'closed', '', 'contato__trashed', '', '', '2018-07-11 14:39:25', '2018-07-11 17:39:25', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=122', 0, 'page', '', 0),
(123, 1, '2018-07-11 13:32:19', '2018-07-11 16:32:19', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '122-revision-v1', '', '', '2018-07-11 13:32:19', '2018-07-11 16:32:19', '', 122, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/11/122-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2018-07-11 13:32:30', '2018-07-11 16:32:30', '', 'Blog', '', 'trash', 'closed', 'closed', '', 'blog__trashed', '', '', '2018-07-11 14:39:25', '2018-07-11 17:39:25', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=124', 0, 'page', '', 0),
(125, 1, '2018-07-11 13:32:30', '2018-07-11 16:32:30', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '124-revision-v1', '', '', '2018-07-11 13:32:30', '2018-07-11 16:32:30', '', 124, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/11/124-revision-v1/', 0, 'revision', '', 0),
(126, 1, '2018-07-11 13:32:39', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-11 13:32:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=126', 0, 'page', '', 0),
(127, 1, '2018-07-11 13:32:47', '2018-07-11 16:32:47', '', 'Loja', '', 'trash', 'closed', 'closed', '', 'loja__trashed', '', '', '2018-07-11 14:39:25', '2018-07-11 17:39:25', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?page_id=127', 0, 'page', '', 0),
(128, 1, '2018-07-11 13:32:47', '2018-07-11 16:32:47', '', 'Loja', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2018-07-11 13:32:47', '2018-07-11 16:32:47', '', 127, 'http://localhost/projetos/amandakaroline_portfolio/2018/07/11/127-revision-v1/', 0, 'revision', '', 0),
(155, 1, '2018-07-11 14:58:26', '2018-07-11 17:58:26', 'Ensaio Fotográfico Darlan Lara', 'Ensaio Fotográfico Darlan Lara', '', 'trash', 'closed', 'closed', '', 'ensaio-fotografico-darlan-lara__trashed', '', '', '2018-07-11 17:32:55', '2018-07-11 20:32:55', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=155', 0, 'portfolio', '', 0),
(154, 1, '2018-07-11 14:56:39', '2018-07-11 17:56:39', 'Ensaio Urbano ', 'Ensaio Fotográfico 2º Sandra Mara', '', 'trash', 'closed', 'closed', '', 'ensaio-fotografico-2o-sandra-mara__trashed', '', '', '2018-07-11 17:33:07', '2018-07-11 20:33:07', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=154', 0, 'portfolio', '', 0),
(174, 1, '2018-07-11 15:18:55', '2018-07-11 18:18:55', '', 'HsBeaute {seqn{seqn}022', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn022', '', '', '2018-07-11 15:18:55', '2018-07-11 18:18:55', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn022.jpg', 0, 'attachment', 'image/jpeg', 0),
(172, 1, '2018-07-11 15:18:15', '2018-07-11 18:18:15', '', 'HsBeaute {seqn{seqn}082', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn082', '', '', '2018-07-11 15:18:15', '2018-07-11 18:18:15', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn082.jpg', 0, 'attachment', 'image/jpeg', 0),
(136, 1, '2018-07-11 13:33:13', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-07-11 13:33:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=136', 1, 'nav_menu_item', '', 0),
(173, 1, '2018-07-11 15:18:47', '2018-07-11 18:18:47', '', 'HsBeaute {seqn{seqn}009', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn009', '', '', '2018-07-11 15:18:47', '2018-07-11 18:18:47', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn009.jpg', 0, 'attachment', 'image/jpeg', 0),
(142, 1, '2018-07-11 14:44:09', '2018-07-11 17:44:09', '', 'Início', 'Início', 'publish', 'closed', 'closed', '', 'inicio', '', '', '2018-07-11 14:47:10', '2018-07-11 17:47:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=142', 1, 'nav_menu_item', '', 0),
(143, 1, '2018-07-11 14:44:09', '2018-07-11 17:44:09', '', 'Portfolio', 'Portfolio', 'publish', 'closed', 'closed', '', 'portfolio', '', '', '2018-07-11 14:47:10', '2018-07-11 17:47:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=143', 2, 'nav_menu_item', '', 0),
(144, 1, '2018-07-11 14:44:09', '2018-07-11 17:44:09', '', 'Sobre Mim', 'Sobre Mim', 'publish', 'closed', 'closed', '', 'sobre-mim', '', '', '2018-07-11 14:47:10', '2018-07-11 17:47:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=144', 3, 'nav_menu_item', '', 0),
(145, 1, '2018-07-11 14:44:10', '2018-07-11 17:44:10', '', 'Estatística', 'Estatística', 'publish', 'closed', 'closed', '', 'estatistica', '', '', '2018-07-11 14:47:10', '2018-07-11 17:47:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=145', 4, 'nav_menu_item', '', 0),
(146, 1, '2018-07-11 14:44:10', '2018-07-11 17:44:10', '', 'Qualidades', 'Qualidades', 'publish', 'closed', 'closed', '', 'qualidades', '', '', '2018-07-11 14:47:10', '2018-07-11 17:47:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=146', 5, 'nav_menu_item', '', 0),
(147, 1, '2018-07-11 14:44:10', '2018-07-11 17:44:10', '', 'Contato', 'Contato', 'publish', 'closed', 'closed', '', 'contato', '', '', '2018-07-11 14:47:10', '2018-07-11 17:47:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=147', 6, 'nav_menu_item', '', 0),
(148, 1, '2018-07-11 14:44:10', '2018-07-11 17:44:10', '', 'Blog', 'Blog', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-07-11 14:47:10', '2018-07-11 17:47:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=148', 7, 'nav_menu_item', '', 0),
(149, 1, '2018-07-11 14:44:10', '2018-07-11 17:44:10', '', 'Loja', 'Loja', 'publish', 'closed', 'closed', '', 'loja', '', '', '2018-07-11 14:47:10', '2018-07-11 17:47:10', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?p=149', 8, 'nav_menu_item', '', 0),
(156, 1, '2018-07-11 14:59:31', '2018-07-11 17:59:31', 'Editorial de Noivas realizado no dia 10 de maio  de 2018 para @hsbeaute', 'Ensaio numero 7', '', 'publish', 'closed', 'closed', '', 'editorial-de-noivas', '', '', '2018-07-11 16:10:29', '2018-07-11 19:10:29', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=156', 2, 'portfolio', '', 0),
(157, 1, '2018-07-11 14:59:16', '2018-07-11 17:59:16', '', 'imagemTopoa', '', 'inherit', 'open', 'closed', '', 'imagemtopoa', '', '', '2018-07-11 14:59:16', '2018-07-11 17:59:16', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/imagemTopoa.jpg', 0, 'attachment', 'image/jpeg', 0),
(158, 1, '2018-07-11 15:02:02', '2018-07-11 18:02:02', 'Desfile Ballet Papillon um evento incrível que adorei fazer parte! Realizado dia 28 de julho de 2018 no Spazio Vecchia', 'Ensaio numero 3', '', 'publish', 'closed', 'closed', '', 'desfile-de-noivas', '', '', '2018-07-11 17:32:06', '2018-07-11 20:32:06', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=158', 6, 'portfolio', '', 0);
INSERT INTO `ak_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(159, 1, '2018-07-11 15:06:20', '2018-07-11 18:06:20', '', 'carrossel-1', '', 'inherit', 'open', 'closed', '', 'carrossel-1', '', '', '2018-07-11 15:06:20', '2018-07-11 18:06:20', '', 80, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-1.png', 0, 'attachment', 'image/png', 0),
(175, 1, '2018-07-11 15:19:04', '2018-07-11 18:19:04', '', 'HsBeaute {seqn{seqn}032', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn032', '', '', '2018-07-11 15:19:04', '2018-07-11 18:19:04', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn032.jpg', 0, 'attachment', 'image/jpeg', 0),
(176, 1, '2018-07-11 15:19:13', '2018-07-11 18:19:13', '', 'HsBeaute {seqn{seqn}038', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn038', '', '', '2018-07-11 15:19:13', '2018-07-11 18:19:13', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn038.jpg', 0, 'attachment', 'image/jpeg', 0),
(177, 1, '2018-07-11 15:19:20', '2018-07-11 18:19:20', '', 'HsBeaute {seqn{seqn}052', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn052', '', '', '2018-07-11 15:19:20', '2018-07-11 18:19:20', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn052.jpg', 0, 'attachment', 'image/jpeg', 0),
(178, 1, '2018-07-11 15:19:29', '2018-07-11 18:19:29', '', 'HsBeaute {seqn{seqn}078', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn078', '', '', '2018-07-11 15:19:29', '2018-07-11 18:19:29', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn078.jpg', 0, 'attachment', 'image/jpeg', 0),
(179, 1, '2018-07-11 15:19:37', '2018-07-11 18:19:37', '', 'HsBeaute {seqn{seqn}082', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn082-2', '', '', '2018-07-11 15:19:37', '2018-07-11 18:19:37', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn082-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2018-07-11 15:19:44', '2018-07-11 18:19:44', '', 'HsBeaute {seqn{seqn}084', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn084', '', '', '2018-07-11 15:19:44', '2018-07-11 18:19:44', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn084.jpg', 0, 'attachment', 'image/jpeg', 0),
(181, 1, '2018-07-11 15:19:51', '2018-07-11 18:19:51', '', 'HsBeaute {seqn{seqn}086', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn086', '', '', '2018-07-11 15:19:51', '2018-07-11 18:19:51', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn086.jpg', 0, 'attachment', 'image/jpeg', 0),
(182, 1, '2018-07-11 15:19:58', '2018-07-11 18:19:58', '', 'HsBeaute {seqn{seqn}090', '', 'inherit', 'open', 'closed', '', 'hsbeaute-seqnseqn090', '', '', '2018-07-11 15:19:58', '2018-07-11 18:19:58', '', 94, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/HsBeaute-seqnseqn090.jpg', 0, 'attachment', 'image/jpeg', 0),
(183, 1, '2018-07-11 15:23:16', '2018-07-11 18:23:16', 'Makes', 'Ensaio numero 4', '', 'publish', 'closed', 'closed', '', 'makes', '', '', '2018-07-11 17:26:48', '2018-07-11 20:26:48', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=183', 5, 'portfolio', '', 0),
(184, 1, '2018-07-11 15:22:20', '2018-07-11 18:22:20', '', 'carrossel-4', '', 'inherit', 'open', 'closed', '', 'carrossel-4', '', '', '2018-07-11 15:22:20', '2018-07-11 18:22:20', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-4.png', 0, 'attachment', 'image/png', 0),
(185, 1, '2018-07-11 15:22:34', '2018-07-11 18:22:34', '', '35666936_1855858947785753_8986319726498021376_n', '', 'inherit', 'open', 'closed', '', '35666936_1855858947785753_8986319726498021376_n', '', '', '2018-07-11 15:22:34', '2018-07-11 18:22:34', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/35666936_1855858947785753_8986319726498021376_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(186, 1, '2018-07-11 15:22:49', '2018-07-11 18:22:49', '', '16684059_1287276444692438_5059187177918994318_n', '', 'inherit', 'open', 'closed', '', '16684059_1287276444692438_5059187177918994318_n', '', '', '2018-07-11 15:22:49', '2018-07-11 18:22:49', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/16684059_1287276444692438_5059187177918994318_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(187, 1, '2018-07-11 15:22:52', '2018-07-11 18:22:52', '', '17264797_1313389965414419_6412324833316148071_n', '', 'inherit', 'open', 'closed', '', '17264797_1313389965414419_6412324833316148071_n', '', '', '2018-07-11 15:22:52', '2018-07-11 18:22:52', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/17264797_1313389965414419_6412324833316148071_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(188, 1, '2018-07-11 15:22:55', '2018-07-11 18:22:55', '', '17342651_1311622338924515_6478359688885335703_n', '', 'inherit', 'open', 'closed', '', '17342651_1311622338924515_6478359688885335703_n', '', '', '2018-07-11 15:22:55', '2018-07-11 18:22:55', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/17342651_1311622338924515_6478359688885335703_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(189, 1, '2018-07-11 15:22:57', '2018-07-11 18:22:57', '', '17353582_1226473577459781_2752955427670846283_n', '', 'inherit', 'open', 'closed', '', '17353582_1226473577459781_2752955427670846283_n', '', '', '2018-07-11 15:22:57', '2018-07-11 18:22:57', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/17353582_1226473577459781_2752955427670846283_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(190, 1, '2018-07-11 15:22:59', '2018-07-11 18:22:59', '', '17362681_1225811627525976_54261209401329020_n', '', 'inherit', 'open', 'closed', '', '17362681_1225811627525976_54261209401329020_n', '', '', '2018-07-11 15:22:59', '2018-07-11 18:22:59', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/17362681_1225811627525976_54261209401329020_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(191, 1, '2018-07-11 15:23:01', '2018-07-11 18:23:01', '', '17951825_1249266415180497_761389869593649494_n', '', 'inherit', 'open', 'closed', '', '17951825_1249266415180497_761389869593649494_n', '', '', '2018-07-11 15:23:01', '2018-07-11 18:23:01', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/17951825_1249266415180497_761389869593649494_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(192, 1, '2018-07-11 15:23:03', '2018-07-11 18:23:03', '', '23031541_1536869026399844_2931608818054581038_n', '', 'inherit', 'open', 'closed', '', '23031541_1536869026399844_2931608818054581038_n', '', '', '2018-07-11 15:23:03', '2018-07-11 18:23:03', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/23031541_1536869026399844_2931608818054581038_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(193, 1, '2018-07-11 15:23:06', '2018-07-11 18:23:06', '', '23316789_1542445372508876_3280173550834253973_n', '', 'inherit', 'open', 'closed', '', '23316789_1542445372508876_3280173550834253973_n', '', '', '2018-07-11 15:23:06', '2018-07-11 18:23:06', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/23316789_1542445372508876_3280173550834253973_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(194, 1, '2018-07-11 15:23:09', '2018-07-11 18:23:09', '', '30582263_1690576454362433_4529715240438333440_n', '', 'inherit', 'open', 'closed', '', '30582263_1690576454362433_4529715240438333440_n', '', '', '2018-07-11 15:23:09', '2018-07-11 18:23:09', '', 183, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/30582263_1690576454362433_4529715240438333440_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(195, 1, '2018-07-11 15:23:32', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-07-11 15:23:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&p=195', 0, 'portfolio', '', 0),
(196, 1, '2018-07-11 15:24:01', '2018-07-11 18:24:01', '', 'carrossel-3', '', 'inherit', 'open', 'closed', '', 'carrossel-3', '', '', '2018-07-11 15:24:01', '2018-07-11 18:24:01', '', 158, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-3.png', 0, 'attachment', 'image/png', 0),
(197, 1, '2018-07-11 15:57:07', '2018-07-11 18:57:07', 'ensaio numero 5', 'Ensaio numero 5', '', 'publish', 'closed', 'closed', '', 'ensaio-numero-5', '', '', '2018-07-11 15:57:07', '2018-07-11 18:57:07', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=197', 4, 'portfolio', '', 0),
(198, 1, '2018-07-11 15:26:32', '2018-07-11 18:26:32', '', 'carrossel-5', '', 'inherit', 'open', 'closed', '', 'carrossel-5', '', '', '2018-07-11 15:26:32', '2018-07-11 18:26:32', '', 197, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-5.png', 0, 'attachment', 'image/png', 0),
(199, 1, '2018-07-11 16:00:37', '2018-07-11 19:00:37', 'Ensaio numero 6', 'Ensaio numero 6', '', 'publish', 'closed', 'closed', '', 'ensaio-numero-6', '', '', '2018-07-11 16:00:37', '2018-07-11 19:00:37', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=199', 3, 'portfolio', '', 0),
(200, 1, '2018-07-11 15:59:31', '2018-07-11 18:59:31', '', '8ce9b5f6-838d-4305-a205-1c1c3b151d55', '', 'inherit', 'open', 'closed', '', '8ce9b5f6-838d-4305-a205-1c1c3b151d55', '', '', '2018-07-11 15:59:31', '2018-07-11 18:59:31', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/8ce9b5f6-838d-4305-a205-1c1c3b151d55.jpg', 0, 'attachment', 'image/jpeg', 0),
(201, 1, '2018-07-11 15:59:33', '2018-07-11 18:59:33', '', 'c9988187-e605-43b8-9b8d-850b6180d621', '', 'inherit', 'open', 'closed', '', 'c9988187-e605-43b8-9b8d-850b6180d621', '', '', '2018-07-11 15:59:33', '2018-07-11 18:59:33', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/c9988187-e605-43b8-9b8d-850b6180d621.jpg', 0, 'attachment', 'image/jpeg', 0),
(202, 1, '2018-07-11 15:59:35', '2018-07-11 18:59:35', '', 'IMG_3052 (2)', '', 'inherit', 'open', 'closed', '', 'img_3052-2', '', '', '2018-07-11 15:59:35', '2018-07-11 18:59:35', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_3052-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(203, 1, '2018-07-11 15:59:37', '2018-07-11 18:59:37', '', 'IMG_3053', '', 'inherit', 'open', 'closed', '', 'img_3053', '', '', '2018-07-11 15:59:37', '2018-07-11 18:59:37', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_3053.jpg', 0, 'attachment', 'image/jpeg', 0),
(204, 1, '2018-07-11 15:59:39', '2018-07-11 18:59:39', '', 'IMG_3054', '', 'inherit', 'open', 'closed', '', 'img_3054', '', '', '2018-07-11 15:59:39', '2018-07-11 18:59:39', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_3054.jpg', 0, 'attachment', 'image/jpeg', 0),
(205, 1, '2018-07-11 15:59:41', '2018-07-11 18:59:41', '', 'IMG_3055', '', 'inherit', 'open', 'closed', '', 'img_3055', '', '', '2018-07-11 15:59:41', '2018-07-11 18:59:41', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_3055.jpg', 0, 'attachment', 'image/jpeg', 0),
(206, 1, '2018-07-11 15:59:44', '2018-07-11 18:59:44', '', 'IMG_3056', '', 'inherit', 'open', 'closed', '', 'img_3056', '', '', '2018-07-11 15:59:44', '2018-07-11 18:59:44', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_3056.jpg', 0, 'attachment', 'image/jpeg', 0),
(207, 1, '2018-07-11 15:59:46', '2018-07-11 18:59:46', '', 'IMG_3062', '', 'inherit', 'open', 'closed', '', 'img_3062', '', '', '2018-07-11 15:59:46', '2018-07-11 18:59:46', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_3062.jpg', 0, 'attachment', 'image/jpeg', 0),
(208, 1, '2018-07-11 15:59:48', '2018-07-11 18:59:48', '', 'IMG_3072', '', 'inherit', 'open', 'closed', '', 'img_3072', '', '', '2018-07-11 15:59:48', '2018-07-11 18:59:48', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_3072.jpg', 0, 'attachment', 'image/jpeg', 0),
(209, 1, '2018-07-11 15:59:51', '2018-07-11 18:59:51', '', 'sereia', '', 'inherit', 'open', 'closed', '', 'sereia', '', '', '2018-07-11 15:59:51', '2018-07-11 18:59:51', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/sereia.jpg', 0, 'attachment', 'image/jpeg', 0),
(210, 1, '2018-07-11 16:00:10', '2018-07-11 19:00:10', '', 'carrossel-6', '', 'inherit', 'open', 'closed', '', 'carrossel-6', '', '', '2018-07-11 16:00:10', '2018-07-11 19:00:10', '', 199, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-6.png', 0, 'attachment', 'image/png', 0),
(211, 1, '2018-07-11 16:03:12', '2018-07-11 19:03:12', '', 'carrossel-7', '', 'inherit', 'open', 'closed', '', 'carrossel-7', '', '', '2018-07-11 16:03:12', '2018-07-11 19:03:12', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-7.png', 0, 'attachment', 'image/png', 0),
(212, 1, '2018-07-11 16:05:17', '2018-07-11 19:05:17', '', 'DSC00996', '', 'inherit', 'open', 'closed', '', 'dsc00996', '', '', '2018-07-11 16:05:17', '2018-07-11 19:05:17', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC00996.jpg', 0, 'attachment', 'image/jpeg', 0),
(213, 1, '2018-07-11 16:05:24', '2018-07-11 19:05:24', '', 'DSC00999', '', 'inherit', 'open', 'closed', '', 'dsc00999', '', '', '2018-07-11 16:05:24', '2018-07-11 19:05:24', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC00999.jpg', 0, 'attachment', 'image/jpeg', 0),
(214, 1, '2018-07-11 16:05:30', '2018-07-11 19:05:30', '', 'DSC01000', '', 'inherit', 'open', 'closed', '', 'dsc01000', '', '', '2018-07-11 16:05:30', '2018-07-11 19:05:30', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC01000.jpg', 0, 'attachment', 'image/jpeg', 0),
(215, 1, '2018-07-11 16:05:36', '2018-07-11 19:05:36', '', 'DSC01001', '', 'inherit', 'open', 'closed', '', 'dsc01001', '', '', '2018-07-11 16:05:36', '2018-07-11 19:05:36', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC01001.jpg', 0, 'attachment', 'image/jpeg', 0),
(216, 1, '2018-07-11 16:05:42', '2018-07-11 19:05:42', '', 'DSC01034', '', 'inherit', 'open', 'closed', '', 'dsc01034', '', '', '2018-07-11 16:05:42', '2018-07-11 19:05:42', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC01034.jpg', 0, 'attachment', 'image/jpeg', 0),
(217, 1, '2018-07-11 16:05:49', '2018-07-11 19:05:49', '', 'DSC01036', '', 'inherit', 'open', 'closed', '', 'dsc01036', '', '', '2018-07-11 16:05:49', '2018-07-11 19:05:49', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC01036.jpg', 0, 'attachment', 'image/jpeg', 0),
(218, 1, '2018-07-11 16:05:56', '2018-07-11 19:05:56', '', 'DSC01039', '', 'inherit', 'open', 'closed', '', 'dsc01039', '', '', '2018-07-11 16:05:56', '2018-07-11 19:05:56', '', 156, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC01039.jpg', 0, 'attachment', 'image/jpeg', 0),
(219, 1, '2018-07-11 16:13:49', '2018-07-11 19:13:49', 'Ensaio numero 8', 'Ensaio numero 8', '', 'publish', 'closed', 'closed', '', 'ensaio-numero-8', '', '', '2018-07-11 16:13:49', '2018-07-11 19:13:49', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=219', 1, 'portfolio', '', 0),
(220, 1, '2018-07-11 16:11:17', '2018-07-11 19:11:17', '', 'carrossel-8', '', 'inherit', 'open', 'closed', '', 'carrossel-8', '', '', '2018-07-11 16:11:17', '2018-07-11 19:11:17', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-8.png', 0, 'attachment', 'image/png', 0),
(221, 1, '2018-07-11 16:12:32', '2018-07-11 19:12:32', '', 'IMG_2251', '', 'inherit', 'open', 'closed', '', 'img_2251', '', '', '2018-07-11 16:12:32', '2018-07-11 19:12:32', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2251.jpg', 0, 'attachment', 'image/jpeg', 0),
(222, 1, '2018-07-11 16:12:38', '2018-07-11 19:12:38', '', 'IMG_2252', '', 'inherit', 'open', 'closed', '', 'img_2252', '', '', '2018-07-11 16:12:38', '2018-07-11 19:12:38', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2252.jpg', 0, 'attachment', 'image/jpeg', 0),
(223, 1, '2018-07-11 16:12:43', '2018-07-11 19:12:43', '', 'IMG_2253', '', 'inherit', 'open', 'closed', '', 'img_2253', '', '', '2018-07-11 16:12:43', '2018-07-11 19:12:43', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2253.jpg', 0, 'attachment', 'image/jpeg', 0),
(224, 1, '2018-07-11 16:12:48', '2018-07-11 19:12:48', '', 'IMG_2256', '', 'inherit', 'open', 'closed', '', 'img_2256', '', '', '2018-07-11 16:12:48', '2018-07-11 19:12:48', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2256.jpg', 0, 'attachment', 'image/jpeg', 0),
(225, 1, '2018-07-11 16:12:53', '2018-07-11 19:12:53', '', 'IMG_2258', '', 'inherit', 'open', 'closed', '', 'img_2258', '', '', '2018-07-11 16:12:53', '2018-07-11 19:12:53', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2258.jpg', 0, 'attachment', 'image/jpeg', 0),
(226, 1, '2018-07-11 16:12:58', '2018-07-11 19:12:58', '', 'IMG_2259', '', 'inherit', 'open', 'closed', '', 'img_2259', '', '', '2018-07-11 16:12:58', '2018-07-11 19:12:58', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2259.jpg', 0, 'attachment', 'image/jpeg', 0),
(227, 1, '2018-07-11 16:13:03', '2018-07-11 19:13:03', '', 'IMG_2262', '', 'inherit', 'open', 'closed', '', 'img_2262', '', '', '2018-07-11 16:13:03', '2018-07-11 19:13:03', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2262.jpg', 0, 'attachment', 'image/jpeg', 0),
(228, 1, '2018-07-11 16:13:09', '2018-07-11 19:13:09', '', 'IMG_2263', '', 'inherit', 'open', 'closed', '', 'img_2263', '', '', '2018-07-11 16:13:09', '2018-07-11 19:13:09', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2263.jpg', 0, 'attachment', 'image/jpeg', 0),
(229, 1, '2018-07-11 16:13:14', '2018-07-11 19:13:14', '', 'IMG_2266', '', 'inherit', 'open', 'closed', '', 'img_2266', '', '', '2018-07-11 16:13:14', '2018-07-11 19:13:14', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2266.jpg', 0, 'attachment', 'image/jpeg', 0),
(230, 1, '2018-07-11 16:13:19', '2018-07-11 19:13:19', '', 'IMG_2268', '', 'inherit', 'open', 'closed', '', 'img_2268', '', '', '2018-07-11 16:13:19', '2018-07-11 19:13:19', '', 219, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/IMG_2268.jpg', 0, 'attachment', 'image/jpeg', 0),
(231, 1, '2018-07-11 17:26:18', '2018-07-11 20:26:18', '', 'Ensaio numero 9', '', 'publish', 'closed', 'closed', '', 'ensaio-numero-9', '', '', '2018-07-11 17:26:18', '2018-07-11 20:26:18', '', 0, 'http://localhost/projetos/amandakaroline_portfolio/?post_type=portfolio&#038;p=231', 0, 'portfolio', '', 0),
(232, 1, '2018-07-11 16:17:47', '2018-07-11 19:17:47', '', 'carrossel-9', '', 'inherit', 'open', 'closed', '', 'carrossel-9', '', '', '2018-07-11 16:17:47', '2018-07-11 19:17:47', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/carrossel-9.png', 0, 'attachment', 'image/png', 0),
(233, 1, '2018-07-11 16:18:11', '2018-07-11 19:18:11', '', '_DSC8146', '', 'inherit', 'open', 'closed', '', '_dsc8146', '', '', '2018-07-11 16:18:11', '2018-07-11 19:18:11', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC8146.jpg', 0, 'attachment', 'image/jpeg', 0),
(234, 1, '2018-07-11 16:18:38', '2018-07-11 19:18:38', '', '_DSC7938', '', 'inherit', 'open', 'closed', '', '_dsc7938', '', '', '2018-07-11 16:18:38', '2018-07-11 19:18:38', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC7938.jpg', 0, 'attachment', 'image/jpeg', 0),
(235, 1, '2018-07-11 16:18:47', '2018-07-11 19:18:47', '', '_DSC7939', '', 'inherit', 'open', 'closed', '', '_dsc7939', '', '', '2018-07-11 16:18:47', '2018-07-11 19:18:47', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC7939.jpg', 0, 'attachment', 'image/jpeg', 0),
(236, 1, '2018-07-11 16:18:53', '2018-07-11 19:18:53', '', '_DSC7951', '', 'inherit', 'open', 'closed', '', '_dsc7951', '', '', '2018-07-11 16:18:53', '2018-07-11 19:18:53', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC7951.jpg', 0, 'attachment', 'image/jpeg', 0),
(237, 1, '2018-07-11 16:19:00', '2018-07-11 19:19:00', '', '_DSC7954', '', 'inherit', 'open', 'closed', '', '_dsc7954', '', '', '2018-07-11 16:19:00', '2018-07-11 19:19:00', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC7954.jpg', 0, 'attachment', 'image/jpeg', 0),
(238, 1, '2018-07-11 16:19:05', '2018-07-11 19:19:05', '', '_DSC7955', '', 'inherit', 'open', 'closed', '', '_dsc7955', '', '', '2018-07-11 16:19:05', '2018-07-11 19:19:05', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC7955.jpg', 0, 'attachment', 'image/jpeg', 0),
(239, 1, '2018-07-11 16:19:11', '2018-07-11 19:19:11', '', '_DSC8140', '', 'inherit', 'open', 'closed', '', '_dsc8140', '', '', '2018-07-11 16:19:11', '2018-07-11 19:19:11', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC8140.jpg', 0, 'attachment', 'image/jpeg', 0),
(240, 1, '2018-07-11 16:19:16', '2018-07-11 19:19:16', '', '_DSC8146', '', 'inherit', 'open', 'closed', '', '_dsc8146-2', '', '', '2018-07-11 16:19:16', '2018-07-11 19:19:16', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC8146-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(241, 1, '2018-07-11 16:19:21', '2018-07-11 19:19:21', '', '_DSC8153', '', 'inherit', 'open', 'closed', '', '_dsc8153', '', '', '2018-07-11 16:19:21', '2018-07-11 19:19:21', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC8153.jpg', 0, 'attachment', 'image/jpeg', 0),
(242, 1, '2018-07-11 16:19:26', '2018-07-11 19:19:26', '', '_DSC8163', '', 'inherit', 'open', 'closed', '', '_dsc8163', '', '', '2018-07-11 16:19:26', '2018-07-11 19:19:26', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC8163.jpg', 0, 'attachment', 'image/jpeg', 0),
(243, 1, '2018-07-11 16:19:31', '2018-07-11 19:19:31', '', '_DSC8172', '', 'inherit', 'open', 'closed', '', '_dsc8172', '', '', '2018-07-11 16:19:31', '2018-07-11 19:19:31', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC8172.jpg', 0, 'attachment', 'image/jpeg', 0),
(244, 1, '2018-07-11 16:19:36', '2018-07-11 19:19:36', '', '_DSC8174', '', 'inherit', 'open', 'closed', '', '_dsc8174', '', '', '2018-07-11 16:19:36', '2018-07-11 19:19:36', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC8174.jpg', 0, 'attachment', 'image/jpeg', 0),
(245, 1, '2018-07-11 16:19:41', '2018-07-11 19:19:41', '', '_DSC8175', '', 'inherit', 'open', 'closed', '', '_dsc8175', '', '', '2018-07-11 16:19:41', '2018-07-11 19:19:41', '', 231, 'http://localhost/projetos/amandakaroline_portfolio/wp-content/uploads/2018/07/DSC8175.jpg', 0, 'attachment', 'image/jpeg', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_termmeta`
--

CREATE TABLE `ak_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_terms`
--

CREATE TABLE `ak_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_terms`
--

INSERT INTO `ak_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Menu Principal', 'menu-principal', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_term_relationships`
--

CREATE TABLE `ak_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_term_relationships`
--

INSERT INTO `ak_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(142, 2, 0),
(143, 2, 0),
(144, 2, 0),
(145, 2, 0),
(146, 2, 0),
(147, 2, 0),
(148, 2, 0),
(149, 2, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_term_taxonomy`
--

CREATE TABLE `ak_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_term_taxonomy`
--

INSERT INTO `ak_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_usermeta`
--

CREATE TABLE `ak_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_usermeta`
--

INSERT INTO `ak_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'amandakaroline'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'ak_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'ak_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(53, 1, 'session_tokens', 'a:4:{s:64:"4111b2360e03da94bbaa55d656af65f97cac28e1e8ce3aabfa135915d1390c60";a:4:{s:10:"expiration";i:1531500788;s:2:"ip";s:3:"::1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";s:5:"login";i:1531327988;}s:64:"48e35501cbd3bb7a8a69ecbb3cf02eb5683a5647b1ec063e1a33e778ff13eea1";a:4:{s:10:"expiration";i:1531504331;s:2:"ip";s:3:"::1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";s:5:"login";i:1531331531;}s:64:"f7487ee6b5e8fccc90347775853ddf0a38ed0b802aac037753cf6e99a7c21879";a:4:{s:10:"expiration";i:1531664981;s:2:"ip";s:3:"::1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";s:5:"login";i:1531492181;}s:64:"a1915d8f998af1e09984c33381043f9fa4ca8fdb969ec49e2e0f8ea6fd2013e3";a:4:{s:10:"expiration";i:1531668503;s:2:"ip";s:3:"::1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";s:5:"login";i:1531495703;}}'),
(48, 1, 'metaboxhidden_portfolio', 'a:2:{i:0;s:10:"wpseo_meta";i:1;s:7:"slugdiv";}'),
(49, 1, 'closedpostboxes_banner-destaque', 'a:0:{}'),
(50, 1, 'metaboxhidden_banner-destaque', 'a:2:{i:0;s:10:"wpseo_meta";i:1;s:7:"slugdiv";}'),
(51, 1, 'meta-box-order_banner-destaque', 'a:7:{s:8:"form_top";s:0:"";s:16:"before_permalink";s:0:"";s:11:"after_title";s:0:"";s:12:"after_editor";s:0:"";s:4:"side";s:22:"submitdiv,postimagediv";s:6:"normal";s:37:"wpseo_meta,metaSessao_Inicial,slugdiv";s:8:"advanced";s:0:"";}'),
(17, 1, 'ak_dashboard_quick_press_last_post_id', '246'),
(19, 1, 'ak_r_tru_u_x', 'a:2:{s:2:"id";s:0:"";s:7:"expires";i:86400;}'),
(20, 1, 'last_login_time', '2018-07-13 12:28:23'),
(21, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(22, 1, 'metaboxhidden_dashboard', 'a:5:{i:0;s:19:"dashboard_right_now";i:1;s:18:"dashboard_activity";i:2;s:24:"wpseo-dashboard-overview";i:3;s:21:"dashboard_quick_press";i:4;s:17:"dashboard_primary";}'),
(55, 1, 'managenav-menuscolumnshidden', 'a:2:{i:0;s:3:"xfn";i:1;s:11:"description";}'),
(24, 1, 'closedpostboxes_post', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(25, 1, 'metaboxhidden_post', 'a:7:{i:0;s:11:"postexcerpt";i:1;s:13:"trackbacksdiv";i:2;s:10:"postcustom";i:3;s:16:"commentstatusdiv";i:4;s:11:"commentsdiv";i:5;s:7:"slugdiv";i:6;s:9:"authordiv";}'),
(27, 1, 'closedpostboxes_destaque', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(54, 1, 'ak_yoast_notifications', 'a:3:{i:0;a:2:{s:7:"message";s:193:"Don\'t miss your crawl errors: <a href="http://localhost/projetos/amandakaroline_portfolio/wp-admin/admin.php?page=wpseo_search_console&tab=settings">connect with Google Search Console here</a>.";s:7:"options";a:9:{s:4:"type";s:7:"warning";s:2:"id";s:17:"wpseo-dismiss-gsc";s:5:"nonce";N;s:8:"priority";d:0.5;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";s:14:"yoast_branding";b:0;}}i:1;a:2:{s:7:"message";s:362:"You still have the default WordPress tagline, even an empty one is probably better. <a href="http://localhost/projetos/amandakaroline_portfolio/wp-admin/customize.php?autofocus[control]=blogdescription&amp;url=http%3A%2F%2Flocalhost%2Fprojetos%2Famandakaroline_portfolio%2Fwp-admin%2Fpost.php%3Fpost%3D231%26action%3Dedit">You can fix this in the customizer</a>.";s:7:"options";a:9:{s:4:"type";s:5:"error";s:2:"id";s:28:"wpseo-dismiss-tagline-notice";s:5:"nonce";N;s:8:"priority";d:0.5;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";s:14:"yoast_branding";b:0;}}i:2;a:2:{s:7:"message";s:247:"<strong>Huge SEO Issue: You\'re blocking access to robots.</strong> You must <a href="http://localhost/projetos/amandakaroline_portfolio/wp-admin/options-reading.php">go to your Reading Settings</a> and uncheck the box for Search Engine Visibility.";s:7:"options";a:9:{s:4:"type";s:5:"error";s:2:"id";s:32:"wpseo-dismiss-blog-public-notice";s:5:"nonce";N;s:8:"priority";i:1;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";s:14:"yoast_branding";b:0;}}}'),
(28, 1, 'metaboxhidden_destaque', 'a:1:{i:0;s:7:"slugdiv";}'),
(29, 1, 'meta-box-order_destaque', 'a:7:{s:8:"form_top";s:0:"";s:16:"before_permalink";s:0:"";s:11:"after_title";s:0:"";s:12:"after_editor";s:0:"";s:4:"side";s:43:"submitdiv,categoriaDestaquediv,postimagediv";s:6:"normal";s:81:"detalhesMetaboxDestaqueInicial,wpseo_meta,detalhesMetaboxDestaquedescicao,slugdiv";s:8:"advanced";s:0:"";}'),
(30, 1, 'screen_layout_destaque', '2'),
(31, 1, 'closedpostboxes_servico', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(32, 1, 'metaboxhidden_servico', 'a:1:{i:0;s:7:"slugdiv";}'),
(33, 1, 'ak_user-settings', 'libraryContent=browse&editor=html'),
(34, 1, 'ak_user-settings-time', '1531331818'),
(35, 1, 'closedpostboxes_depoimento', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(36, 1, 'metaboxhidden_depoimento', 'a:1:{i:0;s:7:"slugdiv";}'),
(44, 1, 'screen_layout_servicos', '2'),
(47, 1, 'closedpostboxes_portfolio', 'a:0:{}'),
(45, 1, 'closedpostboxes_sessao-inicial', 'a:0:{}'),
(46, 1, 'metaboxhidden_sessao-inicial', 'a:2:{i:0;s:10:"wpseo_meta";i:1;s:7:"slugdiv";}'),
(43, 1, 'meta-box-order_servicos', 'a:7:{s:8:"form_top";s:0:"";s:16:"before_permalink";s:0:"";s:11:"after_title";s:0:"";s:12:"after_editor";s:0:"";s:4:"side";s:22:"submitdiv,postimagediv";s:6:"normal";s:31:"metaServicos,wpseo_meta,slugdiv";s:8:"advanced";s:0:"";}'),
(37, 1, 'closedpostboxes_parceria', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(38, 1, 'metaboxhidden_parceria', 'a:2:{i:0;s:10:"wpseo_meta";i:1;s:7:"slugdiv";}'),
(39, 1, 'meta-box-order_parceria', 'a:7:{s:8:"form_top";s:0:"";s:16:"before_permalink";s:0:"";s:11:"after_title";s:0:"";s:12:"after_editor";s:0:"";s:4:"side";s:22:"submitdiv,postimagediv";s:6:"normal";s:32:"metaParcerias,wpseo_meta,slugdiv";s:8:"advanced";s:0:"";}'),
(40, 1, 'screen_layout_parceria', '2'),
(41, 1, 'closedpostboxes_servicos', 'a:0:{}'),
(42, 1, 'metaboxhidden_servicos', 'a:2:{i:0;s:10:"wpseo_meta";i:1;s:7:"slugdiv";}'),
(52, 1, 'screen_layout_banner-destaque', '2'),
(56, 1, 'metaboxhidden_nav-menus', 'a:6:{i:0;s:23:"add-post-type-portfolio";i:1;s:24:"add-post-type-depoimento";i:2;s:22:"add-post-type-parceria";i:3;s:22:"add-post-type-destaque";i:4;s:12:"add-post_tag";i:5;s:21:"add-categoriaDestaque";}'),
(57, 1, 'nav_menu_recently_edited', '2'),
(58, 1, 'manageedit-pagecolumnshidden', 'a:5:{i:0;s:11:"wpseo-score";i:1;s:23:"wpseo-score-readability";i:2;s:11:"wpseo-title";i:3;s:14:"wpseo-metadesc";i:4;s:13:"wpseo-focuskw";}'),
(59, 1, 'edit_page_per_page', '20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_users`
--

CREATE TABLE `ak_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_users`
--

INSERT INTO `ak_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'amandakaroline', '$P$BGU4ZKNP2sUvzxnPoDNlZJQSrZ1UiT/', 'amandakaroline', 'carolinoamandacs@gmail.com', '', '2018-06-19 18:44:03', '', 0, 'amandakaroline');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_yoast_seo_links`
--

CREATE TABLE `ak_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ak_yoast_seo_meta`
--

CREATE TABLE `ak_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `ak_yoast_seo_meta`
--

INSERT INTO `ak_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(4, 0, 0),
(18, 0, 0),
(25, 0, 0),
(29, 0, 0),
(63, 0, 0),
(72, 0, 0),
(74, 0, 0),
(79, 0, 0),
(80, 0, 0),
(89, 0, 0),
(90, 0, 0),
(91, 0, 0),
(92, 0, 0),
(94, 0, 0),
(99, 0, 0),
(101, 0, 0),
(103, 0, 0),
(104, 0, 0),
(105, 0, 0),
(107, 0, 0),
(102, 0, 0),
(112, 0, 0),
(114, 0, 0),
(116, 0, 0),
(118, 0, 0),
(120, 0, 0),
(122, 0, 0),
(124, 0, 0),
(127, 0, 0),
(111, 0, 0),
(108, 0, 0),
(138, 0, 0),
(139, 0, 0),
(69, 0, 0),
(129, 0, 0),
(130, 0, 0),
(131, 0, 0),
(132, 0, 0),
(133, 0, 0),
(134, 0, 0),
(135, 0, 0),
(137, 0, 0),
(150, 0, 0),
(154, 0, 0),
(155, 0, 0),
(156, 0, 0),
(158, 0, 0),
(153, 0, 0),
(86, 0, 0),
(151, 0, 0),
(83, 0, 0),
(84, 0, 0),
(87, 0, 0),
(152, 0, 0),
(95, 0, 0),
(88, 0, 0),
(85, 0, 0),
(67, 0, 0),
(81, 0, 0),
(82, 0, 0),
(68, 0, 0),
(76, 0, 0),
(183, 0, 0),
(197, 0, 0),
(199, 0, 0),
(219, 0, 0),
(231, 0, 0),
(20, 0, 0),
(21, 0, 0),
(22, 0, 0),
(23, 0, 0),
(24, 0, 0),
(6, 0, 0),
(7, 0, 0),
(8, 0, 0),
(9, 0, 0),
(10, 0, 0),
(11, 0, 0),
(12, 0, 0),
(13, 0, 0),
(14, 0, 0),
(17, 0, 0),
(15, 0, 0),
(16, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ak_aiowps_events`
--
ALTER TABLE `ak_aiowps_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ak_aiowps_failed_logins`
--
ALTER TABLE `ak_aiowps_failed_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ak_aiowps_global_meta`
--
ALTER TABLE `ak_aiowps_global_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `ak_aiowps_login_activity`
--
ALTER TABLE `ak_aiowps_login_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ak_aiowps_login_lockdown`
--
ALTER TABLE `ak_aiowps_login_lockdown`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ak_aiowps_permanent_block`
--
ALTER TABLE `ak_aiowps_permanent_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ak_commentmeta`
--
ALTER TABLE `ak_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `ak_comments`
--
ALTER TABLE `ak_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `ak_links`
--
ALTER TABLE `ak_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `ak_options`
--
ALTER TABLE `ak_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `ak_postmeta`
--
ALTER TABLE `ak_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `ak_posts`
--
ALTER TABLE `ak_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `ak_termmeta`
--
ALTER TABLE `ak_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `ak_terms`
--
ALTER TABLE `ak_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `ak_term_relationships`
--
ALTER TABLE `ak_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `ak_term_taxonomy`
--
ALTER TABLE `ak_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `ak_usermeta`
--
ALTER TABLE `ak_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `ak_users`
--
ALTER TABLE `ak_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `ak_yoast_seo_links`
--
ALTER TABLE `ak_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `ak_yoast_seo_meta`
--
ALTER TABLE `ak_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ak_aiowps_events`
--
ALTER TABLE `ak_aiowps_events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ak_aiowps_failed_logins`
--
ALTER TABLE `ak_aiowps_failed_logins`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ak_aiowps_global_meta`
--
ALTER TABLE `ak_aiowps_global_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ak_aiowps_login_activity`
--
ALTER TABLE `ak_aiowps_login_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ak_aiowps_login_lockdown`
--
ALTER TABLE `ak_aiowps_login_lockdown`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ak_aiowps_permanent_block`
--
ALTER TABLE `ak_aiowps_permanent_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ak_commentmeta`
--
ALTER TABLE `ak_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ak_comments`
--
ALTER TABLE `ak_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ak_links`
--
ALTER TABLE `ak_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ak_options`
--
ALTER TABLE `ak_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=565;
--
-- AUTO_INCREMENT for table `ak_postmeta`
--
ALTER TABLE `ak_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=882;
--
-- AUTO_INCREMENT for table `ak_posts`
--
ALTER TABLE `ak_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `ak_termmeta`
--
ALTER TABLE `ak_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ak_terms`
--
ALTER TABLE `ak_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ak_term_taxonomy`
--
ALTER TABLE `ak_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ak_usermeta`
--
ALTER TABLE `ak_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `ak_users`
--
ALTER TABLE `ak_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ak_yoast_seo_links`
--
ALTER TABLE `ak_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
