<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amanda_Karoline
 */
global $configuracao;
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />

	<!-- TÍTULO -->
	<title>Amanda Karoline</title>

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /> 
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
	<!-- TOPO -->
	<header class="topo" style="display:none ; background-color: <?php echo $configuracao['opt_background-color'] ?>" id="menu" >
		<div class="container">
			<div class="row">
				
				<!-- LOGO -->
				<div class="col-sm-1">
					<a href="<?php echo get_home_url() ?>" class="logo">
						<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php echo $configuracao['opt_logo']['title'] ?>">
					</a>
				</div>

				<!-- MENU  -->	
				<div class="col-sm-11">
					<div class="navbar" role="navigation">	
										
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
							<span class="sr-only"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!--  MENU MOBILE-->
						<div class="row navbar-header ">			
							<nav class="collapse navbar-collapse" id="collapse">
								<?php 
									$menu = array(
										'theme_location'  => '',
										'menu'            => 'Menu Principal',
										'container'       => false,
										'container_class' => '',
										'container_id'    => '',
										'menu_class'      => 'nav navbar-nav',
										'menu_id'         => '',
										'echo'            => true,
										'fallback_cb'     => 'wp_page_menu',
										'before'          => '',
										'after'           => '',
										'link_before'     => '',
										'link_after'      => '',
										'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
										'depth'           => 2,
										'walker'          => ''
										);
									wp_nav_menu( $menu );
								?>
							</nav>						
						</div>	
					</div>
				</div>
				
			</div>
		</div>
	</header>